document.addEventListener("DOMContentLoaded", function() {
  const input = document.getElementById('filepublic');
  const imagenhistoria = document.getElementById('imagenhistoria');
  const creadorHistoria = document.getElementById('creador-historia');
  const creadorHistorias = document.getElementById('creador-historias');
  const imagenCreadoHistoria = document.getElementById('c-i-h');
  const funcionesCrear = document.getElementById('funciones-crear');
  const crearLayout = document.getElementById('crear-layout');
  const btnEliminarImagen = document.getElementById('eliminar-imagen');
  const botonpublicar = document.getElementById('botonpublicar');
  const previewImagenLibros = document.getElementById('preview-image-libro');
  const imagenesGalleria = document.getElementById('imagenes-galleria');
  const desaparecer_cargando = document.getElementById('desaparecer_cargando');
  const desaparecer_cargando2 = document.getElementById('desaparecer_cargando2');
  const eliminarh = document.getElementById('eliminar-h');
  const eliminarHContainer = document.getElementById('eliminar-h-container');
  const eliminarh2 = document.getElementById('eliminar-h2');
  const eliminarHContainer2 = document.getElementById('eliminar-h-container2');
  const prevPublicar = document.getElementById('prev-publicar');
  
  //funciones para mostrar los precios
  let precioInput = document.getElementById('precio');
  const precioProducto = document.getElementById('precio-producto');
  const precioTotal = document.getElementById('precio-total');
  const descuentoPrecioProducto = document.getElementById('descuento-precioproducto');

  //Eventos en los radio button
  const recomendadosSi = document.getElementById('recomendados-si');
  const recomendadosNo = document.getElementById('recomendados-no');
  const mensajeRecomendados = document.getElementById('mensajerecomendado'); 

  //valores del backend
  var ivaValue;
  var descuento; 
  var descuento_recomendados;


   precioInput.addEventListener("input", function() {
      obtenerDatosIVA();
      calcularPrecio(ivaValue, descuento, descuento_recomendados);
      // alert("Cambio de estado del input");
  });
  

  //Validacion del formulario de historia
  let btnEnviar = document.getElementById('btn-enviar');
  let publicarLibro = document.getElementById('publicar-libro');
  const nextPublicar = document.getElementById('next-publicar');

  // function validarCrearHistoria(event) {
  //   // let nombre = document.getElementById('nombre').value;
  //   // let desc = document.getElementById('desc').value;
  //   let btnEnviar = document.getElementById('btn-enviar');
  //   var fileInput = document.getElementById('upload-files');
    
  //   if (fileInput.files.length === 0) {
  //     //swal("No le has puesto imagen a la historia", "", "error");
  //     Swal.fire({ title:"No le has puesto imagen a la historia",text:"",icon:"error"});   
  //     event.preventDefault(); 
           
  //     btnEnviar.disabled = true;
  //     return false; 
  //   }

  //   // Verificar si el archivo seleccionado es una imagen
  //   var file = fileInput.files[0];
  //   var fileType = file.type;
  //   if (!fileType.startsWith('image/')) {
  //     event.preventDefault();
  //     //swal("Solo se permiten imágenes", "", "error");
  //     Swal.fire({ title:"Solo se permiten imágenes",text:"",icon:"error"});
  //     return false;
  //   }


  // }

  btnEnviar.addEventListener("click", (event) => {
    event.preventDefault();
    var validacionExitosa = validarCrearHistoria(event);

    if (validacionExitosa) {
      btnEnviar.disabled = true;
        event.target.form.submit(); 
        console.log("Desahibilitadio");
    }
    
  });

  function validarCrearHistoria(event) {
    var fileInput = document.getElementById('upload-files');
    var btnEnviar = document.getElementById('btn-enviar');

    if (fileInput.files.length === 0) {
        Swal.fire({ title:"No le has puesto imagen a la historia",text:"",icon:"error"});   
        event.preventDefault(); 
        return false; 
    }

    var file = fileInput.files[0];
    var fileType = file.type;
    if (!fileType.startsWith('image/')) {
        Swal.fire({ title:"Solo se permiten imágenes",text:"",icon:"error"});
        event.preventDefault();
        return false;
    }

    btnEnviar.disabled = true;
    return true;
}

document.querySelector('.formulario-historia').addEventListener('submit', validarCrearHistoria);

  btnEnviar.addEventListener('click', validarCrearHistoria);
  
 

  function validarLibro(){
    var nombre = document.getElementById("nombre-libro").value;
    let desc = document.getElementById("desc-libro").value;
    let categoriaNombre = document.getElementById("categoria_nombre").value;
    let tema = document.getElementById("tema").value;
    let idioma = document.getElementById("idioma").value;
    let correo = document.getElementById("correo").value;
    let uploadfiles2 = document.getElementById('upload-files2');

    if (uploadfiles2.files.length === 0){
      //swal("Ponle una foto a tu libro", "", "error");
      Swal.fire({ title:"El libro debe tener una imagen",text:"",icon:"error"});
      return false;
    }

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (nombre.trim() === "") {
        //swal("Seleccione un título para su libro", "", "error");
        Swal.fire({ title:"Seleccione un título para su libro",text:"",icon:"error"});
        return false;
    } else if (nombre.trim().length > 50) {
      Swal.fire({ title:"El título no puede tener más de 50 caracteres",text:"",icon:"error"});
      return false;
    }else if (desc.trim() === "") {
        //swal("Introduzca un Resumen", "", "error");
        Swal.fire({ title:"Introduzca un Resumen",text:"",icon:"error"});
        return false;
    } else if (categoriaNombre.trim() === "") {
        //swal("Seleccione un Género Literario", "", "error");
        Swal.fire({ title:"Seleccione un Género Literario",text:"",icon:"error"});
        return false;
    } else if (tema.trim() === "") {
      //swal("Seleccione un tema", "", "error");
      Swal.fire({ title:"Seleccione un tema",text:"",icon:"error"});
      return false;
    }else if (idioma.trim() === "") {
      //swal("Seleccione un idioma", "", "error");
      Swal.fire({ title:"Seleccione un idioma",text:"",icon:"error"});
      return false;
    }else if (correo.trim() === "") {
      //swal("Introduzca su correo electrónico", "", "error");
      Swal.fire({ title:"Introduzca su correo electrónico",text:"",icon:"error"});
      return false;
    }else if (!emailRegex.test(correo)) {
      //swal("El correo electrónico no es válido", "", "error");
      Swal.fire({ title:"El correo electrónico no es válido",text:"",icon:"error"});
      event.preventDefault();
    } else { 
      return true; 
    }
  
  }

  nextPublicar.addEventListener('click', function(event) {
    if (!validarLibro(event)) {
      event.preventDefault(); 
    } else {
      if (precioProducto.innerHTML !== "$0.00" || descuentoPrecioProducto.innerHTML !== "$0.00" || precioTotal.innerHTML !== "$0.00") {
        paso21.style.display = 'none';
        paso3.style.display = 'flex';
    } else {
        inicializarValoresDelCalculo();
        paso21.style.display = 'none';
        paso3.style.display = 'flex';
    }
    }
  });

  // if (precioInput.value === '') {
  //   inicializarValoresDelCalculo();
  //   console.log("el valor del precio esta vacio");
  // } 

  // if (precioInput.value !== '') {
  //   calcularPrecio(ivaValue, descuento, descuento_recomendados);
  //   console.log('ya tienes valores');
  // }

 

  publicarLibro.addEventListener('click', function(event) {
    event.preventDefault(); 
    
    if (validarLibroPasoDos()) {
      enviarDatosDelFormulario();
    }
  });
  
  function validarLibroPasoDos() {
    let catp = document.getElementById('catp').value;
    let precio = document.getElementById('precio').value;
    let fileLibro = document.getElementById('fileLibro');
    var archivo = fileLibro.files[0];
  
    if (catp.trim() === "") {
      
      Swal.fire({ title:"Seleccione Categoría de pago",text:"",icon:"error"});
      return false;
    }
  
    if (precio.trim() === "") {
      //swal("Recuerda ponerle precio al libro", "", "error");
      Swal.fire({ title:"Recuerda ponerle precio al libro",text:"",icon:"error"});
      return false;
    }

    if (fileLibro.files.length === 0) {
      Swal.fire({ title:"Debe adjuntar el Libro o escrito a publicar (PDF, EPUB)",text:"",icon:"error"});
      console.log(fileLibro.files.length);
      return false;
    }

    // Verificar la extensión del archivo adjunto
    var extension = archivo.name.split('.').pop().toLowerCase();
    if (extension !== "pdf" && extension !== "epub") {
        Swal.fire({
            title: "El archivo adjuntado debe ser PDF o EPUB",
            text: "",
            icon: "error"
        });
        return false;
    }
  
    publicarLibro.disabled = true;
    return true;
  }
  
  function enviarDatosDelFormulario() {
    document.getElementById("formulario-libro").submit(); 
  }

  // Drag and Drop 
  
  const INPUT_FILE = document.querySelector('#upload-files');
  const INPUT_CONTAINER = document.querySelector('#upload-container');
  const FILES_LIST_CONTAINER = document.querySelector('#files-list-container');
  const FILE_LIST = [];
  let UPLOADED_FILES = [];

  const INPUT_FILE2 = document.querySelector('#upload-files2');
  const INPUT_CONTAINER2 = document.querySelector('#upload-container2');
  const FILES_LIST_CONTAINER2 = document.querySelector('#files-list-container2');
  const FILE_LIST2 = [];
  let UPLOADED_FILES2 = [];
  
  const multipleEvents = (element, eventNames, listener) => {
    const events = eventNames.split(' ');
   
    events.forEach(event => {
      element.addEventListener(event, listener, false);
    });
  };
  
  const previewImages = () => {
    if (FILE_LIST.length > 0) {
        const lastAddedFile = FILE_LIST[FILE_LIST.length - 1]; 
        INPUT_CONTAINER.style.backgroundImage = `url(${lastAddedFile.url})`;
        INPUT_CONTAINER.classList.add('active');
        document.getElementById('overlay').style.backgroundColor = 'rgba(0, 0, 0, 0.5)'; 
        document.getElementById('overlay').style.position = 'absolute';
        document.getElementById('overlay').style.width = '100%';
        document.getElementById('overlay').style.height = '100%';
        document.getElementById('overlay').style.top = '0';
        document.getElementById('overlay').style.left = '0';
        document.getElementById('overlay').style.zIndex = '1'; 
        document.getElementById('overlay').style.display = 'block'; 
        desaparecer_cargando.style.display = 'none';
        eliminarHContainer.style.display = 'block';

        document.querySelector('#visualizar').addEventListener('click', () => {
          if (FILE_LIST.length > 0) {

            // var imageUrl = lastAddedFile.url;
            // window.open(imageUrl);
            // alert("Se esta visualizando")

            let lastAddedFilevi = FILE_LIST.pop();
            FILE_LIST.push(lastAddedFilevi); 

            window.open(lastAddedFilevi.url); 
          } 
        });
        
    } else {
        console.log('empty');
        INPUT_FILE.value = '';
        INPUT_CONTAINER.style.backgroundImage = 'none'; 
        INPUT_CONTAINER.classList.remove('active');
        document.getElementById('overlay').style.backgroundColor = 'transparent'; 
    }
};


const previewImages2 = () => {
  if (FILE_LIST2.length > 0) {
      let lastAddedFile2 = FILE_LIST2[FILE_LIST2.length - 1]; 
      INPUT_CONTAINER2.style.backgroundImage = `url(${lastAddedFile2.url})`;
      INPUT_CONTAINER2.classList.add('active');
      document.getElementById('overlay2').style.backgroundColor = 'rgba(0, 0, 0, 0.5)'; 
      document.getElementById('overlay2').style.backgroundSize = 'cover'; 
      document.getElementById('overlay2').style.position = 'absolute';
      document.getElementById('overlay2').style.width = '100%';
      document.getElementById('overlay2').style.height = '100%';
      document.getElementById('overlay2').style.top = '0';
      document.getElementById('overlay2').style.left = '0';
      document.getElementById('overlay2').style.zIndex = '1';
      document.getElementById('overlay2').style.display = 'block';  
      desaparecer_cargando2.style.display = 'none';
      eliminarHContainer2.style.display = 'block';
      imagenesGalleria.style.display = 'block';
      // INPUT_CONTAINER2.style.width = "100% !important";

      document.querySelector('#visualizar2').addEventListener('click', () => {
        if (FILE_LIST2.length > 0) {
          // openModal2(lastAddedFile2.url);
          // var imageUrl2 = lastAddedFile2.url;
          // console.log(imageUrl2);
          // window.open(lastAddedFile2.url);
          // window.location.href = lastAddedFile2.url;

          if (lastAddedFile2) {
            // console.log(FILE_LIST2);
            // FILE_LIST2.shift();
            let lastAddedFile2vi = FILE_LIST2.pop();
            FILE_LIST2.push(lastAddedFile2vi); 

            window.open(lastAddedFile2vi.url); 
        }
        } 
      });

  } else {
      console.log('empty');
      INPUT_FILE2.value = '';
      INPUT_CONTAINER2.style.backgroundImage = 'none'; 
      INPUT_CONTAINER2.classList.remove('active');
      document.getElementById('overlay2').style.backgroundColor = 'transparent'; 
  }
};

  
  const fileUpload = () => {
    if (FILES_LIST_CONTAINER) {
      multipleEvents(INPUT_FILE, 'click dragstart dragover', () => {
        INPUT_CONTAINER.classList.add('active');
      });
    
      multipleEvents(INPUT_FILE, 'dragleave dragend drop change blur', () => {
        INPUT_CONTAINER.classList.remove('active');
      });
    
      INPUT_FILE.addEventListener('change', () => {
        const files = [...INPUT_FILE.files];
        console.log("changed");
        files.forEach(file => {
          const fileURL = URL.createObjectURL(file);
          const fileName = file.name;
          if (!file.type.match("image/")){
            alert(file.name + " is not an image");
            console.log(file.type);
          } else {
            const uploadedFiles = {
              name: fileName,
              url: fileURL,
            };
  
            FILE_LIST.push(uploadedFiles);
          }
        });
        
        console.log(FILE_LIST); 
        previewImages();
        UPLOADED_FILES = document.querySelectorAll(".js-remove-image");
        removeFile();
      }); 
    }
  };

  const fileUpload2 = () => {
    if (FILES_LIST_CONTAINER2) {
      multipleEvents(INPUT_FILE2, 'click dragstart dragover', () => {
        INPUT_CONTAINER2.classList.add('active');
      });
    
      multipleEvents(INPUT_FILE2, 'dragleave dragend drop change blur', () => {
        INPUT_CONTAINER2.classList.remove('active');
      });
    
      INPUT_FILE2.addEventListener('change', () => {
        const files2 = [...INPUT_FILE2.files];
        console.log("changed");
        files2.forEach(file => {
          const fileURL = URL.createObjectURL(file);
          const fileName = file.name;
          if (!file.type.match("image/")){
            alert(file.name + " is not an image");
            console.log(file.type);
          } else {
            const uploadedFiles = {
              name: fileName,
              url: fileURL,
            };
  
            FILE_LIST2.push(uploadedFiles);
          }
        });
        
        console.log(FILE_LIST2); 
        previewImages2();
        UPLOADED_FILES2 = document.querySelectorAll(".js-remove-image");
        removeFile2();
      }); 
    }
  };
  
  const removeFile = () => {
    UPLOADED_FILES = document.querySelectorAll(".js-remove-image");
    
    if (UPLOADED_FILES) {
      UPLOADED_FILES.forEach(image => {
        image.addEventListener('click', function() {
          const fileIndex = this.getAttribute('data-index');
  
          FILE_LIST.splice(fileIndex, 1);
          previewImages();
          removeFile();
        });
      });
    } else {
      INPUT_FILE.value = ''; 
    }
  };

  const removeFile2 = () => {
    UPLOADED_FILES2 = document.querySelectorAll(".js-remove-image");
    
    if (UPLOADED_FILES2) {
      UPLOADED_FILES2.forEach(image => {
        image.addEventListener('click', function() {
          const fileIndex = this.getAttribute('data-index');
  
          FILE_LIST2.splice(fileIndex, 1);
          previewImages2();
          removeFile2();
        });
      });
    } else {
      INPUT_FILE2.value = ''; 
    }
  };
  
  eliminarh.addEventListener('click', function() {
    // Verificar si hay una imagen cargada
    if (FILE_LIST.length > 0) {
        
        FILE_LIST.splice(0, FILE_LIST.length);
        // Actualizar la vista previa
        previewImages();
        desaparecer_cargando.style.display = 'block';
        eliminarHContainer.style.display = 'none';
        document.getElementById('overlay').style.display = 'none';
    }
  });

  eliminarh2.addEventListener('click', function() {
    // Verificar si hay una imagen cargada
    if (FILE_LIST2.length > 0) {
        // Eliminar la última imagen de la lista
        FILE_LIST2.splice(0, FILE_LIST2.length);
        // Actualizar la vista previa
        previewImages2();
        desaparecer_cargando2.style.display = 'block';
        eliminarHContainer2.style.display = 'none';
        document.getElementById('overlay2').style.display = 'none';
    }
  });

 // Open modal with the specified image URL
const openModal = (imageUrl) => {
  const modal = document.getElementById('modal');
  const modalImage = document.getElementById('modal-image');

  modalImage.src = imageUrl;
  modal.style.display = 'block';
  document.querySelector('.close').addEventListener('click', closeModal);
};

const openModal2 = (imageUrl) => {
  const modal2 = document.getElementById('modal2');
  const modalImage2 = document.getElementById('modal-image2');

  modalImage2.src = imageUrl;
  modal2.style.display = 'block';
  document.getElementById('close2').addEventListener('click', closeModal2);
};

// Close the modal
const closeModal = () => {
  const modal = document.getElementById('modal');
  const modalImage = document.getElementById('modal-image');

  modal.style.display = 'none';
  modalImage.src = '';
};

const closeModal2 = () => {
  const modal2 = document.getElementById('modal2');
  const modalImage2 = document.getElementById('modal-image2');

  modal2.style.display = 'none';
  modalImage2.src = '';
};


  fileUpload();
  removeFile();

  fileUpload2();
  removeFile2();

  // funcionando

//   imagenhistoria.addEventListener('change', function() {
//     const file = this.files[0];
//     if (file && file.type.startsWith('image/')) {
//         const reader = new FileReader();
//         reader.onload = function() {
//             creadorHistorias.style.backgroundImage = `url(${reader.result})`;
//             creadorHistorias.classList.add('historias-prev');
//         }
//         reader.readAsDataURL(file);
//     } else {
//         creadorHistorias.style.backgroundImage = '';
//     }
// });

  // input.addEventListener('change', function() {
  //     const file = this.files[0];
  //     if (file && file.type.startsWith('image/')) {
  //         const reader = new FileReader();
  //         reader.onload = function() {
  //             creadorHistoria.style.backgroundImage = `url(${reader.result})`;
  //             btnEliminarImagen.style.display = 'block';
  //            // creadorHistoria.classList.add('preview-cargado');
  //            // imagenCreadoHistoria.style.display = 'none';
  //            // funcionesCrear.style.display = 'flex';
  //            // crearLayout.style.justifyContent = "space-around";
  //            // botonpublicar.style.display = 'block';

            
  //         }
  //         reader.readAsDataURL(file);
  //     } else {
  //         creadorHistoria.style.backgroundImage = '';
  //     }
  // });

  // btnEliminarImagen.addEventListener('click', function() {
  //     input.value = ''; 
  //     creadorHistoria.style.backgroundImage = '';
  //     preview.innerHTML = ''; 
  //     btnEliminarImagen.style.display = 'none'; 
  // });


  //var publicar
  const filepublic = document.getElementById('filepublic');
  const paso1 = document.getElementById('paso1');
  const paso2 = document.getElementById('paso2');
  const paso3 = document.getElementById('paso3');
  const previewImagenLibro = document.getElementById('p-i-l');
  const paso21 = document.getElementById('paso2-1');
  const back_step2 = document.getElementById('back_step2');



  // filepublic.addEventListener('change', function() {
  //     const file = this.files[0];
  //     if (file && file.type.startsWith('image/')) {
  //         const reader = new FileReader();
  //         reader.onload = function() {
  //             previewImagenLibro.style.backgroundImage = `url(${reader.result})`;
  //             previewImagenLibro.classList.add('preview-cargado');
  //             //imagenCreadoHistoria.style.display = 'none';
  //             //paso1.style.display = 'none';
  //             //paso2.style.display = 'block'; 

                
  //             previewImagenLibros.style.display = 'block';
  //             creadorHistoria.style.display = 'none'; 
  //         }
  //         reader.readAsDataURL(file);
  //     }
  // });

 

  back_step2.addEventListener('click', function(){
      paso3.style.display = 'none';
      paso21.style.display = 'block';
  });

  var toggleRecomendados = document.getElementById('toggle-recomendados');
  var agregarRecomendados = document.querySelector('.agregar-recomendados');
  var calculorecomendados = document.getElementById('calculorecomendados');

  toggleRecomendados.addEventListener('change', function () {
      agregarRecomendados.style.display = this.checked ? 'block' : 'none';
      mensajeRecomendados.style.display = 'block';
      obtenerDatosIVA();
      calcularPrecio(ivaValue, descuento, descuento_recomendados);
      // calculorecomendados.style.display = 'block';
  });

  recomendadosSi.addEventListener('change', function() {
      if (this.checked) {
          mensajeRecomendados.style.display = 'block';
          calcularPrecio(ivaValue, descuento, descuento_recomendados);
      }
  });

  recomendadosNo.addEventListener('change', function() {
      if (this.checked) {
          mensajeRecomendados.style.display = 'none';
          calcularPrecio(ivaValue, descuento, descuento_recomendados);
      }
  });

  precioInput.addEventListener("input", function() {
      // calcularPrecio(ivaValue, descuento, descuento_recomendados);
      alert("Cambio de estado del input");
  });
  
  function obtenerDatosIVA() {
    jQuery.ajax({
        url: ajaxurl,
        method: 'POST',
        data: {
            action: 'obtener_datos_iva'
        },
        success: function(response) {
            if (response.success) {
                console.log(response);
                ivaValue = parseFloat(response.ivaValue); 
                descuento = parseFloat(response.descuento);
                descuento_recomendados = parseFloat(response.descuento_recomendados);
                console.log('Valor del IVA obtenido:', ivaValue);
                console.log('Valor del descuento:', descuento);
                console.log('Valor del descuento recomendados:', descuento_recomendados);
                // console.log('Tipo de dato del valor del IVA:', typeof ivaValue)
                calcularPrecio(ivaValue, descuento, descuento_recomendados); 
            }
        },
        error: function() {
            console.log('Error en la solicitud AJAX.');
        }
    });
}

obtenerDatosIVA();

function inicializarValoresDelCalculo() {
  precioProducto.innerHTML = "$0.00";
  descuentoPrecioProducto.innerHTML = "$0.00";
  precioTotal.innerHTML = "$0.00"
}

function calcularPrecio(ivaValue, descuento, descuento_recomendados) {
  let precioOut = parseFloat(precioInput.value);

  precioProducto.innerHTML = '$' + precioOut.toFixed(2);

  let iba = precioOut - ( (precioOut * ivaValue)/100); 
  console.log("El calculito del precio de salida: ", precioOut );

  let descuentoCalculo = 0;
  let total = 0; 

  if (toggleRecomendados.checked === true) {
      descuentoCalculo = iba * (descuento_recomendados/100);
      console.log(iba, (descuento_recomendados/100), descuentoCalculo);
      console.log(toggleRecomendados.checked);
  } else if (toggleRecomendados.checked === false) {
      descuentoCalculo = iba * (descuento/100);
      console.log(toggleRecomendados.checked);
  } else {
    descuentoCalculo = 0;
  }

  total = iba - descuentoCalculo;

  descuentoCalculo = descuentoCalculo.toFixed(2);
  iba = (precioOut * ivaValue).toFixed(2); 
  total = total.toFixed(2);

  let ibafinal = 0;
  ibafinal = ((precioOut * ivaValue)/100).toFixed(2);

  descuentoPrecioProducto.innerText = "$" + descuentoCalculo + " + iva: " + "$" + ibafinal;
  precioTotal.innerText = "$" + total;
}

// precioInputEstado.addEventListener('input', function() {
//   if (precioInputEstado.value === '') {
//       inicializarValoresDelCalculo();
//       console.log("El valor del precio está vacío");
//   } else {
//       calcularPrecio(ivaValue, descuento, descuento_recomendados);
//       console.log("El valor del precio está con datos");
//   }
// });

  
});




document.addEventListener('DOMContentLoaded', function() {
  ImgUpload();
});

function ImgUpload() {
  var imgWrap = "";
  var imgArray = [];

  var inputFiles = document.querySelectorAll('.upload__inputfile');
  inputFiles.forEach(function(input) {
    input.addEventListener('change', function(e) {
      imgWrap = this.closest('.upload__box').querySelector('.upload__img-wrap');
      var maxLength = 5;

      var files = e.target.files;
      var filesArr = Array.prototype.slice.call(files);
      var iterator = 0;
      filesArr.forEach(function(f, index) {
        if (!f.type.match('image.*')) {
          return;
        }

        if (imgArray.length >= maxLength) {
          //swal('Solo se permiten un máximo de ' + maxLength + ' imágenes.', "", "error");
          Swal.fire({ title:'Solo se permiten un máximo de ' + maxLength + ' imágenes.',text:"",icon:"error"});
      
          return false;
        } else {
          var len = 0;
          for (var i = 0; i < imgArray.length; i++) {
            if (imgArray[i] !== undefined) {
              len++;
            }
          }
          if (len >= maxLength) {
            Swal.fire({ title:'Solo se permiten un máximo de ' + maxLength + ' imágenes.',text:"",icon:"error"});
      
            return false;
          } else {
            imgArray.push(f);

            var reader = new FileReader();
            reader.onload = function(e) {
              var html = "<div class='upload__img-box'><div style='background-image: url(" + e.target.result + ")' data-number='" + document.querySelectorAll(".upload__img-close").length + "' data-file='" + f.name + "' class='img-bg'><div class='upload__img-close'></div></div></div>";
              imgWrap.insertAdjacentHTML('beforeend', html);
              iterator++;
            }
            reader.readAsDataURL(f);
          }
        }
      });
    });
  });

  document.body.addEventListener('click', function(e) {
    if (e.target.classList.contains('upload__img-close')) {
      var file = e.target.parentNode.dataset.file;
      for (var i = 0; i < imgArray.length; i++) {
        if (imgArray[i].name === file) {
          imgArray.splice(i, 1);
          break;
        }
      }
      e.target.parentNode.parentNode.remove();
    }
  });
}

document.addEventListener("DOMContentLoaded", function() {
  const fileInput = document.getElementById('fileLibro');
  const fileNamePreview = document.getElementById('file-name-preview');
  const clearLink = document.getElementById('clear-file');

  fileInput.addEventListener('change', function() {
      const file = this.files[0];
      if (file) {
          fileNamePreview.textContent = 'Archivo seleccionado: ' + file.name;
          clearLink.style.display = 'block'; 
      } else {
          fileNamePreview.textContent = ''; 
      }
  });

  clearLink.addEventListener('click', function(event) {
    event.preventDefault();
    fileInput.value = null; 
    fileNamePreview.textContent = ''; 
    clearLink.style.display = 'none'; 
  });
});


document.addEventListener('DOMContentLoaded', function() {
  const form = document.querySelector('form.formulario-historia');

  form.addEventListener('submit', function(event) {
      // Obtener todos los campos requeridos
      const requiredFields = form.querySelectorAll('[required]');

      // Verificar si alguno de los campos requeridos está vacío
      let isEmpty = false;
      let emptyField = '';

      requiredFields.forEach(function(field) {
          if (field.value.trim() === '') {
              isEmpty = true;
              emptyField = field.getAttribute('id');
          }
      });

      // Mostrar un alert si hay campos vacíos
      if (isEmpty) {
          alert('El campo ' + emptyField + ' no puede estar vacío');
          event.preventDefault(); // Detener el envío del formulario
      }
  });
});
