document.addEventListener("DOMContentLoaded", function() {
    var primerBoton = document.querySelector('.seccion-borde-lateral form.glsr-form .glsr-button');
    // console.log(primerBoton);

    primerBoton.addEventListener("click", function(){
        location.reload();
    });

    // Obtener una lista de todos los elementos de revisión
    var reviewElements = document.querySelectorAll('.glsr-review');

reviewElements.forEach(function(reviewElement) {
  var contentElement = reviewElement.querySelector('.glsr-review-content .glsr-tag-value');
  var avatarElement = reviewElement.querySelector('.glsr-review-avatar');
  var authorElement = reviewElement.querySelector('.glsr-review-author .glsr-tag-value');

  var combinedElement = document.createElement('div');
  combinedElement.classList.add('combined-review');
  combinedElement.appendChild(avatarElement);
  combinedElement.appendChild(authorElement);
  combinedElement.appendChild(contentElement);

  var dateElement = document.createElement('div');
  dateElement.classList.add('fecha');
  var reviewDate = reviewElement.querySelector('.glsr-review-date').cloneNode(true); // Clonar el elemento de fecha
  dateElement.appendChild(reviewDate);

  reviewElement.appendChild(combinedElement);
});

    // Mover la línea reviewElement.appendChild(dateElement); fuera del bucle forEach
    reviewElements.forEach(function(reviewElement) {
    var dateElement = reviewElement.querySelector('.fecha');
    reviewElement.appendChild(dateElement);
    });

    //traducciones
    var noReviewsElement = document.querySelector('p.glsr-no-margins');
    var textareaElements = document.querySelectorAll('.single-product .glsr-field.glsr-field-textarea');
    console.log(textareaElements);
    textareaElements.placeholder = "Prueba...";
    if (noReviewsElement) {
    noReviewsElement.textContent = "No hay comentarios aún. Sé el primero en escribir uno.";
    }

 
});
