document.addEventListener('DOMContentLoaded', function() {
  // Obtén una referencia al botón de acceso
  var loginButton = document.getElementById('wp-submit');
  
  var inputField = document.getElementById('user_login');
  var inputFieldPass = document.getElementById('user_pass');
  
  inputField.setAttribute('placeholder', 'Nombre de usuario');
  inputFieldPass.setAttribute('placeholder', 'Contraseña');
  
  // Crea un nuevo botón de sesión de invitado
  var guestButton = document.createElement('button');
  guestButton.textContent = 'Invitado';
  guestButton.classList.add('boton-publicar');
  guestButton.classList.add('btninv');
  guestButton.setAttribute('name', 'login_guest');
  
  // Inserta el nuevo botón antes del botón de acceso
  loginButton.parentNode.insertBefore(guestButton, loginButton);
  
  var submitButton = document.getElementById('wp-submit');
  if (submitButton) {
    submitButton.value = 'Acceder';
  }
  
  document.addEventListener('keypress', function(event) {
    if (event.key === 'Enter') {
        event.preventDefault(); 
        document.querySelector('input[type="submit"].button.button-primary').click();
    }
  });
  
  var loginPassword = document.querySelector('.login-password');
  
  var recoveryLink = document.createElement('a');
  var urlHome = window.location.origin;
  recoveryLink.href = urlHome + '/recuperar-pass';
  recoveryLink.textContent = 'He olvidado la contraseña';
  recoveryLink.classList.add('pass-remeber');
  
  loginPassword.appendChild(recoveryLink);
  
  // guestButton.addEventListener('click', function() {
    
  // //   var xhr = new XMLHttpRequest();
  // //   xhr.open('POST', '/crear-sesion-invitado'); 
  // //   xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  // //   xhr.onload = function() {
  // //     if (xhr.status === 200) {
  // //       alert('Sesión de invitado creada');
  // //       window.location.href = '/ ';
  // //     } else {
  // //       alert('Error al crear la sesión de invitado');
  // //     }
  // //   };
  // //   xhr.send();
  // });
  
  
  
  });
  
  jQuery.ajax({
    url: ajax_params.ajaxurl,
    type: 'POST',
    data: {
      action: 'get_existing_users'
    },
    success: function(response) {
      console.log(response);
    }
  });
  
  document.addEventListener('DOMContentLoaded', function () {
    var form = document.getElementById('submit');
    var vas_publicar = document.getElementById('boton-escitor')
    var cancelar = document.getElementById('boton-cancelar');
    var paso1 = document.getElementById('opciones-usuario');
    var paso2 = document.getElementById('opciones-escritor');
    var publicacion = 0; 
    let goBack = document.getElementById('go_back_registro');
    var recaptchaTextarea;
  
  // Lógica que se ejecuta después de 5 segundos
  setTimeout(function() {
      recaptchaTextarea = document.getElementById('g-recaptcha-response');
      // console.log(recaptchaTextarea);
  
      if (recaptchaTextarea) {
          recaptchaTextarea.setAttribute('required', 'required');
          // console.log(recaptchaTextarea);
          // console.log(recaptchaTextarea.value);
      }
  }, 5000);
  
  var usuario_exist = false;
  var correo_exist = false;
  
  function verificarCorreo() {
      var email = document.getElementById('email').value;
      correo_exist = false;
    
          jQuery.ajax({
              url: ajax_params.ajaxurl,
              type: 'POST',
              data: {
                  action: 'check_email_exists',
                  email: email
                  // username: username,
              },
              success: function(response) {
                  var data = JSON.parse(response);
                  console.log(data);
                  var emailajax = document.getElementById('email').value;
  
                  if (data.status === 'exists') {
                      if (data.email === emailajax) {
                          var email = data.email; 
                          console.log("Correo electrónico existe:", email);
                          correo_exist = true;
                      } 
                  }
              }
          });
      
  }

  function verificarUsuario(){
    var username = document.getElementById('username').value;
    usuario_exist = false;

    jQuery.post(
      ajaxurl,
      {
        action: 'check_username_exists',
        username: username
      },
      function(response) {
        var data = JSON.parse(response);
        console.log("Esta es la data: ", data);
        var usernameajax = document.getElementById('username').value;

        if (data.status === 'exists') {
          if (data.username === usernameajax){
            console.log("usuario existe", data.username);
            usuario_exist = true;
            console.log(usuario_exist);
          }
        }

      }
    );
  }
  
  document.getElementById('email').addEventListener('blur', function(){
    if (correo_exist == true){
      console.log("Correo existe", correo_exist);
    }else {
      console.log("Correo no existe",  correo_exist);
      correo_exist = false;
    }
  
    verificarCorreo();
  });

  
  document.getElementById('username').addEventListener('blur', function(){
     verificarUsuario();
  });
  
  document.getElementById('submit').addEventListener('click', function(event) {

    let validacion_correcta = true;
      if (usuario_exist || correo_exist) {
          event.preventDefault();
          if (usuario_exist) {
              Swal.fire({ title: "El nombre de usuario ya está en uso", text: "", icon: "error" });
              validacion_correcta = false;
          } else if (correo_exist) {
              Swal.fire({ title: "El correo electrónico ya está en uso", text: "", icon: "error" });
              validacion_correcta = false;
          }
      } 

      if (validacion_correcta) {
        Swal.fire({ title: "Registro exitoso", text: "¡Tu cuenta ha sido creada con éxito!", icon: "success" });
      }

  });
  var registro = document.getElementById("submit");
  document.getElementById('boton-escitor').addEventListener('click', function(event) {
    registro.disabled = true;
    registro.classList.add("disabled");
      if (usuario_exist) {
        Swal.fire({ title: "El nombre de usuario ya está en uso", text: "", icon: "error" });
        paso1.style.display = "flex";
        paso2.style.display = "none";
      } else if (correo_exist) {
        Swal.fire({ title: "El correo electrónico ya está en uso", text: "", icon: "error" });
        paso1.style.display = "flex";
        paso2.style.display = "none";
      }
  
  });
  
  
  document.getElementById('terms').addEventListener('click', function(event) {
    event.preventDefault();
    Swal.fire({
      title: "Términos y condiciones",
      html: " <strong> Acuerdo de Distribución de publicaciones en autoedición entre Siescribo Red Social, S.L. y el Escritor </strong><br>"+ 
  "<p>Por una parte, Siescribo Red Social, S.L., CIF: B16466138, con domicilio en Tenor Massini, 114 Atico 1 08028 <br> Barcelona, en adelante denominada La Editorial, y el Escritor, cuyos datos constan al darse <br> de alta en la web “siescribo.com”, se acuerda lo siguiente:</p><br><strong>1. Objeto del Acuerdo:</strong> El presente acuerdo tiene como objetivo establecer los términos y <br> condiciones bajo los cuales La Editorial publicará las obras literarias (cualesquiera que sean) escrita <br> por El Escritor, en formato digital y/o impreso, bajo el régimen de autoedición.<br>"+
  "<strong>2. Derechos de Autor:</strong>El Escritor declara ser el único titular de los derechos de autor de la obra <br> mencionada anteriormente, y garantiza a La Editorial que la misma no infringe los derechos de <br> propiedad intelectual de terceros. Asimismo, El Escritor se compromete a indemnizar y mantener <br> indemne a La Editorial de cualquier reclamación, demanda, o acción legal relacionada con la violación <br> de derechos de autor, plagio, o cualquier otra forma de infracción intelectual derivada de la obra.<br>"+
  "<strong>3. Normas de la Sociedad General de Autores:</strong> Ambas partes acuerdan cumplir con todas las <br> normativas y disposiciones establecidas por la Sociedad General de Autores y Editores (SGAE) u <br> cualquier otra entidad de gestión de derechos de autor competente en el territorio correspondiente.<br>"+
  "<strong>4. Responsabilidad del Escritor:</strong> El Escritor será el único responsable de cualquier reclamación <br> derivada de la violación de los derechos de privacidad, de imagen, o cualquier otro derecho de <br> terceros que pueda surgir como consecuencia de la publicación de la obra. El Escritor se compromete <br> a obtener todos los permisos, autorizaciones, y licencias necesarias para el uso de material protegido por derechos de terceros.<br>"+
  "<strong>5. Publicación y Distribución:</strong> La Editorial se compromete a publicar la obra en el formato acordado <br> con El Escritor, y a distribuirla en los canales de venta correspondientes, principalmente web <br> siescribo.com a través de formatos online válidos en la misma. El Escritor tendrá derecho a recibir un <br> porcentaje del precio de venta de cada ejemplar vendido, según lo acordado entre ambas partes.<br>"+
  "<strong>6. Vigencia del Acuerdo:</strong> Este acuerdo entrará en vigor en la fecha de su firma/aceptación términos <br> y condiciones web “siescribo”, por ambas partes y tendrá una duración indefinida, a menos que sea <br> rescindido por mutuo acuerdo o por incumplimiento de alguna de las partes. <br> En el momento en que el escritor se de baja en la web “siescribo.com” el contrato quedará sin efecto <br> con esa misma fecha.<br>"+
  "<strong>7. Condiciones Económicas:</strong> <strong>Ambas</strong> partes se comprometen a respetar lo indicado en el Anexo I de <br> este contrato: <em>“Condiciones Económicas”<em>. Dicho anexo forma parte íntegramente de este acuerdo.<br>"+
  "<strong>8. Ley Aplicable y Jurisdicción:</strong> Este acuerdo se regirá e interpretará de acuerdo con las leyes de <br> España, y cualquier disputa derivada del mismo será sometida a la jurisdicción de los tribunales <br> competentes de Barcelona.<br>"+
  "En prueba de conformidad, ambas partes aceptan el presente acuerdo: <br><br>"+
  "<br><br><div class='d-flex justify-content-between' > <div class='col-12 col-md-6 d-flex flex-column align-items-center'><img width='180px' height='60px' src='https://siescribo.com/wp-content/uploads/2024/03/cropped-Uno-04-1536x523.png'><br><strong>Siescribo Red Social, S.L.</strong></div>  <div class='col-12 col-md-6 d-flex flex-column align-items-center'><strong style='position:relative; '>Escritor</strong> <br><br> <p>Valida su firma al aceptar los términos y <br> condiciones en la web “siescribo.com” </p> </div> </div> <br><br>"+
  "<strong>Fecha: </strong> coincide con la fecha de alta del escritor en la web “siescribo,com” en el momento de aceptar <br> los términos y condiciones de la misma.<br>"+
  "Este acuerdo tiene validez a partir de la fecha indicada. <br><br>"+
  "<strong class='text-center'>Anexo I: Condiciones Económicas</strong><br>"+
  "El Escritor y los representantes de Siescribo Red Social, S.L., aceptan las siguientes nueve clausulas <br> respecto al cobro y pago de las publicaciones y todo lo relacionado con los derechos y deberes <br> económicos de las partes: <br>"+
  "1)	El cobro se acordará por cada “descarga” en el espacio virtual que el Escrito tenga registrado <br> en la web Siescribo.com, y por los servicios adicionales que se acuerden.<br>"+
  "2)	El precio del producto y por lo tanto de las descargas lo pondrá el Escritor cada vez <br> que suba un escrito, el mismo puede ser igual a “0”. El precio incluirá el coste neto más los <br> impuestos aplicables correspondientes.<br>"+
  "3) El cobro lo hará la sociedad Siescribo Red Social S.L. a través de los medios de pago que <br> figuren en la web Siescribo.com.<br>"+
  "4) Siescribo Red Social, S.L. abonará al escritor, a través de transferencia bancaria enviada a la <br> cuenta que el Escritor haya facilitado previamente, antes del día 15 del mes siguiente al que <br> se haya producido la descarga, el 80% del importe neto, incluyendo o deduciendo lo que <br> corresponda por los impuestos asociados. El porcentaje indicado puede variar por dos circunstancias: <br>"+
  "Este importe puede variar por dos circunstancias.<br>"+
  "- La primera es que el Escritor libremente se acoja a que su publicación figure en <br> el apartado de la web Siescribo.com como recomendado, en cuyo caso el <br> porcentaje que recibirá será de un 65%. <br>"+
  "- La segunda es que el Escritor se acoja a la posibilidad de <br> promociones/descuentos a través de cupones u otros sistemas, en cuyo caso el <br> precio final será el indicado por él, menos el correspondiente al importe en neto o <br> en porcentaje del descuento ofrecido en la web.<br>"+
  "5) El Escritor es responsable de liquidar los impuestos a la Hacienda Pública Española o al <br> organismo que corresponda y frente al que haya que responder. <br>"+
  "6) El Escritor es responsable de abonar a la Sociedad General de Autores el importe que corresponda por dicha publicación. <br>"+
  "7) El Escritor se hará cargo de los gastos de ISBN de cada publicación que realice.<br>"+
  "8) En caso de que la web reciba una reclamación relacionada con una publicación del Escritor <br> por plagio, ofensa, no respecto derecho intimidad o cualquier otra causa que afecte a <br> terceros, el Escritor se hace único responsable de resarcir económicamente al reclamante, <br> con independencia que dicha reclamación incluya o no a la web siescribo.com o la empresa <br> Siescribo Red social, S.L<br>"+
  "9) En caso de que un usuario se muestre insatisfecho con el producto recibido, el Escritor <br> acepta devolver la totalidad del dinero abonado por dicho usuario de la web Siescribo.com, <br> aceptando la política que Siescribo aplique de forma pública y que en general será la <br> devolución del importe íntegro abonado, si considera la, bajo su criterio, que la causa <br> invocada está suficientemente justificada. <br><br><br>"+

  "Nombre: 	José María de la Puente del Pozo<br>"+
  "Cargo: 	Administrador Único" ,    
      icon: "warning",
      width: "70em",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Acepto"
    }).then((result) => {
      if (result.isConfirmed) {
        document.getElementById('terms').checked = true;
        registro.disabled = false;
        registro.classList.remove("disabled");
        Swal.fire({
          text: "Términos y condiciones aceptadas",
          icon: "success"
        });
      }
    });
  });


    form.addEventListener('click', function (event) {
      
      var nombre = document.getElementById('nombre').value;
      var username = document.getElementById('username').value;
      var email = document.getElementById('email').value;
      // var valorEmail = document.getElementById('valor_email').value;
      var contrasena = document.getElementById('contrasena').value;
      var contrasena2 = document.getElementById('contrasena2').value;

      const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      // const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*(),."{}|\/])[A-Za-z\d@$!%*?&]{8,}$/;
      // const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*(),."{}|\/])[A-Za-z\d!@#$%^&*(),."{}|\/]{8,}$/;
      const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*(),."{}|\/_\-])[A-Za-z\d!@#$%^&*(),."{}|\/_\-]{8,}$/;
  
      // jQuery.ajax({
      //   url: ajax_params.ajaxurl,
      //   type: 'POST',
      //   data: {
      //     action: 'check_email_exists',
      //     email: email
      //   },
      //   success: function(response) {
      //     if (response === 'exists') {
      //       Swal.fire({ title: "El correo electrónico ya está registrado", text: "", icon: "error" });
      //       event.preventDefault();
      //     } 
      //   }
      // });
  
      var formValid = false; 
      let valor;
  
      var valorEmail = document.getElementById('valor_email').value;
      var validacion_correcta = true;
  
      if (nombre === "") {
          //swal("Introduzca un nombre", "", "error");
          Swal.fire({ title:"Introduzca un nombre",text:"",icon:"error"});
          event.preventDefault();
          validacion_correcta = false;
      } else if (username === "") {
          //swal("Introduzca un nombre de usuario", "", "error");
          Swal.fire({ title:"Introduzca un nombre de usuario",text:"",icon:"error"});
          event.preventDefault();
          validacion_correcta = false;
      } else if (email === "") {
          //swal("Introduzca un correo electrónico", "", "error");
          Swal.fire({ title:"Introduzca un correo electrónico",text:"",icon:"error"});
          event.preventDefault();
          validacion_correcta = false;
      } else if (!emailRegex.test(email)) {
        //swal("El correo electrónico no es válido", "", "error");
        Swal.fire({ title:"El correo electrónico no es válido",text:"",icon:"error"});
        event.preventDefault();
        validacion_correcta = false;
      // } else if(email !== "") {
        
  
      //   jQuery.ajax({
      //     url: ajax_params.ajaxurl,
      //     type: 'POST',
      //     data: {
      //       action: 'check_email_exists',
      //       email: email
      //     },
      //     success: function(response) {
      //       var data = JSON.parse(response);
      //       var usernameajax = document.getElementById('username').value;
      //       var emailajax = document.getElementById('email').value;
      //       if (data.status === 'exists') {
  
      //         console.log(usernameajax)
      //         if (data.username === usernameajax) {
      //           var username = data.username; 
      //           console.log("Nombre de usuario existe:", username);
      //           usuario_exist = true;
      //         } else {
      //           usuario_exist = false;
      //         }
              
      //         console.log(emailajax);
      //         if (data.email === emailajax) {
      //           var email = data.email; 
      //           console.log("Correo electrónico existe:", email);
      //           correo_exist = true;
      //           console.log("La var ants", correo_exist);
      //         }else{
      //           email_exist = false;
      //         }
    
      //       }
      //     }
      //   });
  
        
      //   if (usuario_exist == true){
      //     console.log("entre en codicion usuario");
      //     Swal.fire({ title:"El nombre de usuario ya esta en uso",text:"",icon:"error"});
      //     event.preventDefault();
      //   } else if (correo_exist == true){
      //     console.log("entre en condicion correo");
      //     Swal.fire({ title:"El correo electrónico ya esta en uso",text:"",icon:"error"});
      //     event.preventDefault();
      //   } else {
      //     console.log("entr en el ultimo paso");
      //     usuario_exist = false;
      //     email_exist = false;      
      //   }
      //   // alert("hola");
      // } 
      }else if (contrasena === "") {
          //swal("Introduzca una contraseña", "", "error");
          Swal.fire({ title:"Introduzca una contraseña",text:"",icon:"error"});
          event.preventDefault();
          validacion_correcta = false;
      } else if (contrasena.length < 8) {
          //swal("La contraseña debe contener al menos una mayúscula, una minúscula, un número, un caracter especial y al menos 8 caracteres", "", "error");
          Swal.fire({ title:"La contraseña debe contener al menos una mayúscula, una minúscula, un número, un caracter especial y al menos 8 caracteres",text:"",icon:"error"});
          event.preventDefault();
          validacion_correcta = false;
      } else if (!passwordRegex.test(contrasena)) {
        //swal("La contraseña debe contener al menos una mayúscula, una minúscula, un número, un caracter especial y al menos 8 caracteres", "", "error");
        Swal.fire({ title:"La contraseña debe contener al menos una mayúscula, una minúscula, un número, un caracter especial y al menos 8 caracteres",text:"",icon:"error"});
        event.preventDefault();
        validacion_correcta = false;
      } else if (contrasena !== contrasena2) {
          //swal("Las contraseñas no coinciden", "", "error");
          Swal.fire({ title:"Las contraseñas no coinciden",text:"",icon:"error"});
          event.preventDefault();
          validacion_correcta = false;
      } else if (recaptchaTextarea.value === ''){
        Swal.fire({ title:"Por favor valide el CAPTCHA",text:"",icon:"error"});
        validacion_correcta = false;
      }else  {
        // event.preventDefault();
        // Swal.fire({ title:"Su cuenta ha sido creada satisfactoriamente",text:"",icon:"success"});
        // form.click();

      

      }


        
     
      
  });
  

  
  
  // form.addEventListener( 'click', function(event)  {
  //   // event.preventDefault();
  //   // setTimeout(function() {
    
  //       console.log("usuario_exist:", usuario_exist);
  //       console.log("correo_exist:", correo_exist);
    
        
        
      
  // // }, 2000);
  
  // });
   

    vas_publicar.addEventListener("click", function(event){
      var nombre = document.getElementById('nombre').value;
      var username = document.getElementById('username').value;
      var email = document.getElementById('email').value;
      var contrasena = document.getElementById('contrasena').value;
      var contrasena2 = document.getElementById('contrasena2').value;
  
      const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      // const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
      // const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*(),."{}|\/])[A-Za-z\d!@#$%^&*(),."{}|\/]{8,}$/;
      const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*(),."{}|\/_\-])[A-Za-z\d!@#$%^&*(),."{}|\/_\-]{8,}$/;
      var validacion_correcta = true;
      
      
      if (nombre === "") {
          //swal("Introduzca un nombre", "", "error");
          Swal.fire({ title:"Introduzca un nombre",text:"",icon:"error"});
          event.preventDefault();
          var validacion_correcta = false;
      } else if (username === "") {
          //swal("Introduzca un nombre de usuario", "", "error");
          Swal.fire({ title:"Introduzca un nombre de usuario",text:"",icon:"error"});
          event.preventDefault();
          var validacion_correcta = false;
      } else if (email === "") {
          //swal("Introduzca un correo electrónico", "", "error");
          Swal.fire({ title:"Introduzca un correo electrónico",text:"",icon:"error"});
          event.preventDefault();
          var validacion_correcta = false;
      } else if (!emailRegex.test(email)) {
        //swal("El correo electrónico no es válido", "", "error");
        Swal.fire({ title:"El correo electrónico no es válido",text:"",icon:"error"});
        event.preventDefault();
        var validacion_correcta = false;
      } else if (contrasena === "") {
          //swal("Introduzca una contraseña", "", "error");
          Swal.fire({ title:"Introduzca una contraseña",text:"",icon:"error"});
          event.preventDefault();
          var validacion_correcta = false;
      } else if (contrasena.length < 8) {
          //swal("La contraseña debe contener al menos una mayúscula, una minúscula, un número, un caracter especial y al menos 8 caracteres", "", "error");
          Swal.fire({ title:"La contraseña debe contener al menos una mayúscula, una minúscula, un número, un caracter especial y al menos 8 caracteres",text:"",icon:"error"});
          event.preventDefault();
          var validacion_correcta = false;
      } else if (!passwordRegex.test(contrasena)) {
        //swal("La contraseña debe contener al menos una mayúscula, una minúscula, un número, un caracter especial y al menos 8 caracteres", "", "error");
        Swal.fire({ title:"La contraseña debe contener al menos una mayúscula, una minúscula, un número, un caracter especial y al menos 8 caracteres",text:"",icon:"error"});
        event.preventDefault();
        var validacion_correcta = false;
      } else if (contrasena !== contrasena2) {
          //swal("Las contraseñas no coinciden", "", "error");
          Swal.fire({ title:"Las contraseñas no coinciden",text:"",icon:"error"});
          event.preventDefault();
          var validacion_correcta = false;
          
      }else if (usuario_exist) {
        Swal.fire({ title: "El nombre de usuario ya está en uso", text: "", icon: "error" });
        
        var validacion_correcta = false;
      } else if (correo_exist) {
        Swal.fire({ title: "El correo electrónico ya está en uso", text: "", icon: "error" });
        var validacion_correcta = false;
        return;
      } else {
        publicacion = 1; 
        let submitFin = document.getElementById('submit');
        var validacion = true;
        
        var hiddenField = document.createElement('input');
        hiddenField.type = 'hidden';
        hiddenField.name = 'publicacion_value';
        hiddenField.value = publicacion;
        form.appendChild(hiddenField); 
  
        paso1.style.display = "none";
        paso2.style.display = "flex";
  
        submitFin.addEventListener('click', function(event){
          let cuentabancaria = document.getElementById('cuenta_bancaria').value; 
          let termscond = document.getElementById('terms').value; 

          if (cuentabancaria === "") {
            //swal("Introduzca una cuenta bancaria", "", "error");
            Swal.fire({ title:"Introduzca una cuenta bancaria",text:"",icon:"error"});
            event.preventDefault();
            // validacion = false;
            return false;
          }else {
            console.log("ya tengo valor");
          }

          if (termscond != "on") {
            //swal("Introduzca una cuenta bancaria", "", "error");
            Swal.fire({ title:"Lea los términos y condiciones",text:"",icon:"error"});
            event.preventDefault();
            validacion = false;
            return false;
          }

        //    if (validacion) {
        //   Swal.fire({ title: "Registro exitoso", text: "¡Tu cuenta ha sido creada con éxito!", icon: "success" }); 
        // }

        });
       

       

  
        vas_publicar.style.display = "none";
        cancelar.style.display = "block";
        jQuery.ajax({
          type: "POST",
          url: ajaxurl,
          data: {
              action: "procesar_publicacion",
              publicacion_value: publicacion
          },
          success: function(response) {
              console.log(response);
          },
          error: function(xhr, status, error) {
              console.error(xhr.responseText);
          }
        });
  
        
      
      }

      
      
    });
  
    goBack.addEventListener("click", function(){
      paso1.style.display = "flex";
      paso2.style.display = "none"; 
  
      vas_publicar.style.display = "block";
      cancelar.style.display = "none";
    });
  
  
    
    // goBack.addEventListener("click", function(){
    //   paso1.style.display = "flex";
    //   paso2.style.display = "none"; 
  
    //   vas_publicar.style.display = "block";
    //   cancelar.style.display = "none";
    // });
  
  
  });