jQuery(document).ready(function($) {
    //paginador
    const posts = document.querySelectorAll('.post');
    const itemsPerPage = 3;
    let currentPage = 1;
    
    const showPosts = (start, end) => {
        posts.forEach((post, index) => {
            post.style.display = (index >= start && index < end) ? 'block' : 'none';
        });
    };
    
    const updatePagination = () => {
        showPosts((currentPage - 1) * itemsPerPage, currentPage * itemsPerPage);
    };
    
    document.getElementById('btn_prev').addEventListener('click', () => {
        if (currentPage > 1) {
            currentPage--;
            updatePagination();
        }
    });
    
    document.getElementById('btn_next').addEventListener('click', () => {
        if (currentPage < Math.ceil(posts.length / itemsPerPage)) {
            currentPage++;
            updatePagination();
        }
    });
    
    // Mostrar los primeros 4 posts al cargar la página
    updatePagination();

});
