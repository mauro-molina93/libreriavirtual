// custom-followers.js

(function($) {
    // Función para manejar el clic en el botón de seguir/dejar de seguir
    $(document).on('click', '.follow-button', function(event) {
      event.preventDefault();
      var $button = $(this);
      var userId = $button.data('user-id');
      
      // Realizar la solicitud AJAX
      $.ajax({
        url: ajax_object.ajax_url,
        type: 'POST',
        data: {
          action: 'follow_user',
          user_id: userId
        },
        success: function(response) {
          // Actualizar la apariencia del botón según la respuesta
          if (response === 'followed') {
            $button.text('Siguiendo');
          } else if (response === 'unfollowed') {
            $button.text('Seguir');
          }
        }
      });
    });
  })(jQuery);