document.addEventListener("DOMContentLoaded", function() {

    const newmail = document.getElementById('emailinv');
    const billing_email = document.getElementById('billing_email');
    const billingFirstName = document.getElementById('billing_first_name');
    const billingLastName = document.getElementById('billing_last_name');
    const billingAddress = document.getElementById('billing_address_1');
    const billingCity = document.getElementById('billing_city');
    const billingCountry = document.getElementById('billing_country');
    const billing_postcode = document.getElementById('billing_postcode');
  
    
   if (newmail) { 
        newmail.addEventListener('input', function() {
            //alert(document.getElementById('select2-billing_country-container'));
            billing_email.value = newmail.value;
            billingFirstName.value = 'Invitado';
            billingLastName.value = 'Invitado';
            billingAddress.value = 'Invitado';
            billingCity.value = 'Madrid';
            billingCountry.value = 'ES';
            billing_postcode.value = '28001';
            
        });
    }

 

    const formxx = document.getElementById('change-password-form');

    if(formxx){
        formxx.addEventListener('submit', function(event) {
            const currentPassword = document.getElementById('current_password').value;
            const newPassword = document.getElementById('new_password').value;
            const confirmPassword = document.getElementById('confirm_password').value;

            if (!validarContrasena(newPassword) || !validarContrasena(confirmPassword)) {
                
                Swal.fire("Error", "La contraseña no cumple los requisitos de seguridad", "error");
                event.preventDefault();
            } 
        });
    }

    var botonComentario = document.getElementById("comentario-icono");
    var formularioComentarios = document.getElementById("form-reviews");
    var back = document.getElementById("back");
    var datos = document.getElementById("datos-producto");
    var ocultar1 = document.getElementById("ocultar1");
    var ocultar2 = document.getElementById("ocultar2");
    var ocultar3 = document.getElementById("ocultar3");
    var ocultar4 = document.getElementById("ocultar4");
    var ocultar5 = document.getElementById("ocultar5");

    // console.log(ocultar4);

    var sectionSmall = document.getElementById("seccion-pequena");
    var sectionBig = document.getElementById("seccion-grande");

    // var label = document.querySelector(".glsr-label-rating");
    // var span = label.querySelector("span");
    // span.textContent = "Tu puntuación general";
    
    // var textarea = document.querySelector(".glsr-label-textarea");
    // var spanTextarea = textarea.querySelector("span");
    // spanTextarea.textContent = "Tu reseña";

    // var button = document.querySelector(".glsr-button");
    // button.textContent = "Publicar";

    // var button = document.querySelector(".wp-block-button button");
    // var divToMoveInto = document.querySelector(".glsr-field-textarea");
    // var clonedButton = button.cloneNode(true);
    // divToMoveInto.appendChild(clonedButton);
    // button.remove();



    
    botonComentario.addEventListener("click", function() {
        formularioComentarios.style.display = "block";
        datos.style.display = "none";
        ocultar1.classList.remove("d-flex");
        ocultar3.classList.remove("d-flex");
        ocultar1.style.display = "none";
        ocultar2.style.display = "none";
        ocultar3.style.display = "none";
        ocultar4.style.display = "none";
        ocultar5.style.display = "none";

        sectionBig.classList.remove("col-md-5");
        sectionBig.classList.add("col-md-4");

        sectionSmall.classList.remove("col-md-4");
        sectionSmall.classList.add("col-md-5");

    });

    back.addEventListener("click", function() {
        formularioComentarios.style.display = "none";
        datos.style.display = "block";
        ocultar1.classList.add("d-flex");
        ocultar3.classList.add("d-flex");
        ocultar1.style.display = "block";
        ocultar2.style.display = "block";
        ocultar3.style.display = "block";
        ocultar4.style.display = "block";
        ocultar5.style.display = "block";

        sectionBig.classList.add("col-md-5");
        sectionBig.classList.remove("col-md-4");

        sectionSmall.classList.add("col-md-4");
        sectionSmall.classList.remove("col-md-5");

    });
});

jQuery(document).ready(function($) {

    var input = $('.search-field-top');
    if (input.length) {
        input.attr('placeholder', 'Buscar...');
    }

    $('.deletet').on('click', function(e){
     
        e.preventDefault();
        
      const post_id =document.getElementById('post_id').value;
      Swal.fire({
        title: "¿Está seguro que desea eliminar este libro?",
        showDenyButton: true,        
        confirmButtonText: "Eliminar",
        denyButtonText: `Cancelar`
      }).then((result) => {
       
        if (result.isConfirmed) {
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: {
                    action: 'delete_book',
                    post_id: post_id
                },
                success: function(response){
                    Swal.fire({title:"Libro eliminado correctamente",icon:"success"});
                    esperar();
                   window.location.reload()
                }
            });
        } 
      });

      
    });


    $('#publicaciones').on('click', function(e){       
        var user_id = document.getElementById('id_user').value;
      
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: {
              action: 'post_by_user', 
              user_id: user_id     
            },
            success: function(response) {
                Swal.fire({ html: response});
            }
          });
       
    });

    $('#seguidos').on('click', function(e){       
        var user_id = document.getElementById('id_user').value;
      
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: {
              action: 'follwed_by_user', 
              user_id: user_id     
            },
            success: function(response) {
                Swal.fire({ html: response});
            }
          });
       
    });
    
    $('.more-friends').on('click', function(e){       
        
        var user_id = document.getElementById('id_user').value;
      
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: {
              action: 'more_friends', 
              user_id: user_id     
            },
            success: function(response) {
                Swal.fire({ html: response});
            }
          });
       
    });

    $('#seguidores').on('click', function(e){       
        var user_id = document.getElementById('id_user').value;
      
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: {
              action: 'follwer_by_user', 
              user_id: user_id     
            },
            success: function(response) {
                Swal.fire({ html: response});
            }
          });
       
    });

});

document.addEventListener('DOMContentLoaded', function() {
      
    document.getElementById("filtroSelect").addEventListener("input", function() {
        var input = this.value.toLowerCase();
        var options = this.options;
        
        for (var i = 0; i < options.length; i++) {
          var optionText = options[i].text.toLowerCase();
          
          if (optionText.indexOf(input) !== -1) {
            options[i].style.display = "block";
          } else {
            options[i].style.display = "none";
          }
        }
      });
      
   var current_password = document.getElementById('current_password');
   var new_password = document.getElementById('new_password');
   var confirm_password = document.getElementById('confirm_password');
  
   if (current_password) {
       var showPasswordButton = document.createElement('button');
       showPasswordButton.setAttribute('type', 'button');
       showPasswordButton.setAttribute('id', 'togglePassword');
       showPasswordButton.innerHTML = '<img src="https://libreriavirtual.ingeniuscuba.com/wp-content/themes/buddyx-child/assents/svg/eye.png" alt="" width="25px" height="25px">';

       
       var passwordContainer = document.createElement('div');
       passwordContainer.classList.add('password-input-password');
 
       current_password.parentNode.insertBefore(passwordContainer, current_password);
       passwordContainer.appendChild(current_password);
       passwordContainer.appendChild(showPasswordButton);
       
       showPasswordButton.addEventListener('click', function() {
           var passwordType = current_password.getAttribute('type') === 'password' ? 'text' : 'password';
           current_password.setAttribute('type', passwordType);
       });
   }

   if (new_password) {
    var showPasswordButton1 = document.createElement('button');
    showPasswordButton1.setAttribute('type', 'button');
    showPasswordButton1.setAttribute('id', 'togglePassword');
    showPasswordButton1.innerHTML = '<img src="https://libreriavirtual.ingeniuscuba.com/wp-content/themes/buddyx-child/assents/svg/eye.png" alt="" width="25px" height="25px">';

    
    var passwordContainer = document.createElement('div');
    passwordContainer.classList.add('password-input-password');

    new_password.parentNode.insertBefore(passwordContainer, new_password);
    passwordContainer.appendChild(new_password);
    passwordContainer.appendChild(showPasswordButton1);
    
    showPasswordButton1.addEventListener('click', function() {
        var passwordType = new_password.getAttribute('type') === 'password' ? 'text' : 'password';
        new_password.setAttribute('type', passwordType);
    });
}

if (confirm_password) {
    var showPasswordButton2 = document.createElement('button');
    showPasswordButton2.setAttribute('type', 'button');
    showPasswordButton2.setAttribute('id', 'togglePassword');
    showPasswordButton2.innerHTML = '<img src="https://libreriavirtual.ingeniuscuba.com/wp-content/themes/buddyx-child/assents/svg/eye.png" alt="" width="25px" height="25px">';

    
    var passwordContainer = document.createElement('div');
    passwordContainer.classList.add('password-input-password');

    confirm_password.parentNode.insertBefore(passwordContainer, confirm_password);
    passwordContainer.appendChild(confirm_password);
    passwordContainer.appendChild(showPasswordButton2);
    
    showPasswordButton2.addEventListener('click', function() {        
        var passwordType = confirm_password.getAttribute('type') === 'password' ? 'text' : 'password';
        confirm_password.setAttribute('type', passwordType);
    });
}

     
});

function validarContrasena(contrasena) {
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    return regex.test(contrasena);
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function esperar() {
    for (let i = 0; i < 5; i++) {
        await sleep(i * 1000);
    }
   
}

const numeroInput = document.getElementById('telefonob');

        numeroInput.addEventListener('input', (event) => {
            const inputValue = event.target.value;
            const numericValue = inputValue.replace(/[^0-9]/g, ''); 
            event.target.value = numericValue; 
        });

       
        numeroInput.addEventListener('keydown', (event) => {
            if (event.key === 'Delete' || event.key === 'Backspace') {
                return; 
            }
            if (!/[0-9]/.test(event.key)) {
                event.preventDefault();
            }
        });