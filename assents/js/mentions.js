// jQuery(document).ready(function($) {
//     $('#site-reviews-content-glsr_938c6d6b').autocomplete({
//         source: function(request, response) {
//             $.ajax({
//                 url: autocomplete_data.ajax_url,
//                 method: 'POST',
//                 dataType: 'json',
//                 data: {
//                     action: 'autocomplete_users',
//                     term: request.term,
//                     nonce: autocomplete_data.nonce
//                 },
//                 success: function(data) {
//                     response(data);
//                 }
//             });
//         },
//         minLength: 1,
//         select: function(event, ui) {
//             var content = $('#site-reviews-content-glsr_938c6d6b').val();
//             var atIndex = content.lastIndexOf('@');
//             var newText = content.substring(0, atIndex) + '@' + ui.item.value + ' ' + content.substring(atIndex + 1);
//             $('#site-reviews-content-glsr_938c6d6b').val(newText);
//             return false;
//         }
//     });

//     // Agregar evento al campo para mostrar la lista desplegable cuando se ingresa "@" y actualizar la búsqueda en tiempo real
//     $('#site-reviews-content-glsr_938c6d6b').on('input', function() {
//         var content = $(this).val();
//         if (content.includes('@')) {
//             $(this).autocomplete('search', content.slice(content.lastIndexOf('@') + 1));
//         }
//     });
// });


jQuery(document).ready(function($) {
    $('.glsr-review-form').on('submit', function(e) {
        e.preventDefault(); // Prevenir el envío del formulario por defecto
        
        var formData = $(this).serialize(); // Serializar los datos del formulario
        
        $.ajax({
            url: ajaxurl, // URL del punto de entrada AJAX de WordPress
            type: 'POST',
            data: formData + '&action=insert_notification', // Agregar una acción para identificar el tipo de solicitud
            success: function(response) {
                console.log('Notification inserted successfully');
            },
            error: function(error) {
                console.error('Error inserting notification:', error);
            }
        });
    });
});
