document.addEventListener("DOMContentLoaded", function() {
   
    setTimeout(function() {
        // var billingPhoneInput = document.querySelectorAll('.wc-block-components-address-form-wrapper #billing .wc-block-components-text-input.wc-block-components-address-form__phone input[type="tel"]#billing-phone');
        var billingPhoneInput = document.getElementById('billing-phone');
        // console.log(billingPhoneInput);

        var telefonob = document.getElementById('telefonob');
        // console.log(telefonob);

        if(billingPhoneInput){
            billingPhoneInput.addEventListener('input', function(event) {
                // Obtener el valor actual del campo
                var valor = event.target.value;
        
                // Filtrar solo números y el signo '+'
                var nuevoValor = valor.replace(/[^\d+]/g, '');
        
                // Actualizar el valor del campo
                event.target.value = nuevoValor;
            });
        }

        if(telefonob){
            telefonob.addEventListener('input', function(event) {
                // Obtener el valor actual del campo
                var valor = event.target.value;
        
                // Filtrar solo números y el signo '+'
                var nuevoValor = valor.replace(/[^\d+]/g, '');
        
                // Actualizar el valor del campo
                event.target.value = nuevoValor;
            });
        }

       

    }, 2000);

    

    setTimeout(function() {
    let buttonSubmit = document.getElementById('place_order');
    let campoCorreo = document.getElementById('emailinv');
    var expresionCorreo = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

	if (campoCorreo) {
    let buttonParent = buttonSubmit.parentNode;

    // Clonar el botón con un nuevo ID
    let newButton = buttonSubmit.cloneNode(true);
    newButton.id = "nuevo_id"; 
    newButton.removeAttribute('name');
    newButton.type = 'button';

    // Agregar el botón clonado al mismo padre del botón original
    buttonParent.appendChild(newButton);

    buttonSubmit.style.display = 'none';

   
    newButton.addEventListener('click', function() {
        if (campoCorreo.value === "") {
            Swal.fire({
                title: "Debes escribir un correo electrónico.",
                text: "",
                icon: "error"
            });   
        } else if (!expresionCorreo.test(campoCorreo.value)) {
            Swal.fire({
                title: "Debes escribir un correo electrónico válido.",
                text: "",
                icon: "error"
            });  
        } else {
            newButton.style.display = 'none';
            buttonSubmit.style.display = 'block';

            setTimeout(function(){
                buttonSubmit.click();
            }, 100);
        }
    });
		
	}
}, 4000);

});


