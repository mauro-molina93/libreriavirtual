jQuery(document).ready(function($) {


  jQuery('input[type="checkbox"][name="users[]"]').change(applyFilters);
  jQuery('input[type="checkbox"][name="idiomas[]"]').change(applyFilters);
  jQuery('input[type="checkbox"][name="temas[]"]').change(applyFilters);
  jQuery('input[type="checkbox"][name="categorias[]"]').change(applyFilters);

  $('#min-price').change(applyFilters);
  $('#max-price').change(applyFilters);
  jQuery('#start-date').change(applyFilters);
  jQuery('#end-date').change(applyFilters);
 
  $('#filter-price').click(function() {    
    applyFilters
  });

  $('#btn-filter').click(function() {
    $('#clear-filters').show();
  });

  $('#clear-filters').click(function() {
    $('#clear-filters').hide();
  });




$('#clear-filters').click(function() {
    $('input[type="checkbox"]').prop('checked', false);
    $('#min-price').val('');
    $('#max-price').val('');
    $('#start-date').val('');
    $('#end-date').val('');
    
   applyFilters();

});

});

function applyFilters() {
    var selectedUsers = [];

    jQuery('input[type="checkbox"][name="users[]"]:checked').each(function() {
      selectedUsers.push(jQuery(this).val());
    });
  
    var minPrice = jQuery('#min-price').val();
    var maxPrice = jQuery('#max-price').val();
    var selectedIdiomas = [];
    jQuery('input[type="checkbox"][name="idiomas[]"]:checked').each(function() {
      selectedIdiomas.push(jQuery(this).val());
    });
    var selectedTemas = [];
    jQuery('input[type="checkbox"][name="temas[]"]:checked').each(function() {
      selectedTemas.push(jQuery(this).val());
    });
    var selectedCategorias = [];
    jQuery('input[type="checkbox"][name="categorias[]"]:checked').each(function() {
      selectedCategorias.push(jQuery(this).val());
    });
  
    var startDate = jQuery('#start-date').val();
    var endDate = jQuery('#end-date').val();

    jQuery.ajax({
      url: customAjax.ajaxurl,
      type: 'POST',
      data: {
        action: 'filtros_aplicados',
        user_ids: selectedUsers,
        min_price: minPrice,
        max_price: maxPrice,
        idiomas: selectedIdiomas,
        temas: selectedTemas,
        categorias: selectedCategorias,
        start_date: startDate,
        end_date: endDate
      },
      success: function(response) {
        console.log(response);
        if (response.success) {
          jQuery('#productos-container').html(response.html);
        } else {          
        Swal.fire({title:response.message ,icon:"info"});
        }
      },
      error: function(xhr, status, error) {
        console.error(xhr.responseText);
      }
    });
  }

  
  /*
 
  jQuery(document).ready(function($) {
    jQuery('input[type="checkbox"][name="users[]"]').change(function() {
      var selectedUsers = [];
  
      jQuery('input[type="checkbox"][name="users[]"]:checked').each(function() {
        selectedUsers.push(jQuery(this).val());
      });

      let paginador = document.getElementById('paginador');
      let menu = document.getElementById('menu-filtros');
  
      if (selectedUsers.length > 0) {
        jQuery.ajax({
          url: customAjax.ajaxurl,
          type: 'POST',
          data: {
            action: 'load_products_by_user',
            user_ids: selectedUsers
          },
          success: function(response) {
            if (response.success) {
              jQuery('#productos-container').html(response.html);
              paginador.style.display = 'none';
            } else {
              console.log(response.message);
              //jQuery('#productos-container').html(response.message);
              Swal.fire({title:response.message ,icon:"success"});
              paginador.style.display = 'none';
              // menu.style.position = 'relative';
            //   document.write(response.message);
            }
          },
          error: function(xhr, status, error) {
            console.error(xhr.responseText);
          }
        });
      }
    });
  });
  
  jQuery(document).ready(function($) {
    jQuery('#price-filter-form').submit(function(event) {
      event.preventDefault();
  
      var minPrice = jQuery('#min-price').val();
      var maxPrice = jQuery('#max-price').val();
      
      let paginador = document.getElementById('paginador');
  
      jQuery.ajax({
        url: customAjax.ajaxurl,
        type: 'POST',
        data: {
          action: 'filter_products_by_price',
          min_price: minPrice,
          max_price: maxPrice
        },
        success: function(response) {
          if (response.success) {
            jQuery('#productos-container').html(response.html);
            paginador.style.display = 'none';
          } else {
            console.log(response.message);
            Swal.fire({title:response.message ,icon:"success"});
          }
        },
        error: function(xhr, status, error) {
          console.error(xhr.responseText);
        }
      });
    });
  });

jQuery(document).ready(function($) {
    $('input[name="idiomas[]"]').change(function() {
        var selectedIdiomas = [];
        $('input[name="idiomas[]"]:checked').each(function() {
            selectedIdiomas.push($(this).val());
        });

        
      let paginador = document.getElementById('paginador');

        $.ajax({
            url: customAjax.ajaxurl,
            type: 'POST',
            data: {
                action: 'filter_products_by_idioma',
                idiomas: selectedIdiomas
            },
            success: function(response) {
                if (response.success) {
                    $('#productos-container').html(response.html);
                    // alert('Productos filtrados por idioma');
                } else {
                    console.log(response.message);
                    Swal.fire({title:response.message ,icon:"success"});
                    // alert('Hubo un problema al filtrar los productos por idioma');
                }
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    });
});

jQuery(document).ready(function($) {
    $('input[name="temas[]"]').change(function() {
        var selectedTemas = [];
        $('input[name="temas[]"]:checked').each(function() {
            selectedTemas.push($(this).val());
        });

        
      let paginador = document.getElementById('paginador');

        $.ajax({
            url: customAjax.ajaxurl,
            type: 'POST',
            data: {
                action: 'filter_products_by_tema',
                temas: selectedTemas
            },
            success: function(response) {
                if (response.success) {
                    $('#productos-container').html(response.html);
                    // alert('Productos filtrados por tema');
                } else {
                    console.log(response.message);
                    Swal.fire({title:response.message ,icon:"success"});
                    // alert('Hubo un problema al filtrar los productos por tema');
                }
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    });
});


jQuery(document).ready(function($) {
    $('input[name="categorias[]"]').change(function() {
        var selectedCategorias = [];
        $('input[name="categorias[]"]:checked').each(function() {
            selectedCategorias.push($(this).val());
        });

        
      let paginador = document.getElementById('paginador');

        $.ajax({
            url: customAjax.ajaxurl,
            type: 'POST',
            data: {
                action: 'filter_products_by_categoria',
                categorias: selectedCategorias
            },
            success: function(response) {
                if (response.success) {
                    $('#productos-container').html(response.html);
                    // alert('Productos filtrados por categoría');
                } else {
                    console.log(response.message);
                    Swal.fire({title:response.message ,icon:"success"});
                    // alert('Hubo un problema al filtrar los productos por categoría');
                }
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    });
});


jQuery(document).ready(function($) {
    $('#filter-form').submit(function(event) {
        event.preventDefault(); 

        
      let paginador = document.getElementById('paginador');

        var formData = $(this).serialize(); 
        $.ajax({
            url: customAjax.ajaxurl,
            type: 'POST',
            data: formData + '&action=filter_products_by_dates', 
            success: function(response) {
                if (response.success) {
                    $('#productos-container').html(response.html);
                    // alert('Productos filtrados por fechas');
                } else {
                    console.log(response.message);
                    Swal.fire({title:response.message ,icon:"success"});
                    // alert('Hubo un problema al filtrar los productos por fechas');
                }
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    });
});

*/
