document.addEventListener("DOMContentLoaded", function() {

    var mensajerecomendado = document.getElementById("mensajerecomendado");
    var toogle = document.getElementById("toggle-recomendados");

    if(toogle){

 
    toogle.addEventListener('change', function() {
        if (this.checked) {
            mensajerecomendado.style.display = "block";
        } else {
            mensajerecomendado.style.display = "none";
        }
      });
      
    }


    document.getElementById('imageUpload').addEventListener('change', function (e) {
        var imagePreview = document.getElementById('imagePreview');
        imagePreview.style.backgroundImage = 'url("' + URL.createObjectURL(e.target.files[0]) + '")';
    });


    var profileForm = document.getElementById('update-profile-user');
    var cuentaBancariaInput = document.getElementById('cuenta_bancaria');
    let cuentaBancariaInputValue = 0;
   
    cuentaBancariaInput.addEventListener('input', function() {
        cuentaBancariaInputValue = cuentaBancariaInput.value;
    });

    console.log(cuentaBancariaInput.value);

    profileForm.addEventListener('click', function(event) {

        if (cuentaBancariaInput.value === '') {
            event.preventDefault(); 
            Swal.fire({ title:"Error",text:"Introduzca una cuenta bancaria",icon:"error"});
            //swal("Error", "Introduzca una cuenta bancaria", "error");
        }

    });
});


