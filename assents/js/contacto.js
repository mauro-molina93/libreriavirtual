jQuery(document).ready(function($) {
    var userData = {};
    let nameUsuario = document.querySelector('.wpcf7-text');
    let correoUsuario = document.querySelector('.wpcf7-email');
  
    console.log(nameUsuario);
    console.log(correoUsuario);
  
    
    $.ajax({
      url: ajaxurl,
      type: 'POST',
      data: {
        action: 'get_user_info'
      },
      success: function(response) {
        userData = JSON.parse(response); 
        console.log('Email:', userData.email); 
        console.log('Name:', userData.name); 
        nameUsuario.value = userData.name;
        correoUsuario.value = userData.email;
      }
    });
  });