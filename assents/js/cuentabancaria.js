jQuery(document).ready(function($) {
    // Realizar la solicitud AJAX al cargar la página

    let tabs = document.getElementById('tab-publicar');

    $.ajax({
        url: cuentaBancariaAjax.ajax_url,
        type: 'post',
        data: {
            action: 'obtener_cuenta_bancaria',
            nonce: cuentaBancariaAjax.nonce
        },
        success: function(response) {
                // console.log(response);

                //     var tipoDeDato = typeof response;
                //     console.log("Tipo de dato de response:", tipoDeDato);
                //     console.log("Y tienen un lenght de", response.length);

            // if (response == "") {
            //     alert("hola");
            // }

            // if (typeof response === "string" && response.length === 2) {
            //     alert("hola");
            // }


            tabs.addEventListener('click', function() {
            
                if (typeof response === "string" && response.length === 2) {
                    Swal.fire({
                        title: "No puedes publicar un libro porque aun no tienes cuenta bancaria, por favor digita una en la página de configuraciones",
                        text: "",
                        icon: "error",
                        confirmButtonText: "Aceptar"
                    }).then((result) => {
                        if (result.isConfirmed) {
                            console.log('Cuenta bancaria:', response);
                            location.reload();
                        }
                    });
                }
               
                console.log('Cuenta bancaria:', response);

            });
        },
        error: function(xhr, status, error) {
            // Manejar errores aquí, por ejemplo, imprimir en la consola
            console.error('Error al obtener la cuenta bancaria:', error);
        }
    });

   
});
