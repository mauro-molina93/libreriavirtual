document.addEventListener("DOMContentLoaded", function() {
    let btnNext = document.getElementById('next-editar');
    let btnPrev = document.getElementById('prev-editar');
    let cancelar = document.getElementById('back_step2');

    let paso3 = document.getElementById('paso3');
    let paso2 = document.getElementById('paso2-1');


    // btnNext.addEventListener("click", function(){
    //     paso3.style.display = "block";
    //     paso2.style.display = "none";
    //     btnPrev.style.display = "block";
    // });

    btnPrev.addEventListener("click", function(){
        paso3.style.display = "none";
        paso2.style.display = "block";
        btnPrev.style.display = "none";
    });

    //Ajax
    let urlfondo;

    jQuery(document).ready(function($) {
        if (editar_vars.is_edit_page) {
            function obtenerMiniaturaPorAjax(product_id) {
                return new Promise(function(resolve, reject) {
                    var data = {
                        'action': 'obtener_miniatura_ajax', 
                        'product_id': product_id 
                    };

                    $.post(editar_vars.ajax_url, data, function(response) {
                        resolve(response);
                    }).fail(function() {
                        reject("Error al obtener la miniatura por Ajax");
                    });
                });
            }

            obtenerMiniaturaPorAjax(editar_vars.product_id).then(function(url) {
                urlfondo = url;
              // Aquí puedes hacer lo que necesites con urlfondo, ya que estás dentro de la promesa
                // Por ejemplo, puedes llamar a una función que use urlfondo
                miFuncionQueUsaUrlFondo(urlfondo);
            }).catch(function(error) {
                console.error(error);
            });
        }
    });

   
    //Carga de imagen destacada  desaparecer_cargando-editar //
    const INPUT_FILE = document.querySelector('#upload-files-editar');
    const INPUT_CONTAINER = document.querySelector('#upload-container-editar');
    const FILES_LIST_CONTAINER = document.querySelector('#files-list-container-editar');
    
    const desaparecer_cargando = document.getElementById('desaparecer_cargando-editar');
    const eliminarh = document.getElementById('eliminar-h-editar');
    const eliminarHContainer = document.getElementById('eliminar-h-container-editar');
    // const eliminarh2 = document.getElementById('eliminar-h2');
    const FILE_LIST = [];

    let UPLOADED_FILES = [];

    const multipleEvents = (element, eventNames, listener) => {
        const events = eventNames.split(' ');
       
        events.forEach(event => {
          element.addEventListener(event, listener, false);
        });
    };

      
    function miFuncionQueUsaUrlFondo(url) {
        console.log("Esta función usa urlfondo:", url);
        console.log(FILE_LIST.length);
        if (FILE_LIST.length == 0) {
            console.log("sssss: ", url);
            INPUT_CONTAINER.style.background = `url(${url})`;
            console.log(INPUT_CONTAINER);
            // INPUT_CONTAINER.style.background = 'url(' + url + ')';
            // INPUT_CONTAINER.style.background = `url(${url}) no-repeat center center`;

            INPUT_CONTAINER.classList.add('active');
            document.getElementById('overlay-editar').style.backgroundColor = 'rgba(0, 0, 0, 0.5)'; 
            document.getElementById('overlay-editar').style.position = 'absolute';
            document.getElementById('overlay-editar').style.width = '100%';
            document.getElementById('overlay-editar').style.height = '100%';
            document.getElementById('overlay-editar').style.top = '0';
            document.getElementById('overlay-editar').style.left = '0';
            document.getElementById('overlay-editar').style.zIndex = '1'; 
            document.getElementById('overlay-editar').style.display = 'block'; 
            desaparecer_cargando.style.display = 'none';
            eliminarHContainer.style.display = 'block';
    
        }

        vista = document.getElementById('visualizar-editar');
  
        vista.addEventListener('click', function(){
          if (FILE_LIST.length == 0 ) {
            window.open(url); 
          }
        });
        
    }

    const previewImages = () => {
        if (FILE_LIST.length > 0) {
            const lastAddedFile = FILE_LIST[FILE_LIST.length - 1]; 
            INPUT_CONTAINER.style.backgroundImage = `url(${lastAddedFile.url})`;
            INPUT_CONTAINER.classList.add('active');
            document.getElementById('overlay-editar').style.backgroundColor = 'rgba(0, 0, 0, 0.5)'; 
            document.getElementById('overlay-editar').style.position = 'absolute';
            document.getElementById('overlay-editar').style.width = '100%';
            document.getElementById('overlay-editar').style.height = '100%';
            document.getElementById('overlay-editar').style.top = '0';
            document.getElementById('overlay-editar').style.left = '0';
            document.getElementById('overlay-editar').style.zIndex = '1'; 
            document.getElementById('overlay-editar').style.display = 'block'; 
            desaparecer_cargando.style.display = 'none';
            eliminarHContainer.style.display = 'block';
    
            console.log(lastAddedFile);
            document.getElementById('visualizar-editar').addEventListener('click', () => {
              
              if (FILE_LIST.length > 0) {
                let lastAddedFilevi = FILE_LIST.pop();
                FILE_LIST.push(lastAddedFilevi); 
    
                window.open(lastAddedFilevi.url); 
              } 
            });
            
        } else {
            INPUT_FILE.value = '';
            INPUT_CONTAINER.style.backgroundImage = 'none'; 
            INPUT_CONTAINER.classList.remove('active');
            document.getElementById('overlay-editar').style.backgroundColor = 'transparent'; 
            
        }
    };

    
   

    const fileUpload = () => {
        if (FILES_LIST_CONTAINER) {
          multipleEvents(INPUT_FILE, 'click dragstart dragover', () => {
            INPUT_CONTAINER.classList.add('active');
          });
        
          multipleEvents(INPUT_FILE, 'dragleave dragend drop change blur', () => {
            INPUT_CONTAINER.classList.remove('active');
          });
        
          INPUT_FILE.addEventListener('change', () => {
            const files = [...INPUT_FILE.files];
          
            files.forEach(file => {
              const fileURL = URL.createObjectURL(file);
              const fileName = file.name;
              if (!file.type.match("image/")){
                alert(file.name + " is not an image");
              } else {
                const uploadedFiles = {
                  name: fileName,
                  url: fileURL,
                };
      
                FILE_LIST.push(uploadedFiles);
              }
            });
          
            previewImages();
            UPLOADED_FILES = document.querySelectorAll(".js-remove-image");
            removeFile();
          }); 
        }
      };


    const removeFile = () => {
        UPLOADED_FILES = document.querySelectorAll(".js-remove-image");
        
        if (UPLOADED_FILES) {
          UPLOADED_FILES.forEach(image => {
            image.addEventListener('click', function() {
              const fileIndex = this.getAttribute('data-index');
      
              FILE_LIST.splice(fileIndex, 1);
              previewImages();
              removeFile();
            });
          });
        } else {
          INPUT_FILE.value = ''; 
        }
    };

    eliminarh.addEventListener('click', function() {
            FILE_LIST.splice(0, FILE_LIST.length);
            previewImages();
            desaparecer_cargando.style.display = 'block';
            eliminarHContainer.style.display = 'none';
            document.getElementById('overlay-editar').style.display = 'none';

    });
    

//      // Open modal with the specified image URL
//     const openModal = (imageUrl) => {
//     const modal = document.getElementById('modal');
//     const modalImage = document.getElementById('modal-image');
  
//     modalImage.src = imageUrl;
//     modal.style.display = 'block';
//     document.querySelector('.close').addEventListener('click', closeModal);
//   };
  
//   // Close the modal
//   const closeModal = () => {
//     const modal = document.getElementById('modal');
//     const modalImage = document.getElementById('modal-image');
  
//     modal.style.display = 'none';
//     modalImage.src = '';
//   };

    const fileInput = document.getElementById('fileLibro');
    const fileNamePreview = document.getElementById('file-name-preview');
    const clearLink = document.getElementById('clear-file');

    fileInput.addEventListener('change', function() {
        const file = this.files[0];
        if (file) {
            fileNamePreview.textContent = 'Archivo seleccionado: ' + file.name;
            clearLink.style.display = 'block'; 
        } else {
            fileNamePreview.textContent = ''; 
        }
    });

    clearLink.addEventListener('click', function(event) {
        event.preventDefault();
        fileInput.value = null; 
        fileNamePreview.textContent = ''; 
        clearLink.style.display = 'none'; 
    });
  

    fileUpload();
    removeFile();




    //Validaciones
    const nextPublicar = document.getElementById('next-editar');

    function validarLibro(event){
      var nombre = document.getElementById("nombre-libro").value;
      let desc = document.getElementById("desc-libro").value;
      let categoriaNombre = document.getElementById("categoria_nombre").value;
      let tema = document.getElementById("tema").value;
      let idioma = document.getElementById("idioma").value;
      let correo = document.getElementById("correo").value;
      let uploadfiles2 = document.getElementById('upload-files2');
  
      const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  
      if (nombre.trim() === "") {
          //swal("Seleccione un título para su libro", "", "error");
          Swal.fire({ title:"Seleccione un título para su libro",text:"",icon:"error"});
          event.preventDefault();
          return false;
      } else if (nombre.trim().length > 50) {
        Swal.fire({ title:"El título no puede tener más de 50 caracteres",text:"",icon:"error"});
        return false;
      }else if (desc.trim() === "") {
          //swal("Introduzca un Resumen", "", "error");
          Swal.fire({ title:"Introduzca un Resumen",text:"",icon:"error"});
          return false;
      } else if (categoriaNombre.trim() === "") {
          //swal("Seleccione un Género Literario", "", "error");
          Swal.fire({ title:"Seleccione un Género Literario",text:"",icon:"error"});
          return false;
      } else if (tema.trim() === "") {
        //swal("Seleccione un tema", "", "error");
        Swal.fire({ title:"Seleccione un tema",text:"",icon:"error"});
        return false;
      }else if (idioma.trim() === "") {
        //swal("Seleccione un idioma", "", "error");
        Swal.fire({ title:"Seleccione un idioma",text:"",icon:"error"});
        return false;
      }else if (correo.trim() === "") {
        //swal("Introduzca su correo electrónico", "", "error");
        Swal.fire({ title:"Introduzca su correo electrónico",text:"",icon:"error"});
        return false;
      }else if (!emailRegex.test(correo)) {
        //swal("El correo electrónico no es válido", "", "error");
        Swal.fire({ title:"El correo electrónico no es válido",text:"",icon:"error"});
        event.preventDefault();
      } else { 
        return true; 
      }
    
    }

    nextPublicar.addEventListener('click', function(event) {
      var nombre = document.getElementById("nombre-libro").value;
      if (!validarLibro(event)) {
        event.preventDefault(); 
      }  else {
        paso3.style.display = "block";
        paso2.style.display = "none";
        btnPrev.style.display = "block";
      }
      
    });


    //validar paso2
    var publicarLibro = document.getElementById("editar-libro");

    publicarLibro.addEventListener('click', function(event) {
      event.preventDefault(); 
      
      if (validarLibroPasoDos()) {
        enviarDatosDelFormulario();
      }
    });
    
    function validarLibroPasoDos() {
      let catp = document.getElementById('catp').value;
      let precio = document.getElementById('precio').value;
      let fileLibro = document.getElementById('fileLibro');
      var archivo = fileLibro.files[0];
    
      if (catp.trim() === "") {
        
        Swal.fire({ title:"Seleccione Categoría de pago",text:"",icon:"error"});
        return false;
      }
    
      if (precio.trim() === "") {
        //swal("Recuerda ponerle precio al libro", "", "error");
        Swal.fire({ title:"Recuerda ponerle precio al libro",text:"",icon:"error"});
        return false;
      }
  
      if (fileLibro.files.length === 0 && fileLibro.disabled == false) {
        Swal.fire({ title:"Debe adjuntar el Libro o escrito a publicar (PDF, EPUB)",text:"",icon:"error"});
   
        return false;
      }
      
      if (fileLibro.files.length !=0) {
        var extension = archivo.name.split('.').pop().toLowerCase();
        if (extension !== "pdf" && extension !== "epub") {
            Swal.fire({
                title: "El archivo adjuntado debe ser PDF o EPUB",
                text: "",
                icon: "error"
            });
            return false;
        }
      }
    
      publicarLibro.disabled = true;
      return true;
    }
    
    function enviarDatosDelFormulario() {
      document.getElementById("formulario-libro1").submit(); 
    }

    //categoria de pago
    let catp = document.getElementById("catp");
    let precio = document.getElementById("precio");

   catp.addEventListener('click', function(){

      if (catp.value === "libredepago") {
        precio.disabled = true;
        precio.classList.add("disabled");
        precio.value = "0.00"
      }else {
        precio.disabled = false;
        precio.classList.remove("disabled");
      }

   });

   //calcular precio
   let precioInput = document.getElementById("precio");
   const precioProducto = document.getElementById('precio-producto');
   let datosEliminar = document.getElementById('datos_eliminar');
   let descuentoEliminar = document.getElementById('datos_eliminar-2');
   var toggleRecomendados = document.getElementById('toggle-recomendados');
   var agregarRecomendados = document.querySelector('.agregar-recomendados');
   var precioFinalEliminar = document.getElementById('precio_final_eliminar');
   console.log(precioFinalEliminar);
  
    const precioTotal = document.getElementById('precio-total');
    const descuentoPrecioProducto = document.getElementById('descuento-precioproducto');

   precioInput.addEventListener("input", function() {
    obtenerDatosIVA();
    calcularPrecio(ivaValue, descuento, descuento_recomendados);
  });

  function obtenerDatosIVA() {
    jQuery.ajax({
        url: ajaxurl,
        method: 'POST',
        data: {
            action: 'obtener_datos_iva'
        },
        success: function(response) {
            if (response.success) {
                console.log(response);
                ivaValue = parseFloat(response.ivaValue); 
                descuento = parseFloat(response.descuento);
                descuento_recomendados = parseFloat(response.descuento_recomendados);
                console.log('Valor del IVA obtenido:', ivaValue);
                console.log('Valor del descuento:', descuento);
                console.log('Valor del descuento recomendados:', descuento_recomendados);
                // console.log('Tipo de dato del valor del IVA:', typeof ivaValue)
                calcularPrecio(ivaValue, descuento, descuento_recomendados); 
            }
        },
        error: function() {
            console.log('Error en la solicitud AJAX.');
        }
    });
}

obtenerDatosIVA();

   function inicializarValoresDelCalculo() {
    precioProducto.innerHTML = "$0.00";
    descuentoPrecioProducto.innerHTML = "$0.00";
    precioTotal.innerHTML = "$0.00"
  }
  
  function calcularPrecio(ivaValue, descuento, descuento_recomendados) {
    let precioOut = parseFloat(precioInput.value);
  
    precioProducto.innerHTML = '$' + precioOut.toFixed(2);
  
    let iba = precioOut - ( (precioOut * ivaValue)/100); 
    console.log("El calculito del precio de salida: ", precioOut );
  
    let descuentoCalculo = 0;
    let total = 0; 
  
    if (toggleRecomendados.checked === true) {
        descuentoCalculo = iba * (descuento_recomendados/100);
        console.log(iba, (descuento_recomendados/100), descuentoCalculo);
        console.log(toggleRecomendados.checked);
    } else if (toggleRecomendados.checked === false) {
        descuentoCalculo = iba * (descuento/100);
        console.log(toggleRecomendados.checked);
    } else {
      descuentoCalculo = 0;
    }
  
    total = iba - descuentoCalculo;
  
    descuentoCalculo = descuentoCalculo.toFixed(2);
    iba = (precioOut * ivaValue).toFixed(2); 
    total = total.toFixed(2);
  
    let ibafinal = 0;
    ibafinal = ((precioOut * ivaValue)/100).toFixed(2);
  
    descuentoPrecioProducto.innerText = "$" + descuentoCalculo + " + iva: " + "$" + ibafinal;
    precioTotal.innerText = "$" + total;

    datosEliminar.style.display = "none";
    descuentoEliminar.style.display = "none";
    precioFinalEliminar.style.display = "none";
  }

  function sendImagesToServer(images) {
    var formData = new FormData();
    for (var i = 0; i < images.length; i++) {
        formData.append('imagenes[]', images[i]);
    }

    jQuery.ajax({
        url: ajax_object.ajaxurl,
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function(response) {
            // console.log(response);
            console.log(formData);
            // alert("llego");
        },
        error: function(xhr, status, error) {
            // Maneja los errores de la petición AJAX aquí
            console.log(error);
            // alert("error al enviar datos");
        }
    });
  }

  ImgUpload();
  
  function ImgUpload() {
    // alert("entro en la funcion ");
    var imgWrap = "";
    var imgArray = [];
  
    var inputFiles = document.querySelectorAll('.upload__inputfile');
    // console.log("Input files", inputFiles);
    inputFiles.forEach(function(input) {
      input.addEventListener('change', function(e) {
        imgWrap = this.closest('.upload__box').querySelector('.upload__img-wrap');
        var maxLength = 5;
  
        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        var iterator = 0;
        filesArr.forEach(function(f, index) {
          if (!f.type.match('image.*')) {
            return;
          }
  
          if (imgArray.length >= maxLength) {
            //swal('Solo se permiten un máximo de ' + maxLength + ' imágenes.', "", "error");
            Swal.fire({ title:'Solo se permiten un máximo de ' + maxLength + ' imágenes.',text:"",icon:"error"});
        
            return false;
          } else {
            var len = 0;
            for (var i = 0; i < imgArray.length; i++) {
              if (imgArray[i] !== undefined) {
                len++;
              }
            }
            if (len >= maxLength) {
              //swal('Solo se permiten un máximo de ' + maxLength + ' imágenes.', "", "error");
              Swal.fire({ title:'Solo se permiten un máximo de ' + maxLength + ' imágenes.',text:"",icon:"error"});
        
              return false;
            } else {
              imgArray.push(f);
  
              var reader = new FileReader();
              reader.onload = function(e) {
                var html = "<div class='upload__img-box'><div style='background-image: url(" + e.target.result + ")' data-number='" + document.querySelectorAll(".upload__img-close").length + "' data-file='" + f.name + "' class='img-bg'><div class='upload__img-close'></div></div></div>";
                imgWrap.insertAdjacentHTML('beforeend', html);
                iterator++;
              }
              reader.readAsDataURL(f);
            }
          }

          sendImagesToServer(imgArray);
        });
      });
    });
  
    document.body.addEventListener('click', function(e) {
      if (e.target.classList.contains('upload__img-close')) {
        var file = e.target.parentNode.dataset.file;
        for (var i = 0; i < imgArray.length; i++) {
          if (imgArray[i].name === file) {
            imgArray.splice(i, 1);
            break;
          }
        }
        e.target.parentNode.parentNode.remove();
      }
    });
  }

});
