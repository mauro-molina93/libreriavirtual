jQuery(document).ready(function($) {

    $('.logouticon').on('click', function() {

    $.ajax({
      url: ajaxurl,
      type: 'POST',
      data: {
          action: 'logout_guest'
      },
      success: function(response) {
        //swal("Gracias por visitarnos", "", "success");
        Swal.fire({ title:"Gracias por visitarnos",text:"",icon:"success"});     
        window.location.href = "/";
      }
  });



  });

    $('.heart-icon').each(function() {
      var heartIcon = $(this);
      var producto_id = heartIcon.data('producto-id');
      var contadorDiv = $('#contador-' + producto_id);
      var valor = 0;

      
  
      var likeState = localStorage.getItem('like_' + producto_id);
      
      if (likeState === 'liked') {
        heartIcon.removeClass('fa-heart-o');
        heartIcon.addClass('fas fa-heart');
        valor = 1;
      }
  
      heartIcon.on('click', function() {
        if (heartIcon.hasClass('fa-heart-o')) { 
          heartIcon.removeClass('fa-heart-o');
          heartIcon.addClass('fas fa-heart');
          //valor = 1;
        } else {
          heartIcon.removeClass('fas fa-heart');
          heartIcon.addClass('fa fa-heart-o');
          //valor = 0;
        }
  
        // Almacenar el estado del like en localStorage
       // localStorage.setItem('like_' + producto_id, valor);

       $.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'get_current_user_id'
        },
        success: function(user_id) {
            console.log('ID del usuario:', user_id);

            if (user_id === '0') {
                window.location.href = '/login';
            } 
        }
    });
  
        $.ajax({
            url: ajaxurl,
          type: 'POST',
          data: {
            action: 'actualizar_contador',
            producto_id: producto_id,
            valor: valor
          },
          success: function(response) {
            if(response.includes("error")) {
              //swal("Error", "Para dar Me gusta debe loguearse", "error");
              Swal.fire({ title:"Error",text:"Para dar Me gusta debe loguearse",icon:"error"});     
             }else{
                  contadorDiv.html(response);
             }
          }
        });

        $.ajax({
          url: ajaxurl,
          type: 'POST',
          data: {
              action: 'obtener_contador_likes',
              producto_id: producto_id
          },
          success: function(response) {
              contadorDiv.html(response);
          }
      });
      });
    });

    $('.heart-like').on('click', function() {
      
      story_id = $("[name='post_id']").attr("value");
     
      let fechaActual = new Date();
      let horasASumar = 24;
      fechaActual.setHours(fechaActual.getHours() + horasASumar);
      let options = { weekday: 'short', day: '2-digit', month: 'short', year: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit', timeZoneName: 'short' };
      let fechaFormateada = fechaActual.toLocaleString('en-US', options);

      var nombreCookie = getCookieValue("user");
      if(nombreCookie==story_id){
        Swal.fire({ title:"",text:"Ya le dio Me gusta a esta historia",icon:"error"});
      }else{
        document.cookie = "user="+story_id+"; expires="+fechaFormateada+"; path=/;";
        $.ajax({
          url: ajaxurl,
        type: 'POST',
        data: {
          action: 'send_like_story_chat',
          story_id: story_id
        },
        success: function(response) {         
            Swal.fire({ title:"",text:"Gracias por su Me gusta",icon:"success"});   
        }
      });
      }
            
     
         
        });

    $('.guardar-producto').on('click', function() {
      var producto_id = $(this).data('producto-id');
      var $icon = $(this).find('i');
  
      $icon.toggleClass('fa-bookmark-o fa-bookmark');
  
      $.ajax({
          url: ajaxurl,
          type: 'POST',
          data: {
              action: 'guardar_producto',
              producto_id: producto_id,
          },
          success: function(response) {
            if(response.includes("error")) {
              //swal("Error", "Para guardar el libro no puede ser un usuario invitado", "error");
              Swal.fire({ title:"Error",text:"Para guardar el libro no puede ser un usuario invitado",icon:"error"});   
             }else{
              console.log("MENSAJE DE CONSOLA: " + response);
             }
          },
          error: function(xhr, status, error) {
              console.log('Error en la solicitud AJAX:', error);
              $icon.toggleClass('fa-bookmark-o fa-bookmark');
          }
      });
  });
  
  });
//ACCEDER A LA COOKIE Y MOSTRAR
function getCookieValue(nombre) {
  var cookies = document.cookie.split(";"); // Divide la cadena de cookies en un array
  for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i].trim(); // Elimina los espacios en blanco al principio y al final
      if (cookie.startsWith(nombre + "=")) {
          return cookie.substring(nombre.length + 1); // Retorna el valor de la cookie
      }
  }
  return null; // Si no se encuentra la cookie, retorna null
}