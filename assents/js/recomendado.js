
jQuery(document).ready(function($){

    
    $('.aprobar').on('click', function(e){
        e.preventDefault();
        
        var solic_id = $(this).data('solic_id');

        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: {
                action: 'aprobar_solicitud',
                solic_id: solic_id
            },
            success: function(response){
               alert("Aprobado con éxito!");
               window.location.reload()
            }
        });
    });

    $('.denegar').on('click', function(e){
        e.preventDefault();
        
        var solic_id = $(this).data('solic_id');

        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: {
                action: 'denegar_solicitud',
                solic_id: solic_id
            },
            success: function(response){
               alert("Rechazado con éxito!");
               window.location.reload()
            }
        });
    });
});