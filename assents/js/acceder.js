document.addEventListener('DOMContentLoaded', function() {
     //alert("Estoy funcionadno");

      
    var passwordField = document.getElementById('user_pass');
   
    if (passwordField) {
        var showPasswordButton = document.createElement('button');
        showPasswordButton.setAttribute('type', 'button');
        showPasswordButton.setAttribute('id', 'togglePassword');
        showPasswordButton.innerHTML = '<img src="https://libreriavirtual.ingeniuscuba.com/wp-content/uploads/2024/03/eye.png" alt="" width="25px" height="25px">';

        
        var passwordContainer = document.createElement('div');
        passwordContainer.classList.add('password-input');
  
        passwordField.parentNode.insertBefore(passwordContainer, passwordField);
        passwordContainer.appendChild(passwordField);
        passwordContainer.appendChild(showPasswordButton);
        
        showPasswordButton.addEventListener('click', function() {
            var passwordType = passwordField.getAttribute('type') === 'password' ? 'text' : 'password';
            passwordField.setAttribute('type', passwordType);
        });
    }
});




