<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package buddyx
 */

namespace BuddyX\Buddyx;

get_header();

buddyx()->print_styles( 'buddyx-content' );
buddyx()->print_styles( 'buddyx-sidebar', 'buddyx-widgets' );

$default_sidebar = get_theme_mod( 'sidebar_option', buddyx_defaults( 'sidebar-option' ) );

?>

	<?php do_action( 'buddyx_sub_header' ); ?>
	
	<?php do_action( 'buddyx_before_content' ); ?>

    <?php require get_stylesheet_directory() . '/menu-lateral.php'; ?>

	<main id="primary" class="site-main no-scroll">
        <div class="container-configuracion">
            <section class="title-page mt-2">
                <h2><?php echo esc_html_e('Notificaciones', 'libreriasocial'); ?></h2>
                <hr class="separador">
            </section>

            <section class="dashboard">
          
			<?php
				if ( class_exists( 'BuddyPress' ) && is_user_logged_in() && bp_is_active( 'notifications' ) ) {
					global $bp;
					?>
					<div class="user-notifications">
						
						<div id="bp-notify" class="bp-header-submenu bp-dropdown bp-notify">
							<?php
							if ( bp_has_notifications(
								array(
									'user_id'  => bp_loggedin_user_id(),
									'per_page' => 10,
									'max'      => 10,
									'is_new'   => null
								)
							) ) :
								?>
								<div class="bp-dropdown-inner">
									<?php
										// global $wpdb;

														
										// $query = $wpdb->prepare(
										// 	"SELECT user_id, item_id, secondary_item_id
										// 	FROM {$wpdb->prefix}bp_notifications
										// 	WHERE component_name = 'likes'
										// 	AND user_id = %d",
										// 	get_current_user_id()
										// );
										// $likes_data = $wpdb->get_results($query);
									
										// foreach ($likes_data as $like) {
										// 	if ($like !== null && isset($like->item_id)) { 
										// 	$id_noti = bp_get_the_notification_item_id();
										// 	$post_id = $like->item_id;
										// 	$user_id = $like->secondary_item_id;
													
										// 	$post_title = get_the_title($post_id);
										// 	$post_thumbnails = get_the_post_thumbnail($post_id);
													
										// 	$user_info = get_userdata($user_id);
										// 	$user_avatar = get_avatar_url($user_id);
										// 	$user_profile_url = bp_core_get_user_domain($user_id);
													
										// $user_display_name = $user_info ? $user_info->display_name : 'Usuario desconocido';
										// 	}
										// echo "<br>like<br>";
										// var_dump($like);
										// 				}	
															
									$users = get_users();
									while ( bp_the_notifications() ) :
										bp_the_notification();
										
										?>
										<?php
										
										$notification_description = bp_get_the_notification_description();
										$sender_start = strpos($notification_description, 'de ') + 14;
										$sender_name = substr($notification_description, $sender_start);
										$user_id_current =  wp_get_current_user();
													
										$aux = strip_tags($sender_name); 
										$aux = trim($sender_name);
										$aux = substr($aux, 0, -4); 
										foreach ($users as $user) {
															
											$user_id = $user->ID; 
											$user_login = $user->user_login; 
											$user_email = $user->user_email; 
											$user_display_name = $user->display_name; 
											$avatar = get_avatar($user_id, 32);
													
											$user_display_name = trim($user->display_name);

											 // Mostrar la información del usuario
											// echo "<p>User ID: $user_id</p>";
											// echo "<p>User Login: $user_login</p>";
											// echo "<p>User Email: $user_email</p>";
											// echo "<p>User Display Name: $user_display_name</p>";
											// echo "<p>User Avatar: $avatar</p>";
											// echo "<hr>";

											$user_display_name = trim($user->display_name);
											// var_dump($user_display_name);
											$aux_lower = strtolower($aux);
											$user_display_name_lower = strtolower($user_display_name);

											if ($aux_lower == $user_display_name_lower) {
												$si = true;
												$user_initiator = $user_id;
											}else {
												$si = false;
											}
										}
									

											?>
										<div class="notifications-item">
											<!-- <p>Componente: <?php $comoponents = bp_the_notification_component_name(); ?></p> -->
											<?php 
												$component_name = bp_get_the_notification_component_name();
												$component_action = bp_get_the_notification_component_action();
												
												// Realizar la comparación
												if ($component_name === 'friends') {
													global $wpdb;
														$notification_id = bp_get_the_notification_id();

														$query = $wpdb->prepare(
															"SELECT user_id, secondary_item_id, item_id
															FROM {$wpdb->prefix}bp_notifications
															WHERE id = %d",
															$notification_id
														);
														
														$results = $wpdb->get_results($query);

														$user_id = intval($results[0]->user_id);
														$secondary_item_id = intval($results[0]->secondary_item_id);
														$item_id = intval($results[0]->item_id);

														$usuario_mando = get_userdata($item_id);
														$sender_user_name_mando = $usuario_mando ? $usuario_mando->display_name : 'Usuario desconocido';
														$sender_user_avatar_mando = get_avatar_url($item_id);
														$perfil_usuario =bp_core_get_user_domain($usuario_mando->ID);
											
													
													?>
													<div class="components">
															<div class="notifications_informations">
																
																<div class="user_notification">
																	<div>
																		<a href="<?php echo $perfil_usuario; ?>">
																			<img src="<?php echo $sender_user_avatar_mando; ?>" alt="" class="avatar">
																		</a>
																	</div>
																	
																	<div class="d-flex">
																		<p><?php esc_html_e("Tienes una nueva solicitud de amistad de ")?><a href="<?php echo $perfil_usuario; ?>"><span class="elusuario"><?php echo  $sender_user_name_mando; ?></span></a></p>
																		<p class="mx-2" style="color: #DBD9D7;"><?php echo " | "; ?></p>
																		<p><?php bp_the_notification_time_since(); ?></p>
																	</div>
																</div>
															
															</div>
															<div class="notification_actions">
															<?php
															$current_user_id = get_current_user_id(); 

															$other_user_id = $user_initiator; 
															
															$is_friends = friends_check_friendship( $current_user_id, $other_user_id );
															
															if ( $is_friends ) {
																?>
																<span class="son_amigos"><?php esc_html_e("Son amigos"); ?></span>
																<?php
															} else {
																?>

																<form method="post">
																	<input type="hidden" name="bp_notify_id" value="<?php echo bp_get_the_notification_id(); ?>">
																	<input type="hidden" name="initiator_user_id" value="<?php echo $user_initiator; ?>">
																	<button type="submit" name="accept_friend" class="btn btn-primary"><?php esc_html_e("Aceptar", "libreriasocial"); ?></button>
																</form>
																
																
																<form method="post">
																	<input type="hidden" name="delete_bp_notify_id" value="<?php echo bp_get_the_notification_id(); ?>">
																	<button type="submit" name="delete_friend" class="btn btn-danger"><?php esc_html_e("Rechazar", "libreriasocial"); ?></button>
																</form>

																<?php
															}
															?>
																

															</div>
														</div>
													<?php } elseif ($component_name === 'follower') {
														?>
														<?php
														global $wpdb;
														$table_name = $wpdb->prefix . 'bp_notifications';

														$item_id = bp_get_the_notification_item_id();
										
														$query = $wpdb->prepare(
															"SELECT user_login 
															FROM $wpdb->users 
															WHERE ID = (
																SELECT item_id 
																FROM $table_name 
																WHERE id = %d
															)",
															$item_id
														);
														$user_login = $wpdb->get_var($query);

														$user_info = get_userdata($item_id);

														if ($user_info) {
															$nombre_usuario_following = $user_info->display_name;
															$avatar_following =  get_avatar($user_info->ID, 32);
															$user_link = bp_core_get_user_domain($user_info->ID);
														} 

														?>
															<div class="components">
																<div class="notifications_informations">
																	<?php $notification_description = bp_get_the_notification_description(); ?>
																	<p><?php echo $notification_description; ?></p>

																	<div class="user_notification">
																		<a href="<?php echo $user_link; ?>">
																			<?php echo $avatar_following; ?>
																		</a>
																		<div class="d-flex">
																			<p><?php esc_html_e("Te esta siguiendo ")?><a href="<?php echo $user_link; ?>"><span class="elusuario"><?php echo  $nombre_usuario_following;; ?></span></a></p>
																				<p class="mx-2" style="color: #DBD9D7;"><?php echo " | "; ?></p>
																				<p><?php bp_the_notification_time_since(); ?></p>
																		</div>
																	</div>
																	
																</div>	
																
																<div class="notification_actions">
																	<?php echo do_shortcode('[follow_notification_sender]'); ?>
																</div>
															
															</div>							
														<?php
													} elseif ($component_name === 'likes'){
														$post_id = bp_get_the_notification_item_id();
														$user_id = bp_get_the_notification_secondary_item_id();
														
														$post_title = get_the_title($post_id);
														$post_thumbnails = get_the_post_thumbnail($post_id);
														$post_url = get_the_permalink($post_id);
														
														$user_info = get_userdata($user_id);
														$user_display_name = $user_info ? $user_info->display_name : 'Usuario desconocido';
														$user_avatar = get_avatar_url($user_id);
														$user_profile_url = bp_core_get_user_domain($user_id);
														
													
														?>
													<div class="components">
														
															<div class="notifications_informations">
																<div class="user_notification">
																	<a href="<?php echo $user_profile_url; ?>">
																		<img src="<?php echo $user_avatar; ?>" alt="" class="avatar">
																	</a>
																	<div class="d-flex">
																		<p><?php esc_html_e("El usuario ") ?><a href="<?php echo $sender_user_profile_link; ?>"><span class="elusuario"><?php echo $user_display_name; ?></span></a><?php esc_html_e(" le ha dado me gusta al libro "); ?><a class="permalink_post" href="<?php echo $post_url; ?>"><?php echo $post_title; ?></a></p>
																		<p class="mx-2" style="color: #DBD9D7;"><?php echo " | "; ?></p>
																		<p><?php bp_the_notification_time_since(); ?></p>
																	</div>
																</div> 
															</div>

															<div class="notification_action">
																<div class="imagen-producto">
																	<a href="<?php echo $post_url; ?>">
																		<?php echo $post_thumbnails; ?>
																	</a>
																</div> 
															</div>
														
													</div>	
														<?php //}
													} elseif ($component_name === 'comentariolibro') {
														global $wpdb;
														$notification_id = bp_get_the_notification_id();

														$query = $wpdb->prepare(
															"SELECT user_id, secondary_item_id, item_id
															FROM {$wpdb->prefix}bp_notifications
															WHERE id = %d",
															$notification_id
														);
														
														$results = $wpdb->get_results($query);
														
														
														$user_id = intval($results[0]->user_id);
														$secondary_item_id = intval($results[0]->secondary_item_id);
														$item_id = intval($results[0]->item_id);
														

														$post_name = get_the_title($item_id);
														$post_link = get_permalink($item_id);
														$post_thumbnail = get_the_post_thumbnail($item_id);

														$user_info = get_userdata($secondary_item_id);
														$sender_user_name = $user_info ? $user_info->display_name : 'Usuario desconocido';
														$sender_user_avatar = get_avatar_url($secondary_item_id);
														$sender_user_profile_link = bp_core_get_user_domain($secondary_item_id);
													
														// var_dump($item_id);

														if ($secondary_item_id == 0){
															continue;
														}
														?>

														<div class="components">
															<div class="notifications_informations">
																<div class="user_notification">
																	<a href="<?php echo $sender_user_profile_link; ?>">
																		<img src="<?php echo $sender_user_avatar; ?>" class="avatar" alt="">
																	</a>

																<div class="d-flex">
																	<p><?php esc_html_e("El usuario  ")?><a href="<?php echo $sender_user_profile_link; ?>"><span class="elusuario"><?php echo  $sender_user_name; ?></span></a><?php esc_html_e(" ha comentado el libro "); ?><a class="permalink_post" href="<?php echo $post_link; ?>"><?php echo $post_name; ?></a></p>
																			<p class="mx-2" style="color: #DBD9D7;"><?php echo " | "; ?></p>
																			<p><?php bp_the_notification_time_since(); ?></p>
																		</div>
																	</div>
															</div>

															<div class="notification_action">
																<div class="imagen-producto">
																	<a href="<?php echo $post_link; ?>">
																		<?php echo $post_thumbnail; ?>
																	</a>
																</div>
															</div>
															
														</div>

														<?php
													} elseif ($component_name === 'respuestacomentario'){
														global $wpdb;
														$notification_id = bp_get_the_notification_id();

														$query = $wpdb->prepare(
															"SELECT user_id, secondary_item_id, item_id
															FROM {$wpdb->prefix}bp_notifications
															WHERE id = %d",
															$notification_id
														);
														
														$results = $wpdb->get_results($query);
														
														
														$user_id = intval($results[0]->user_id);
														$secondary_item_id = intval($results[0]->secondary_item_id);
														$item_id = intval($results[0]->item_id);

														$user_info = get_userdata($secondary_item_id);
														$sender_user_name = $user_info ? $user_info->display_name : 'Usuario desconocido';
														$sender_user_avatar = get_avatar_url($secondary_item_id);
														$sender_user_profile_link = bp_core_get_user_domain($secondary_item_id);

							
														$post_name = get_the_title($item_id);
														$post_link = get_permalink($item_id);
														$post_thumbnail = get_the_post_thumbnail($item_id);
														
														?>
														<div class="components">
															<div class="notifications_informations">
																<div class="user_notification">
																	<a href="<?php echo $sender_user_profile_link; ?>">
																		<img src="<?php echo $sender_user_avatar; ?>" class="avatar" alt="">
																	</a>

																<div class="d-flex">
																	<p><?php esc_html_e("El usuario  ")?><a href="<?php echo $sender_user_profile_link; ?>"><span class="elusuario"><?php echo  $sender_user_name; ?></span></a><?php esc_html_e(" le ha respondido su comentario en el libro "); ?><a class="permalink_post" href="<?php echo $post_link; ?>"><?php echo $post_name; ?></a></p>
																			<p class="mx-2" style="color: #DBD9D7;"><?php echo " | "; ?></p>
																			<p><?php bp_the_notification_time_since(); ?></p>
																		</div>
																	</div>
															</div>

															<!-- <div class="notification_action">
																<div class="imagen-producto">
																	<a href="<?php echo $post_link; ?>">
																		<?php echo $post_thumbnail; ?>
																	</a>
																</div>
															</div> -->
															
														</div>

														<?php
													} elseif($component_name == 'compra_realizada'){
														global $wpdb;
														$notification_id = bp_get_the_notification_id();

														$query = $wpdb->prepare(
															"SELECT user_id, secondary_item_id, item_id
															FROM {$wpdb->prefix}bp_notifications
															WHERE id = %d",
															$notification_id
														);
														
														$results = $wpdb->get_results($query);

														$user_id = intval($results[0]->user_id);
														$secondary_item_id = intval($results[0]->secondary_item_id);
														$item_id = intval($results[0]->item_id);

														$user_info = get_userdata($item_id);
														$sender_user_name = $user_info ? $user_info->display_name : 'Usuario desconocido';
														$sender_user_avatar = get_avatar_url($item_id);
														$sender_user_profile_link = bp_core_get_user_domain($item_id);

														$post_name = get_the_title($secondary_item_id);
														$post_link = get_permalink($secondary_item_id);
														$post_thumbnail = get_the_post_thumbnail($secondary_item_id);
													
													?>
													<div class="components">
															<div class="notifications_informations">
																<div class="user_notification">
																	<a href="<?php echo $sender_user_profile_link; ?>">
																		<img src="<?php echo $sender_user_avatar; ?>" class="avatar" alt="">
																	</a>

																<div class="d-flex">
																	<p><?php esc_html_e("El usuario  ")?><a href="<?php echo $sender_user_profile_link; ?>"><span class="elusuario"><?php echo  $sender_user_name; ?></span></a><?php esc_html_e(" ha realizado una compra del libro "); ?><a class="permalink_post" href="<?php echo $post_link; ?>"><?php echo $post_name; ?></a></p>
																			<p class="mx-2" style="color: #DBD9D7;"><?php echo " | "; ?></p>
																			<p><?php bp_the_notification_time_since(); ?></p>
																		</div>
																	</div>
															</div>

															<!-- <div class="notification_action">
																<div class="imagen-producto">
																	<a href="<?php echo $post_link; ?>">
																		<?php echo $post_thumbnail; ?>
																	</a>
																</div> 
															</div> -->
															
													</div>
													<?php
													}
												?>
											
											</div>
									<?php endwhile; ?>
								</div>
							<?php else : ?>
								<div class="alert-message">
									<div class="alert alert-warning w-75" role="alert"><?php esc_html_e( 'No hay notificaciones', 'libreriasocial' ); ?></div>
								</div>
							<?php endif; ?>
							
						</div>
					</div>
					<?php
				}
			?>
            </section>
        </div>
    
	</main><!-- #primary -->
	
  

	<?php do_action( 'buddyx_after_content' ); ?>
<?php
get_footer();
