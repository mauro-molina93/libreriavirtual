<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package buddyx
 */

namespace BuddyX\Buddyx;

get_header();

buddyx()->print_styles( 'buddyx-content' );
buddyx()->print_styles( 'buddyx-sidebar', 'buddyx-widgets' );

$default_sidebar = get_theme_mod( 'sidebar_option', buddyx_defaults( 'sidebar-option' ) );

?>

	<?php do_action( 'buddyx_sub_header' ); ?>
	
	<?php do_action( 'buddyx_before_content' ); ?>

	
    <?php require get_stylesheet_directory() . '/menu-lateral.php'; ?>

	<main id="primary" class="site-main">

    <?php require get_stylesheet_directory() . '/inc/users.php'; ?>

    <section class="container guardados">
        <div class="recientes">
            <h2><?php esc_html_e("Recientes", 'libreriasocial'); ?> </h2>
            <?php require get_stylesheet_directory() . '/inc/Guardados/card-recientes.php'; ?>
        </div>

        <div class="guardados">
            <h2><?php esc_html_e("Guardados", 'libreriasocial'); ?> </h2>
            <?php require get_stylesheet_directory() . '/inc/Guardados/card-guardados.php'; ?>
        </div>
    </section>

    <?php //require get_stylesheet_directory() . '/inc/queris-product.php'; ?>

	</main><!-- #primary -->


	<?php do_action( 'buddyx_after_content' ); ?>
<?php
get_footer();
