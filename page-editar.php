<?php
/**
 * The page editar
 */

namespace BuddyX\Buddyx;

get_header();

buddyx()->print_styles( 'buddyx-content' );
buddyx()->print_styles( 'buddyx-sidebar', 'buddyx-widgets' );

$default_sidebar = get_theme_mod( 'sidebar_option', buddyx_defaults( 'sidebar-option' ) );
if(isset($_GET["libro"])){
    $post_id=$_GET["libro"];
}

$product = wc_get_product( $post_id);
$category = get_the_terms( $post_id, 'product_cat' );
$product_cat = $category[0]->name ;
//var_dump($product_cat);


?>

	<?php do_action( 'buddyx_sub_header' ); ?>
	
	<?php do_action( 'buddyx_before_content' ); ?>

	
    <?php require get_stylesheet_directory() . '/menu-lateral.php'; ?>

	<main id="primary" class="site-main pag-publicar">

	<div class="container mt-5">
    <form method="post" class="formulario-crear-publicacion formulario-editar" enctype="multipart/form-data" id="formulario-libro1">
      
        <div class="row" id="pas1">

            <div class="col-md-3">
                <div class="d-flex2 flex-hp">
                    <div id="modal2" class="modal">
                    <div class="modal-content">
                        <span class="close" id="close2">&times;</span>
                        <img id="modal-image2" src="" alt="Modal Image">
                    </div>
                    </div>
                    <div class="form__container form-libro" id="upload-container-editar">
                    <div id="desaparecer_cargando-editar">
                        <h4 class="titulo-historia"><?php esc_html_e('Arrastra tu foto aquí', 'libreriasocial'); ?></h4>
                        <label for="filepublic-editar" class="boton-publicar"><?php esc_html_e('Selecciona del ordenador', 'libreriasocial'); ?></label>
                    </div>
                    <div id="overlay-editar" class="overlay"></div>
                    <label class="dropdown" id="eliminar-h-container-editar">
                        <div class="dd-button" style="display: flex !important; align-items:center;">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/threepoints.png'; ?>" alt="" width="46px" height="11px">
                        </div>

                        <input type="checkbox" class="dd-input" id="test">

                        <ul class="dd-menu">
                            <li id="eliminar-h-editar"><?php esc_html_e('Eliminar', 'libreriasocial'); ?></li>
                            <li id="visualizar-editar"><?php esc_html_e('Visualizar', 'libreriasocial'); ?></li>
                        </ul>
                    </label>
                    <input class="form__file" id="upload-files-editar" name="filepublic-editar" type="file" accept="image/*" multiple value="<?php the_post_thumbnail(); ?>">
                </div>
                <div id="files-list-container-editar" class="form__files-container"></div>
                   
            </div>
            <?php
                // $product_id = $_GET["libro"]; 
                // if ($product_id) {
                //     if (has_post_thumbnail($product_id)) {
                //         echo get_the_post_thumbnail($product_id);
                //     } 
                // }
                // ?>
                <div id="imagenes-galleria">
                    <!-- <div class="feacture image" id="p-i-l"></div> -->
                    <div class="upload__box">
                        <div class="upload__btn-box">
                            <label class="upload__btn" id="seleccionar-archivos">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/add.png' ?>" alt="">
                            <p><?php esc_html_e('Agregar imágenes', 'libreriasocial'); ?></p>
                            <input type="file" accept="image/*" class="upload__inputfile" name="imagenes[]" multiple>
                            </label>

                            <script>
                                document.addEventListener('DOMContentLoaded', function() {
                                    var productImages = <?php echo json_encode($product_images); ?>; // Obtén las imágenes existentes desde PHP

                                    var inputFiles = document.querySelectorAll('.upload__inputfile');
                                    for (var i = 0; i < inputFiles.length; i++) {
                                        if (productImages[i]) {
                                            inputFiles[i].value = productImages[i]; // Asigna el valor de la imagen existente al input correspondiente
                                        }
                                    }
                                });
                            </script>
                        </div>
                        <div class="upload__img-wrap">
                        <?php
                        global $product;
                        $product_id = $product->get_id();
                        $product_images = get_post_meta($product_id, '_product_image_gallery', true);

                        if ($product_images) {
                            $product_images = explode(',', $product_images);

                            $html = '<div class="upload__img-wrap">';
                            foreach ($product_images as $image_id) {
                                $image_url = wp_get_attachment_image_url($image_id, 'full');
                                if ($image_url) {
                                    $html .= '<div class="upload__img-container">';
                                    $html .= '<img src="' . $image_url . '" alt="Imagen destacada">';
                                    $html .= '<span class="upload__img-delete" data-image-id="' . $image_id . '">X</span>';
                                    $html .= '<input type="hidden" name="deleted_images[]" value="0">';
                                    $html .= '</div>';
                                }
                            }
                            $html .= '</div>';

                            echo $html;
                        }
                        ?>

                    <script>
                    // $('.upload__img-delete').on('click', function() {
                    //     // Obtener el ID de la imagen destacada
                    //     var imageId = $(this).data('image-id');

                    //     // Verificar si el valor ya existe en el campo de entrada oculto
                    //     var deletedImages = $('input[name="deleted_images[]"]').val();
                    //     if (deletedImages) {
                    //         // Convertir los valores en un array
                    //         deletedImages = deletedImages.split(',');
                    //     } else {
                    //         // Si no hay valores, crear un array vacío
                    //         deletedImages = [];
                    //     }

                    //     // Verificar si el ID de la imagen ya está en el array
                    //     var index = deletedImages.indexOf(imageId);
                    //     if (index === -1) {
                    //         // Si el ID no existe, agregarlo al array
                    //         deletedImages.push(imageId);
                    //     } else {
                    //         // Si el ID ya existe, eliminarlo del array
                    //         deletedImages.splice(index, 1);
                    //     }

                    //     // Actualizar el valor del campo de entrada oculto
                    //     $('input[name="deleted_images[]"]').val(deletedImages.join(','));
                    // });
                    var deleteButtons = document.getElementsByClassName("upload__img-delete");
                    for (var i = 0; i < deleteButtons.length; i++) {
            var button = deleteButtons[i];

            // Agregamos un evento de clic a cada botón
            button.addEventListener("click", function() {
                // alert("cambiado");
                // Obtenemos el valor del atributo "data-image-id"
                var imageID = this.getAttribute("data-image-id");
                console.log(imageID);
                // Buscamos el input hidden con el mismo "data-image-id"
                var container = this.parentNode;
                var hiddenInput = container.querySelector('input[name="deleted_images[]"]');
            
                // Cambiamos el valor del input hidden a "1"
                if (hiddenInput) {
                    hiddenInput.value = imageID;
                }
            });
        }
                    </script>

                   
                        </div>

                        <script>
                            jQuery(document).ready(function($) {
                                $('.upload__img-wrap').on('click', '.upload__img-delete', function() {
                                    var imageId = $(this).data('image-id');
                                     $(this).closest('.upload__img-container').css('display', 'none');
                                });
                                });
                        </script>

                        <script>
                            jQuery(document).ready(function($) {
                            // Obtiene todas las imágenes existentes
                            var $existingImages = $('.upload__img-wrap img');

                            // Verifica si hay imágenes existentes
                            if ($existingImages.length > 0) {
                                var fileInput = $('input[name="imagenes[]"]')[0];
                                
                                // Crea un objeto FormData para almacenar las imágenes
                                var formData = new FormData();
                                
                                // Itera sobre cada imagen existente
                                $existingImages.each(function(index, element) {
                                var imageUrl = $(element).attr('src');
                                
                                $.ajax({
                                    url: imageUrl,
                                    method: 'GET',
                                    responseType: 'blob',
                                    success: function(blob) {
                                    // Crea un objeto File a partir del Blob de la imagen
                                    var file = new File([blob], 'image' + (index + 1) + '.jpg', { type: 'image/jpeg' });
                                    
                                    // Agrega el archivo al objeto FormData
                                    formData.append('imagenes[]', file);
                                    
                                    // Verifica si se han agregado todas las imágenes al FormData
                                    if (index === $existingImages.length - 1) {
                                        
                                        fileInput.files = formData;
                                    }
                                    }
                                });
                                });
                            }
                            });

                          
                                var fileInput = $('input[name="imagenes[]"]')[0];
                                var fileInput1 = $('input[name="imagenes[]"]')[1];
                                var fileInput2 = $('input[name="imagenes[]"]')[2];
                                var fileInput3 = $('input[name="imagenes[]"]')[3];
                                var fileInput4 = $('input[name="imagenes[]"]')[4];

                                console.log(fileInput);
                                console.log(fileInput1);
                                console.log(fileInput2);
                                console.log(fileInput3);
                                console.log(fileInput4);
                                // console.log('value: ', fileInput.value);

                                // Agrega las imágenes existentes al campo de entrada de archivos
                                var existingImages = $('.upload__img-wrap img');
                                console.log(existingImages);
                                if (existingImages.length > 0) {
                                    var files = [];
                                    console.log("files", files);

                                    existingImages.each(function() {
                                    var imageUrl = $(this).attr('src');
                                    var fileName = imageUrl.substring(imageUrl.lastIndexOf('/') + 1);
                                    var file = dataURLtoFile(imageUrl, fileName);
                                    files.push(file);
                                    });

                                    fileInput.files = new FileListWrapper(files);
                                }

                                fileInput.addEventListener('change', function() {
                                    var files = Array.from(fileInput.files);
                                    console.log('Imágenes cargadas:');

                                    files.forEach(function(file) {
                                    console.log(file.name);
                                    });
                                });

                                // Convierte una URL de imagen en un objeto File
                                function dataURLtoFile(dataURL, filename) {
                                    var arr = dataURL.split(',');
                                    var mime = arr[0].match(/:(.*?);/)[1];
                                    var bstr = atob(arr[1]);
                                    var n = bstr.length;
                                    var u8arr = new Uint8Array(n);

                                    while (n--) {
                                    u8arr[n] = bstr.charCodeAt(n);
                                    }

                                    return new File([u8arr], filename, { type: mime });
                                }

                                // Envuelve los archivos en un objeto FileList personalizado
                                function FileListWrapper(files) {
                                    var self = this;
                                    self.length = files.length;

                                    files.forEach(function(file, index) {
                                    self[index] = file;
                                    });
                                }
                                });
                        </script>
                    </div>
                </div>
            </div>

            <div id="paso2-1">
            <div class="col-md-7">
                <div class="input-group">
                    <label for="titulo"><?php esc_html_e('Título del libro o escrito', 'libreriasocial'); ?> <span>*</span></label>
                    <input type="text" autocomplete="off" name="titulo" id="nombre-libro" value="<?php echo $product->get_title();?>" required>
                </div>

                <div class="input-group">
                    <label for="descripcion"><?php esc_html_e('Resumen del libro o escrito', 'libreriasocial'); ?> <span>*</span></label>
                    <textarea name="descripcion" id="desc-libro" required><?php echo get_post_field( 'post_content', $post_id );?> </textarea>
                </div>
            </div>

            <div class="col-md-5">

                <div class="input-group">
                    <label for="categoria_nombre"><?php esc_html_e('Género literario', 'libreriasocial'); ?> <span>*</span></label>
                    <select name="categoria_nombre" id="categoria_nombre" required>
                    <?php if($product_cat){?>
                        <option value="<?php echo $product_cat; ?>"><?php echo $product_cat; ?></option>
                        <?php }?>
                    
                        <option value=""><?php esc_html_e('Seleccione GL', 'libreriasocial'); ?></option>
                        <?php
                        $categorias = get_terms(array(
                            'taxonomy' => 'product_cat',
                            'hide_empty' => false,
                        ));

                        foreach ($categorias as $categoria) {
                            echo '<option value="' . esc_attr($categoria->name) . '">' . esc_html($categoria->name) . '</option>';
                        }
                        ?>
                    </select>

                </div>

                <div class="input-group">
                    <label for="tema"><?php esc_html_e('Tema', 'libreriasocial'); ?> <span>*</span></label>
                    <select name="tema" id="tema" required>
                    <?php if(get_post_meta($post_id,'tema',true)){?>
                        <option value="<?php echo get_post_meta($post_id,'tema',true); ?>"><?php echo get_post_meta($post_id,'tema',true); ?></option>
                        <?php }?>
                        <option value=""><?php esc_html_e('Seleccione el tema', 'libreriasocial'); ?></option>
                        <?php
                        if (function_exists('acf_get_field_groups')) {
                            $field_groups = acf_get_field_groups();

                            if (!empty($field_groups)) {
                                foreach ($field_groups as $group) {
                                    $fields = acf_get_fields($group['key']);
                                    if (!empty($fields)) {
                                        foreach ($fields as $field) {
                                            if ($field['name'] === 'tema') {
                                                if (isset($field['choices']) && !empty($field['choices'])) {
                                                    foreach ($field['choices'] as $value => $label) {
                                                        echo '<option value="' . esc_attr($value) . '">' . esc_html($label) . '</option>';
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="input-group">
                    <label for="idioma"><?php esc_html_e('Idioma', 'libreriasocial'); ?> <span>*</span></label>
                    <select name="idioma" id="idioma" required>
                    <?php if(get_post_meta($post_id,'idioma',true)){?>
                        <option value="<?php echo get_post_meta($post_id,'idioma',true); ?>"><?php echo get_post_meta($post_id,'idioma',true); ?></option>
                        <?php }?>
                        <option value=""><?php esc_html_e('Seleccione el idioma', 'libreriasocial'); ?></option>
                        <?php
                        if (function_exists('acf_get_field_groups')) {
                            $field_groups = acf_get_field_groups();

                            if (!empty($field_groups)) {
                                foreach ($field_groups as $group) {
                                    $fields = acf_get_fields($group['key']);
                                    if (!empty($fields)) {
                                        foreach ($fields as $field) {
                                            if ($field['name'] === 'idioma') {
                                                if (isset($field['choices']) && !empty($field['choices'])) {
                                                    foreach ($field['choices'] as $value => $label) {
                                                        echo '<option value="' . esc_attr($value) . '">' . esc_html($label) . '</option>';
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="input-group">
                    <?php 
                        $user_id = get_current_user();
                        $user_email = $current_user->user_email;
                    ?>

                    <label for="correo_e"><?php esc_html_e('Correo Electrónico', 'libreriasocial'); ?> <span>*</span></label>
                    <input type="text" name="correo_e" id="correo" placeholder="<?php esc_attr_e('Introduzca su correo', 'libreriasocial'); ?>" value="<?php echo $user_email; ?>" style="color:#000" required>
                </div>


                <div class="d-flex step justify-content-between"> 
                    <a class="boton-publicar" href="/"><?php esc_html_e('Cancelar', 'libreriasocial'); ?></a>   
                    <a class="boton-publicar" id="next-editar"><?php esc_html_e('Siguiente', 'libreriasocial'); ?></a>                    
                </div>
            </div>
            </div>

            <div class="" id="paso3">
               <div class="col-md-6">

               <div class="input-group">
                        <label for="catp"><?php esc_html_e('Categoría de pago', 'libreriasocial'); ?> <span>*</span></label>
                        <select name="catp" id="catp" required>
                        <?php if(get_post_meta($post_id,'categoria_pago',true)){?>
                        <option value="<?php echo get_post_meta($post_id,'categoria_pago',true); ?>"><?php echo get_post_meta($post_id,'categoria_pago',true); ?></option>
                        <?php }?>
                        <option value=""> <?php esc_html_e('Seleccione la categoría de pago'); ?> </option>
                        <?php
                        if (function_exists('acf_get_field_groups')) {
                            $field_groups = acf_get_field_groups();

                            if (!empty($field_groups)) {
                                foreach ($field_groups as $group) {
                                    $fields = acf_get_fields($group['key']);
                                    if (!empty($fields)) {
                                        foreach ($fields as $field) {
                                            if ($field['name'] === 'categoria_pago') {
                                                if (isset($field['choices']) && !empty($field['choices'])) {
                                                    foreach ($field['choices'] as $value => $label) {
                                                        echo '<option value="' . esc_attr($value) . '">' . esc_html($label) . '</option>';
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="input-group">
                    <label for="precio"><?php esc_html_e('Precio'); ?> <span>*</span></label>
                    <?php
                       $precio = $product->get_price();
                       $precio = floatval($precio);
                       

                        ?>
                        <input type="number" autocomplete="off" name="precio" value="<?php echo $precio; ?>" id="precio" placeholder="<?php esc_attr_e('Introduzca el precio del producto', 'libreriasocial'); ?>" <?php if ($precio == 0) echo 'disabled'; ?> required>

                    <div class="contenedor-checkbox">
                        <?
                            $check_recomedados = get_post_meta($post_id, 'checkrecomendados', true);

                            if ($check_recomedados) {
                                $check_recomedados_checkend = 'on' ? 'checked' : '';
                            } else {
                                $check_recomedados_checkend = null;
                            }
                        ?>

                        <input type="checkbox" id="toggle-recomendados" name="checkrecomendados" <?php echo $check_recomedados_checkend; ?>>
                        <label for="toggle-recomendados" class="mt-3">  <?php esc_html_e('Añadir a recomendados', 'libreriasocial'); ?> </label>
                    </div>

                    <div class="agregar-recomendados" style="display: none;">
                        
                        <span class="mensajerecomendados" id="mensajerecomendado"><?php esc_html_e('Se descontará el 20% al precio establecido', 'libreriasocial'); ?></span>
                    </div>
                </div>
                <script>
                    document.addEventListener('DOMContentLoaded', function() {
                    var precioInput = document.getElementById('precio');
                    
                    precioInput.addEventListener('input', function(event) {
                        var valor = event.target.value.trim();
                        var numeros = valor.replace(/\D/g, ''); 
                      
                        if (numeros.length > 7) {
                            numeros = numeros.substr(0, 7);
                        }
                       
                        event.target.value = numeros;
                    });
                });

                </script>

                <div class="mt-5 precios-libro-publicar" id="calculorecomendados">
                    <?php 

                        $iva = floatval(get_post_meta($post_id, 'iva', true));
                        $descuento = floatval(get_post_meta($post_id, 'descuento', true));
                        $descuento_recomendados = floatval(get_post_meta($post_id, 'descuento_recomendados', true));


                        $precio_final_iva = number_format($precio*($iva/100), 2);

                         if ($check_recomedados) {
                            $precio_final_descuento = number_format(($precio - $precio_final_iva)*( $descuento_recomendados / 100),2);
                            } else {
                                $precio_final_descuento = number_format(($precio - $precio_final_iva)*( $descuento / 100),2);
                            }
                         $precio_recibir_auto = number_format($precio - $precio_final_iva -$precio_final_descuento, 2);
                    ?>

                    <div><strong> <?php esc_html_e('Precio del producto', 'libreriasocial'); ?>:</strong> <span id="precio-producto"> </span> <span class="datos_eliminar" id="datos_eliminar">$<?php  $precio = $product->get_price(); echo $precio; ?> </span></div>
                    <div><strong> <?php esc_html_e('Descuento', 'libreriasocial'); ?>:</strong> <span id="descuento-precioproducto"> </span> <span class="datos_eliminar" id="datos_eliminar-2">$<?php echo $precio_final_descuento; ?> <?php esc_html_e(' + iva: '); ?>$<?php echo $precio_final_iva; ?> </span></div>
                    <div class="total_recibir"><?php esc_html_e('Total a recibir', 'libreriasocial'); ?>: <strong class="precio-total" id="precio-total"> </strong> <span class="datos_eliminar" ><strong id="precio_final_eliminar" style="font-weight:bold !important;color:#000 !important;"> $<?php echo $precio_recibir_auto; ?></strong></span></div>
                </div>
                

               </div>

               <div class="col-md-6">

               <div class="input-group share-container">
                    <label for="share"><?php esc_html_e('Compartir para', 'libreriasocial'); ?></label>

                    <?php 
                        $compartir = get_post_meta($post_id, 'compartir', true);
                        if (is_array($compartir)) {
                            $solo_checked = in_array('solo', $compartir) ? 'checked' : '';
                            $todos_checked = in_array('todos', $compartir) ? 'checked' : '';
                            $amigos_checkend = in_array('amigos', $compartir) ? 'checked' : '';
                        } else {
                            $solo_checked = ($compartir === 'solo') ? 'checked' : '';
                            $todos_checked = ($compartir === 'todos') ? 'checked' : '';
                            $amigos_checkend = ($compartir === 'amigos') ? 'checked' : '';
                        }
            
                    ?>

                    <div class="contenedor-radio">
                        <input type="radio" name="share[]" id="solo" value="solo"  <?php echo $solo_checked; ?>>
                        <label for="solo" class="form__radio-label">
                            <span class="form__radio-button"></span>
                            <?php esc_html_e('Solo yo', 'libreriasocial'); ?>
                        </label>
                    </div>

                    <div class="contenedor-radio">
                        <input type="radio" name="share[]" id="todos" value="todos" <?php echo $todos_checked; ?>>
                        <label for="todos" class="form__radio-label">
                            <span class="form__radio-button"></span>
                            <?php esc_html_e('Todos', 'libreriasocial'); ?>
                        </label>
                    </div>


                    <div class="contenedor-radio">
                        <input type="radio" name="share[]" id="amigos" value="amigos" <?php echo $amigos_checkend; ?>>
                        <label for="amigos" class="form__radio-label">
                            <span class="form__radio-button"></span>
                            <?php esc_html_e('Amigos', 'libreriasocial'); ?>
                        </label>
                    </div>
    
                </div>

                <?php
                 $amigosAmigosValue = get_post_meta($post_id, 'amigosAmigos', true);
                 $amigosAmigosChecked = !empty($amigosAmigosValue) ? 'checked' : '';

                ?>

                <div class="contenedor-amistad" id="mostrarAmistad">
                        <div class="contenedor-checkbox" id="amigos_dAmigos">
                            <input type="checkbox" name="amigosAmigos" id="amigos_de_amigos" value="amigos_de_amigos" <?php echo $amigosAmigosChecked; ?>>
                            <label for="amigos_de_amigos" class="form__radio-label">
                               
                                <?php esc_html_e('Amigos de mis amigos', 'libreriasocial'); ?>
                            </label>
                        </div>

                        <!-- <div class="contenedor-radio">
                            <input type="radio" name="share[]" id="amigosAmigos" value="amigosAmigos">
                            <label for="amigosAmigos" class="form__radio-label">
                                <span class="form__radio-button"></span>
                                <?php esc_html_e('Amigos de mis amigos', 'libreriasocial'); ?>
                            </label>
                        </div> -->
                </div>

               <script>
                    document.addEventListener('DOMContentLoaded', function() {
                        var amigosRadioButton = document.getElementById('amigos');
                        var amigosDiv = document.getElementById('amigos_dAmigos');

                        function toggleAmigosDiv() {
                            if (amigosRadioButton.checked) {
                                amigosDiv.style.display = 'block';
                                console.log("checkeado"); 
                            } else {
                                amigosDiv.style.display = 'none';
                                console.log("no checkeado"); 
                            }
                        }

                        toggleAmigosDiv();

                        var radioButtons = document.querySelectorAll('input[type="radio"]');
                        for (var i = 0; i < radioButtons.length; i++) {
                            radioButtons[i].addEventListener('change', function(event) {
                                if (event.target !== amigosRadioButton) {
                                    amigosDiv.style.display = 'none';
                                }
                                toggleAmigosDiv();
                                console.log("cambie de evento");
                            });
                        }
                    });
                </script>

                <div class="input-group d-flex mayorDeEdad">
                    <?php $mayorEdad = get_post_meta($post_id, 'mayorEdad', true);  ?>
                    <div class="contenedor-checkbox">
                        <input type="checkbox" name="mayorEdad" id="mayorEdad" <?php checked($mayorEdad, 'on'); ?>>
                        <label for="mayorEdad" class="mayorEdad"><?php esc_attr_e('Apto para +18 años de edad', 'libreriasocial'); ?> </label>
                    </div>
                </div>
               
                <?php
                $product_id = get_the_ID();

                ?>
                <div class="input-group">
                    <label for="fileLibro" class="adjunto">
                        <?php esc_html_e('Adjuntar Archivo', 'libreriasocial'); ?>
                        <input type="file" name="fileLibro-editar" id="fileLibro" class="fileLibro" placeholder="<?php esc_attr_e('Adjuntar archivo', 'libreriasocial'); ?>" accept=".pdf,.epub" disabled="disabled">
                        <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/adjunto.png' ?>">
                    </label>
                </div>
                <div id="file-name-preview">
                <?php
                    $downloadable_files = $product->get_downloads();
                    if (!empty($downloadable_files)) {
                        echo '<ul class="d-flex">';
                        foreach ($downloadable_files as $download) {
                            echo '<li id="downloadable-file"><a href="' . $download['file'] . '">' . $download['name'] . '</a></li> <a href="#" id="hide-button"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
                        }
                        echo '</ul>';
                    } else {
                        echo '<p>No se encontraron archivos descargables para este producto.</p>';
                    }
                    
                ?>

                <script>
                    var downloadableFile = document.getElementById('downloadable-file');
                    var hideButton = document.getElementById('hide-button');
                    var fileLibro = document.getElementById('fileLibro');

                    hideButton.addEventListener('click', function() {
                        downloadableFile.style.display = 'none';
                        hideButton.style.display = 'none';
                        fileLibro.disabled = false;
                    });
                </script>
                </div>
                <a href="#" id="clear-file"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                <input type="hidden" name="oculto" value="editarlibro">
                <input type="hidden" name="iva" value="<?php echo esc_attr(get_option('iva_value')); ?>">
                <input type="hidden" name="descuento" value="<?php echo esc_attr(get_option('descuento')); ?>">
                <input type="hidden" name="descuento_recomendados" value="<?php echo esc_attr(get_option('descuento_recomendados')); ?>">
                <input type="hidden" name="product_id" value="<?php echo esc_attr($post_id); ?>">

                <div class="d-flex step">    
                    <a href="#" class="boton-publicar" id="prev-editar"><?php esc_html_e('Anterior', 'libreriasocial'); ?></a>
                    <input type="submit" name="botonpublicar" value="<?php esc_attr_e('Editar', 'libreriasocial'); ?>"  class="boton-publicar" id="editar-libro">
                </div>
               </div>
            </div>
        </div>
    </form>
</div>



	</main><!-- #primary -->
	<?php do_action( 'buddyx_after_content' ); ?>
<?php
get_footer();
