<?php
/**
 * The page publicar
 */

namespace BuddyX\Buddyx;

get_header();

buddyx()->print_styles( 'buddyx-content' );
buddyx()->print_styles( 'buddyx-sidebar', 'buddyx-widgets' );

$default_sidebar = get_theme_mod( 'sidebar_option', buddyx_defaults( 'sidebar-option' ) );

?>

	<?php do_action( 'buddyx_sub_header' ); ?>
	
	<?php do_action( 'buddyx_before_content' ); ?>

	
    <?php require get_stylesheet_directory() . '/menu-lateral.php'; ?>

	<main id="primary" class="site-main pag-publicar">

	<?php require get_stylesheet_directory() . '/inc/Publicar/tabs-publicar.php' ?>

	</main><!-- #primary -->
	<?php do_action( 'buddyx_after_content' ); ?>
<?php
get_footer();
