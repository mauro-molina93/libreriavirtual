<?php

function menu_bar(){ 
    add_menu_page('YoEscribo', 'YoEscribo Ajustes', 'administrator', 'yoescribosetting', 'yoescribo_funciones', 'dashicons-admin-generic', 5);
    add_submenu_page('yoescribosetting', 'Opciones Recomendados', 'Opciones Recomendados', 'administrator', 'setting_recomendados', 'ajustes_recomendados');
    add_submenu_page('yoescribosetting', 'Listado Usuarios autores', 'Listado autores', 'administrator', 'listado_autores', 'listado_autores');
}

add_action( 'admin_menu', 'menu_bar' );

function aprobar_solicitud(){
    $id = $_POST['solic_id'];  
    global $wpdb;
    $table_name = 'solicitudes_recomendados';   
    $wpdb->update(
        $table_name,
        array(
            'estado' => 'aprobado',
        ),
        array(
            'ID' => $id
        )
    ); 
    exit();
}
add_action('wp_ajax_aprobar_solicitud', 'aprobar_solicitud');
add_action('wp_ajax_nopriv_aprobar_solicitud', 'aprobar_solicitud');

function denegar_solicitud(){
    $id = $_POST['solic_id'];  
    global $wpdb;
    $table_name = 'solicitudes_recomendados';   
    $wpdb->update(
        $table_name,
        array(
            'estado' => 'denegado',
        ),
        array(
            'ID' => $id
        )
    ); 
    exit();
}
add_action('wp_ajax_denegar_solicitud', 'denegar_solicitud');
add_action('wp_ajax_nopriv_denegar_solicitud', 'denegar_solicitud');

function insertar_solicitud_recomendados($nombre,$tipo){

    global $wpdb;
    $table_name = 'solicitudes_recomendados';  
    // $query = 'SELECT count(ID) as c FROM '.$table_name.' where nombre = '.$nombre.' and estado = "aprobado"';
    
    //  $stats = $wpdb->get_results($query);
   
    // var_dump("probando");
    // die();
    // if(intval($stats[0]->c)>0){
    //     _e('<script>  alert("Usted ya tiene una solicitud aprobada"); </script>','libreriasocial');        
      
    // }else{
          
    $wpdb->insert(
        $table_name,
        array(
            'nombre' => $nombre,                
            'tipo' => $tipo,
            'fecha' => date('Y-m-d H:i:s'), 
            'estado' => 'pendiente'
        )
    );
    // $admin_email = get_option('admin_email');
    //     wp_mail( $admin_email, 'Solicitud de recomendado' , "Estimado gestor se ha solicitado la aprobación de una solicitud de Recomendado para un ".$tipo." con el nombre ".$nombre."");
    // }
}

function ajustes_recomendados(){
?>
<style>
    table {
        margin-top: 20px;
    width: 100%;
    border: 1px solid #000;
    text-align: center!important;
    }
    thead, td {    
    width: 25%;
    text-align: left;  
    height: 26px; 
    }

    thead {
    background: #f5ba38;
    color: #4A148C;
    font-weight: 800;
    border-bottom: 1px solid #000;
    }
</style>
<div class="wrap">
    <h2><?php echo esc_html_e('Solicitudes de recomendados', 'libreriasocial'); ?></h2>

   <?php mostra_solicitudes_recomendado();?>
</div>

<?php
}


function mostra_solicitudes_recomendado() {
global $wpdb;
$table_name = 'solicitudes_recomendados';

$recommended_data = $wpdb->get_results("SELECT * FROM $table_name", ARRAY_A);

if ($recommended_data) {      
  echo '<table id="recommended"><thead><tr><td>'.__('Nombre','libreriasocial').'</td><td>'.__('Tipo','libreriasocial').'</td><td>'.__('Fecha de solicitud','libreriasocial').'</td><td>'.__('Estado','libreriasocial').'</td><td>'.__('Operaciones','libreriasocial').'</td></tr></thead>';
  foreach ($recommended_data as $data) {
        echo '<tr>';
        echo '<td>' . $data['nombre'] . '</td>';
        echo '<td>' . $data['tipo'] . '</td>';
        echo '<td>' . $data['fecha'] . '</td>';
        echo '<td>' . $data['estado'] . '</td>';
        if($data['estado']==="pendiente"){
            echo '<td><a class="aprobar" data-solic_id="' . $data['ID'] . '" href="#">Aprobar</a><a class="denegar" data-solic_id="' . $data['ID'] . '" href="#">Denegar</a></td>';
       }elseif($data['estado']==="aprobado"){
        echo '<td><a class="denegar" data-solic_id="' . $data['ID'] . '" href="#">Denegar</a></td>';
       
       }elseif($data['estado']==="denegado"){
        echo '<td></td>';
       
       }
         echo '</tr>';
    }
    echo '</table>';
} else {
    echo '<table id="recommended"><tr><th>'.__('Nombre','libreriasocial').'</th><th>'.__('Tipo','libreriasocial').'</th><th>'.__('Fecha de solicitud','libreriasocial').'</th><th>'.__('Estado','libreriasocial').'</th><th>'.__('Operaciones','libreriasocial').'</th></tr>';
    echo '<tr><td></td><td>'.__('No hay solicitudes disponibles.','libreriasocial').'</td><td></td></tr>';
 echo '</table>';
}
}

function yoescribo_funciones(){
    if (isset($_POST['submit'])) {
       
        $iva = floatval($_POST['iva']);
        $descuento = floatval($_POST['descuento']);
        $descuento_recomendados = floatval($_POST['descuento_recomendados']);

        update_option('iva_value', $iva);
        update_option('descuento', $descuento);
        update_option('descuento_recomendados', $descuento_recomendados);

        echo '<div class="notice notice-success is-dismissible">
                <p>Datos Actualizados. </p>
              </div>';
    }
    ?>
    <div class="wrap">
        <h2><?php echo esc_attr_e('YoEscribo Ajustes', 'libreriasocial'); ?></h2>

        <form method="post" action="">
            <label for="iva"><?php esc_html_e('Valor del IVA:', 'libreriasocial'); ?></label>
            <input type="number" name="iva" id="iva" value="<?php echo esc_attr(get_option('iva_value')); ?>" placeholder="<?php echo esc_attr(get_option('iva_value')); ?>" />
            <br><br>

            <label for="descuento"><?php esc_html_e('Descuento para no recomendados: '); ?></label>
            <input type="number" name="descuento" id="descuento" value="<?php echo esc_attr(get_option('descuento')); ?>" placeholder="<?php echo esc_attr(get_option('descuento')); ?>">
            <br><br>

            <label for="descuento_recomendados"><?php esc_html_e('Descuento para recomendados: '); ?></label>
            <input type="number" name="descuento_recomendados" id="descuento_recomendados" value="<?php echo esc_attr(get_option('descuento_recomendados')); ?>" placeholder="<?php echo esc_attr(get_option('descuento_recomendados')); ?>">
            <br><br>

            <p class="sms-back"><?php esc_html_e('Todos los datos seran divididos entre 100 para poder multiplicar en %', 'libreriasocial'); ?></p>
            <input type="submit" name="submit" value="Guardar" class="button-primary" />
        </form>
    </div>
    <?php

    $js_file_path = get_stylesheet_directory_uri() . '/assets/js/publicar.js';
    wp_enqueue_script('publicarjs', $js_file_path, array(), '1.0.0', true);

    $js_file_path_editar = get_stylesheet_directory_uri() . '/assets/js/editar.js';
    wp_enqueue_script('editarjs', $js_file_path_editar, array(), '1.0.0', true);
    
    $iva_value = get_option('iva_value');
    $descuento = get_option('descuento');
    $descuento_recomendados = get_option('descuento_recomendados');

    wp_localize_script('editarjs', 'ivaData', array(
        'ivaValue' => $iva_value
    ));
    
    wp_localize_script('editarjs', 'descuentoData', array(
        'descuento' => $descuento
    ));
    
    wp_localize_script('editarjs', 'descuentoRecomendadosData', array(
        'descuento_recomendados' => $descuento_recomendados
    ));

    wp_localize_script('publicarjs', 'ivaData', array(
        'ivaValue' => $iva_value
    ));

    wp_localize_script('publicarjs', 'descuentoData', array(
        'descuento' => $descuento
    ));

    wp_localize_script('publicarjs', 'descuentoRecomendadosData', array(
        'descuento_recomendados' => $descuento_recomendados
    ));
}

add_action('wp_ajax_obtener_datos_iva', 'obtener_datos_iva');
add_action('wp_ajax_nopriv_obtener_datos_iva', 'obtener_datos_iva');

function obtener_datos_iva() {
    $iva_value = get_option('iva_value');
    $descuento = get_option('descuento');
    $descuento_recomendados = get_option('descuento_recomendados');
    
    $response = array(
        'success' => true,
        'ivaValue' => $iva_value,
        'descuento' => $descuento,
        'descuento_recomendados' => $descuento_recomendados
    );
  
    wp_send_json($response);
}

function listado_autores() {
    global $wpdb;

    $query = $wpdb->prepare(
        "SELECT u.ID, u.display_name, u.user_email, um.meta_value AS account_status
        FROM {$wpdb->users} AS u
        INNER JOIN {$wpdb->usermeta} AS um ON u.ID = um.user_id
        WHERE um.meta_key = 'account_status' AND um.meta_value IN ('pending', 'ready', 'noready')
        ORDER BY u.user_registered DESC"
    );

    // $results_per_page = 5;
    // $total_results = $wpdb->get_var("SELECT COUNT(*) FROM ($query) AS result");
    // $total_pages = ceil($total_results / $results_per_page);
    // $current_page = max(1, get_query_var('paged'));
    // $offset = ($current_page - 1) * $results_per_page;
    // $query .= " LIMIT $offset, $results_per_page";

    $results = $wpdb->get_results($query);
    ?>
    <div class="wrap">
        <h2><?php echo esc_html__('Listado de autores', 'libreriasocial'); ?></h2>

        <table class="wp-list-table widefat fixed striped">
            <thead>
                <tr>
                    <th><?php echo esc_html__('Nombre', 'libreriasocial'); ?></th>
                    <th><?php echo esc_html__('Correo', 'libreriasocial'); ?></th>
                    <th><?php echo esc_html__('Cuenta bancaria', 'libreriasocial'); ?></th>
                    <th><?php echo esc_html__('Acciones', 'libreriasocial'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($results as $user) {
                    $user_id = $user->ID;
                    $user_email = $user->user_email;
                    $account_status = $user->account_status;
                    $user_profile_link = get_edit_user_link($user_id);
                    ?>
                    <tr>
                        <td><?php echo "<a href='" .  $user_profile_link  . "' ><strong> " . esc_html($user->display_name) . "</strong></a>";  ?></td>
                        <td><?php echo esc_html($user_email); ?></td>
                        <td><?php $cuenta_bancaria = get_user_meta($user->ID, 'cuenta_bancaria', true); echo esc_html($cuenta_bancaria); ?></td>
                        <td>
                            <?php if ($account_status === 'pending') { ?>
                                <button class="button button-primary" onclick="aceptarCuenta(<?php echo $user_id; ?>, '<?php echo $user_email; ?>')">
                                    <?php echo esc_html__('Aceptar', 'libreriasocial'); ?>
                                </button>
                                <button class="button button-secondary" onclick="denegarCuenta(<?php echo $user_id; ?>)">
                                    <?php echo esc_html__('Denegar', 'libreriasocial'); ?>
                                </button>
                                <button class="button button-secondary">
                                    <a href="<?php echo $user_profile_link; ?>">
                                        <?php esc_html_e('Ver perfil', 'libreriasocial')?>
                                    </a>
                                </button>
                            <?php } elseif ($account_status === 'ready') { ?>
                                <?php esc_html_e('Este usuario fue aprobado', 'libreriasocial'); ?>
                                <button class="button button-secondary">
                                    <a href="<?php echo $user_profile_link; ?>">
                                        <?php esc_html_e('Ver perfil', 'libreriasocial')?>
                                    </a>
                                </button>
                            <?php } elseif ($account_status === 'noready') { ?>
                                <?php esc_html_e('Este autor fue denegado', 'libreriasocial'); ?>
                                <button class="button button-secondary">
                                    <a href="<?php echo $user_profile_link; ?>">
                                        <?php esc_html_e('Ver perfil', 'libreriasocial')?>
                                    </a>
                                </button>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

        <!-- <?php if ($total_pages > 1) { ?>
            <div class="tablenav">
                <div class="tablenav-pages">
                    <?php
                    echo paginate_links(array(
                        'base' => add_query_arg('paged', '%#%'),
                        'format' => '',
                        'prev_text' => '&laquo;',
                        'next_text' => '&raquo;',
                        'total' => $total_pages,
                        'current' => $current_page,
                        'before_page_number' => '<span class="page-numbers">',
                        'after_page_number' => '</span>',
                    ));
                    ?>
                </div>
            </div>
        <?php } ?> -->
    </div>

    <script>
        function aceptarCuenta(userId, userEmail) {
            var data = {
                action: 'aceptar_cuenta',
                user_id: userId,
                user_email: userEmail
            };

            jQuery.post(ajaxurl, data, function(response) {
                if (response.success) {
                    alert("Este autor esta listo para publicar");
                    location.reload();
                } else {
                    alert("Hubo un problema en el servidor");
                }
            });
        }

        function denegarCuenta(userId) {
            var data = {
                action: 'denegar_cuenta',
                user_id: userId
            };

            jQuery.post(ajaxurl, data, function(response) {
                if (response.success) {
                    alert("La cuenta se ha denegado correctamente");
                    location.reload(); 
                } else {
                    alert("Hubo un problema en el servidor");
                }
            });
        }
    </script>
    <?php
}

add_action('wp_ajax_aceptar_cuenta', 'aceptar_cuenta_callback');
function aceptar_cuenta_callback() {
    $user_id = $_POST['user_id'];
    $user_email = $_POST['user_email'];

    update_user_meta($user_id, 'account_status', 'ready');
    $user = new WP_User($user_id);
    $user->set_role('shop_manager');

    $to = $user_email;
    $subject = 'Tu cuenta está lista';
    $message = '¡Tu cuenta ha sido aceptada y está lista para usar!';
    $headers = array('Content-Type: text/html; charset=UTF-8');
    wp_mail($to, $subject, $message, $headers);


    wp_send_json_success();
}

add_action('wp_ajax_denegar_cuenta', 'denegar_cuenta_callback');
function denegar_cuenta_callback() {
    $user_id = $_POST['user_id'];

    update_user_meta($user_id, 'account_status', 'noready');
    $user = new WP_User($user_id);
    // $user->set_role('suscriber1');

    $user_email = $user->user_email;
    $subject = 'Solicitud de autor rechazada';
    $message = 'Su solicitud de autor ha sido denegada, por lo que podrá navegar en el sitio y efectuar compras, pero no publicar';
    $headers = array('Content-Type: text/html; charset=UTF-8');
    
    wp_mail($user_email, $subject, $message, $headers);
 
    wp_send_json_success();
}

function quitar_opcion_menu() {
    remove_menu_page('buddyboss-globalsearch');
    remove_menu_page('buddyx-demo-data');
    remove_menu_page('bp-better-messages');
    remove_menu_page('one-user-avatar');
    remove_menu_page('acf-field-group');
    remove_menu_page('bp-groups');
    remove_menu_page('bp-activity');
    remove_menu_page('bp-email');
}

add_action('admin_menu', 'quitar_opcion_menu', 99);
