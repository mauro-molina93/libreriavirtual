<?php
function agregar_campos_personalizados($user)
{
    $documento = get_user_meta($user->ID, 'documento', true);
    $direccion = get_user_meta($user->ID, 'direccion', true);
    $cuenta_bancaria = get_user_meta($user->ID, 'cuenta_bancaria', true);
    $desc_perfil = get_user_meta($user->ID, 'desc_perfil', true);
    $telefono = get_user_meta($user->ID, 'telefono', true);
    $mayor_edad = get_user_meta($user->ID, 'mayor_edad', true);
    $dia_nacimiento = get_user_meta($user->ID, 'dia_nacimiento', true);
    $mes_nacimiento = get_user_meta($user->ID, 'mes_nacimiento', true);
    $anio_nacimiento = get_user_meta($user->ID, 'anio_nacimiento', true);
    $recomendado = get_user_meta($user->ID, 'recomendado', true);
   
    ?>
    <h3><?php esc_html_e('Información Adicional', 'libreriasocial'); ?></h3>

    <table class="form-table">
        <tr>
            <th><label for="documento"><?php esc_html_e('Documento de Identidad', 'libreriasocial')?></label></th>
            <td>
                <input type="text" name="documento" id="documento" value="<?php echo esc_attr($documento); ?>" class="regular-text">
            </td>
        </tr>
        <tr>
            <th><label for="direccion"><?php esc_html_e('Dirección', 'libreriasocial')?></label></th>
            <td>
                <input type="text" name="direccion" id="direccion" value="<?php echo esc_attr($direccion); ?>" class="regular-text">
            </td>
        </tr>
        <tr>
            <th><label for="cuenta_bancaria"><?php esc_html_e('Cuenta Bancaria', 'libreriasocial')?></label></th>
            <td>
                <input type="text" name="cuenta_bancaria" id="cuenta_bancaria" value="<?php echo esc_attr($cuenta_bancaria); ?>" class="regular-text">
            </td>
        </tr>
        <tr>
            <th><label for="telefono"><?php esc_html_e('Número de Teléfono', 'libreriasocial')?></label></th>
            <td>
                <input type="tel" name="telefono" id="telefono" value="<?php echo esc_attr($telefono); ?>" class="regular-text">
            </td>
        </tr>

        <tr>
            <th><label for="desc_perfil"><?php esc_html_e('Descripción del perfil', 'libreriasocial')?></label></th>
            <td>
                <textarea name="desc_perfil" id="desc_perfil" value="" class="regular-text"><?php echo esc_attr($desc_perfil); ?></textarea>
            </td>
        </tr>

        <tr>
            <th><label for="dia_nacimiento"><?php esc_html_e('Día de Nacimiento', 'libreriasocial')?></label></th>
            <td>
                <select name="dia_nacimiento" id="dia_nacimiento">
                    <option value=""><?php esc_html_e('Día', 'libreriasocial'); ?></option>
                    <?php for ($i = 1; $i <= 31; $i++) : ?>
                        <option value="<?php echo $i; ?>" <?php selected($dia_nacimiento, $i); ?>><?php echo $i; ?></option>
                    <?php endfor; ?>
                </select>
            </td>
        </tr>
        <tr>
            <th><label for="mes_nacimiento"><?php esc_html_e('Mes de Nacimiento', 'libreriasocial')?></label></th>
            <td>
                <select name="mes_nacimiento" id="mes_nacimiento">
                    <option value=""><?php esc_html_e('Mes', 'libreriasocial'); ?></option>
                    <?php
                    $meses = array(
                        1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio',
                        7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre'
                    );
                    foreach ($meses as $numero => $nombre) : ?>
                        <option value="<?php echo $numero; ?>" <?php selected($mes_nacimiento, $numero); ?>><?php echo $nombre; ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
        <tr>
            <th><label for="anio_nacimiento"><?php esc_html_e('Año de Nacimiento', 'libreriasocial')?></label></th>
            <td>
                <select name="anio_nacimiento" id="anio_nacimiento">
                    <option value=""><?php esc_html_e('Año', 'libreriasocial'); ?></option>
                    <?php
                    $anioActual = date("Y");
                    for ($i = $anioActual; $i >= 1900; $i--) : ?>
                        <option value="<?php echo $i; ?>" <?php selected($anio_nacimiento, $i); ?>><?php echo $i; ?></option>
                    <?php endfor; ?>
                </select>
            </td>
        </tr>

        <tr>
            <th><label for="mayor_edad"><?php esc_html_e('Mayor de Edad', 'libreriasocial')?></label></th>
            <td>
                <input type="checkbox" name="mayor_edad" id="mayor_edad" value="1" <?php checked($mayor_edad, 1); ?>>
            </td>
        </tr>

        <tr>
            <th><label for="recomendado"><?php esc_html_e('Recomendado', 'libreriasocial')?></label></th>
            <td>
                <input type="radio" name="recomendado" id="recomendado_si" value="si" <?php checked($recomendado, 'si'); ?>> <?php esc_html_e('SI', 'libreriasocial')?>
                <input type="radio" name="recomendado" id="recomendado_no" value="no" <?php checked($recomendado, 'no'); ?>> <?php esc_html_e('NO', 'libreriasocial')?>
            </td>
        </tr>
    </table>
    <?php
}
add_action('show_user_profile', 'agregar_campos_personalizados');
add_action('edit_user_profile', 'agregar_campos_personalizados');


function guardar_campos_personalizados($user_id)
{
    if (current_user_can('edit_user', $user_id)) {
        update_user_meta($user_id, 'documento', $_POST['documento']);
        update_user_meta($user_id, 'direccion', $_POST['direccion']);
        update_user_meta($user_id, 'cuenta_bancaria', $_POST['cuenta_bancaria']);
        update_user_meta($user_id, 'telefono', $_POST['telefono']);
        update_user_meta($user_id, 'desc_perfil', $_POST['desc_perfil']);
        $mayor_edad = isset($_POST['mayor_edad']) ? 1 : 0;
        update_user_meta($user_id, 'mayor_edad', $mayor_edad);

        update_user_meta($user_id, 'dia_nacimiento', $_POST['dia_nacimiento']);
        update_user_meta($user_id, 'mes_nacimiento', $_POST['mes_nacimiento']);
        update_user_meta($user_id, 'anio_nacimiento', $_POST['anio_nacimiento']);

        $recomendado = isset($_POST['recomendado']) ? $_POST['recomendado'] : 'no';
        update_user_meta($user_id, 'recomendado', $recomendado);    
    }
}
add_action('personal_options_update', 'guardar_campos_personalizados');
add_action('edit_user_profile_update', 'guardar_campos_personalizados');