<div id="exTab1" class="container">	
<ul  class="nav nav-pills tabs">
			<li class="active">
                <a  href="#1a" data-toggle="tab" class="active show"><?php esc_html_e("Compras", "libreriasocial"); ?></a>
			</li>
			<button id="exportButton">Exportar</button>

<script>
document.getElementById('exportButton').addEventListener('click', function() {
    window.location.href = '<?php echo home_url() . '?export_orders=1'; ?>';
});
</script>

			<li>
                <a href="#2a" data-toggle="tab"><?php esc_html_e("Pagos Recibidos", "libreriasocial"); ?></a>
			</li>
			
		</ul>

			<div class="tab-content clearfix">
			  <div class="tab-pane active" id="1a">
                    <?php require get_stylesheet_directory() . '/template-parts/cart-user/compras.php'; ?>
			  </div>

			  <div class="tab-pane" id="2a">
                    <?php require get_stylesheet_directory() . '/template-parts/cart-user/pagos.php'; ?>
			  </div>

			</div>
  </div>