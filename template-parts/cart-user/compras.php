<?php
if (class_exists('WooCommerce')) {
   
    $user_id = get_current_user_id();

    $customer_orders = wc_get_orders(array(
        'customer' => $user_id,
        'status' => array('completed') 
    ));

    if ($customer_orders) {
        ?>
        <ul class="producto-lista">
            <?php
             foreach ($customer_orders as $order) {
                $items = $order->get_items();

                foreach ($items as $item) {

                    $product_id = $item->get_product_id();
                    $product_name = $item->get_name();
                    $product_quantity = $item->get_quantity();
                    
                    
                    $product = wc_get_product($product_id);
                    $product_image = $product->get_image();
                    $date_created = $order->get_date_created();
                    $rating_html = wc_get_rating_html($product->get_average_rating(), $product->get_rating_count());
                    
                    $product_sold_count = $product->get_total_sales();

                    ?>
                    <li class="producto-configuracion">
                            <div class="user-details">
                                <?php 
                                $customer_id = $order->get_customer_id();
                                if ($customer_id) {
                                    $customer = get_user_by('id', $customer_id); 
                                    if ($customer) {
                                        $customer_name = $customer->display_name; 
                                        $customer_avatar = get_avatar($customer_id); 
                                        ?>
                                        <div class="d-flex align-items-center mb-2">
                                            <div class="avatar"><?php echo $customer_avatar; ?></div>
                                            <p class="nombre-usuario mb-0 ml-2"><?php echo $customer_name; ?></p>
                                            <i class="ml-4 fa fa-angle-right" aria-hidden="true"></i>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        <div class="d-flex">
                            <div class="col-md-9 p-0">
                            
                                <div class="d-flex imagen">
                                    <?php echo $product_image; ?>

                                    <div class="datos-compra ml-2">
                                        <h5 class="mb-0"><?php echo $product_name; ?></h5>
                                        <p class="desc"><?php echo substr($product->get_description(), 0, 120); ?></p>
                                        <p><?php echo $rating_html; ?></p>
                                
                                        <div class="cantidad-compras mt-4 "><span><?php echo $product_sold_count; ?></span><?php esc_html_e(' ventas este mes', 'libreriasocial'); ?></div> 
                                    </div>
                                </div>
                                
                             
                            </div>

                            <div class="col-md-3 d-flex justify-content-between flex-column">
                                <div class="precio mb-3">
                                    <span class="cantidad-total"><?php echo $product->get_price_html(); ?> </span>
                                    <span class="mensaje-precio"><?php echo esc_html_e('Total pagado', 'libreriasocial'); ?></span>
                                </div>

                                <div class="fecha">
                                    <p class="fecha-compra mb-0"><?php echo $date_created->format('d.m.Y'); ?> </p>
                                    <span class="d-block"><?php echo esc_html_e('Fecha de la compra', 'libreriasocial'); ?></span>
                                </div>
                            </div>
                        </div>

                    </li>
                    <?php
                }
            }
            ?>
        </ul>
        <?php
    } else {
        echo '<p>No se encontraron órdenes para este usuario.</p>';
    }
} else {
    echo '<p>WooCommerce no está activo.</p>';
}
?>
