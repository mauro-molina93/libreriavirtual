<?php
global $wpdb;

$user_id = get_current_user_id();

$product_ids = $wpdb->get_col(
    $wpdb->prepare(
        "SELECT ID FROM $wpdb->posts WHERE post_type = 'product' AND post_author = %d",
        $user_id
    )
);

if (!empty($product_ids)) {
    ?>
    <ul class="producto-lista">
        <?php
        foreach ($product_ids as $product_id) {
            $product = wc_get_product($product_id);

            if ($product) {
                $product_sales = $product->get_total_sales();

                if ($product_sales > 0) {
                    $product_name = $product->get_name();
                    $product_image = $product->get_image();
                    ?>

                    <li class="producto-configuracion pagos">
                        <div class="d-flex">
                            <div class="col-md-9">
                                <div class="d-flex imagen">
                                    <?php echo $product_image; ?>
                                    <div class="datos-compra ml-2">
                                        <h5 class="mb-0"> <?php echo $product_name; ?></h5>
                                        <span><?php echo esc_html_e('Precio establecido', 'libreriasocial'); ?></span>
                                        <p class="mb-0"><?php echo esc_html__('Cantidad de compras:', 'libreriasocial') . ' ' . $product_sales; ?> </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 d-flex justify-content-between flex-column">
                                <div class="precio mb-3">
                                    <span class="cantidad-total"><?php echo $product->get_price_html(); ?> </span>
                                    <span class="mensaje-precio"><?php echo esc_html_e('Total pagado', 'libreriasocial'); ?></span>
                                </div>
                                <div class="fecha">
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php
                }
            }
        }
        ?>
    </ul>
    <?php
} else {
    echo '<p>No se encontraron productos vendidos para este usuario.</p>';
}
?>
