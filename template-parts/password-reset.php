<?php
if ( isset( $_POST['submit'] ) ) {
    $current_password = $_POST['current_password'];
    $new_password = $_POST['new_password'];
    $confirm_password = $_POST['confirm_password'];

    if ( $new_password !== $confirm_password ) {
       
        echo '<script>Swal.fire("Error", "Las contraseñas no coinciden.", "error");</script>';
    } else {
        $user = wp_get_current_user();

        if ( wp_check_password( $current_password, $user->user_pass, $user->ID ) ) {
            wp_set_password( $new_password, $user->ID );
           
            echo '<script>Swal.fire("", "¡La contraseña se ha cambiado con éxito!", "success");</script>';
        } else {
            echo '<script>Swal.fire("Error", "La contraseña actual no es válida", "error");</script>';
           die();
        }
    }
}
?>

<div class="password-reset ">
    <section class="title-page">
        <h2><?php echo esc_html_e('Cambiar contraseña', 'libreriasocial'); ?></h2>
        <hr class="separador">
    </section>

    <div class="row">
        <form id="change-password-form" method="post" action="<?php echo esc_url( $_SERVER['REQUEST_URI'] ); ?>">
            <label for="current_password">Contraseña actual:</label>
            <input type="password" name="current_password" id="current_password" required><br>
             <label for="new_password">Nueva contraseña:</label>
            <input type="password" name="new_password" id="new_password" required>
            <small><i class="fa fa-info-circle" aria-hidden="true"></i>La contraseña debe contener al menos 8 caracteres, una mayúscula, una minúscula, un número y un caracter especial.</small>
            <br>
            <label for="confirm_password">Confirmar nueva contraseña:</label>
            <input type="password" name="confirm_password" id="confirm_password" required><br>

           <div class="d-flex">
                <input class="boton-publicar" type="submit" name="submit" value="<?php esc_attr_e('Aceptar'); ?>">
                <a href="/configuracion" class="boton-publicar"><?php esc_html_e('Cancelar', 'libreriasocial'); ?></a>
           </div>
        </form>
    </div>
</div>

