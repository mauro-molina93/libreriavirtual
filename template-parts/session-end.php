<div class="container-session-end d-flex justify-content-center align-items-center">
    <div class="alert-finish-session bg-white p-4" style="width: 75%; border-radius: 30px;">
        <div class="icono d-flex justify-content-center mb-2">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/alertsessionend.png'; ?>" alt="">
        </div>
        <h3 class="text-center">¿Seguro que deseas cerrar sesión?</h3>

        <div class="d-flex justify-content-between">
            <form action="<?php echo esc_url(wp_logout_url()); ?>" method="post">
                <button type="submit" class="boton-cerrar">Aceptar</button>
            </form>
            <a href="<?php home_url(); ?>" class="boton-publicar">Cancelar</a>
        </div>
    </div>
</div>