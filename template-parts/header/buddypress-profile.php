<?php
/**
 * BuddyX notification nav
 *
 * Displays in the notification navbar
 *
 * @package buddyx
 * @since 1.0.0
 */

/** Do not allow directly accessing this file. */
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}

// User Messages.
if ( class_exists( 'BuddyPress' ) && is_user_logged_in() && bp_is_active( 'messages' ) ) { ?>
	<div class="bp-msg">
		<a class="bp-icon-wrap" href="<?php echo esc_url( bp_get_messages_slug() ); ?>">
			<span class="fa fa-envelop e">
				<!-- <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/mensaje.png'; ?>" alt="" width="20px" height="20px"> -->
				<svg xmlns="http://www.w3.org/2000/svg" width="23" height="25" viewBox="0 0 23 25" fill="none">
				<g clip-path="url(#clip0_1_386)">
					<path d="M4.54938 13.642H13.6358V11.3642H4.54938V13.642ZM4.54938 9.09259H18.1852V8.98148C17.5072 8.8312 16.8614 8.56158 16.2778 8.18519C15.7023 7.81837 15.1965 7.35228 14.7839 6.80864H4.54938V9.09259ZM0 25.0062V4.54938C0.00162915 3.94742 0.241511 3.37057 0.667167 2.94492C1.09282 2.51926 1.66967 2.27941 2.27164 2.27778H13.7531C13.6741 2.65119 13.6348 3.0319 13.6358 3.41358C13.6354 3.79522 13.6747 4.17588 13.7531 4.54938H2.27164V19.463L3.58028 18.1852H20.4568V8.98148C20.8787 8.89013 21.2918 8.76207 21.6914 8.59877C22.0688 8.44409 22.4221 8.23622 22.7407 7.98148V18.1852C22.7391 18.7871 22.4993 19.364 22.0736 19.7897C21.648 20.2153 21.0711 20.4552 20.4692 20.4568H4.54938L0 25.0062ZM19.321 6.82099C18.5361 6.81788 17.7765 6.5429 17.1715 6.04283C16.5664 5.54277 16.1534 4.84852 16.0026 4.07822C15.8518 3.30792 15.9726 2.50917 16.3444 1.81788C16.7162 1.12659 17.316 0.585474 18.0418 0.286615C18.7676 -0.0122431 19.5745 -0.050383 20.3252 0.178688C21.076 0.407758 21.7242 0.889884 22.1595 1.54303C22.5949 2.19617 22.7904 2.97997 22.7129 3.76106C22.6354 4.54215 22.2897 5.27226 21.7345 5.82716C21.4215 6.149 21.0459 6.40324 20.6308 6.57417C20.2157 6.74511 19.7699 6.82911 19.321 6.82099Z" fill="#4A148C"/>
				</g>
				<defs>
					<clipPath id="clip0_1_386">
					<rect width="22.7284" height="25" fill="white"/>
					</clipPath>
				</defs>
				</svg>
			</span>
			<?php if ( messages_get_unread_count( bp_loggedin_user_id() ) > 0 ) : ?>
				<?php if ( messages_get_unread_count( bp_loggedin_user_id() ) > 9 ) : ?>
					<sup class="count"><?php esc_html_e( '9+', 'buddyx' ); ?></sup>
				<?php else : ?>
					<sup class="count"><?php echo esc_html( messages_get_unread_count() ); ?></sup>
				<?php endif; ?>
			<?php endif; ?>
		</a>
	</div>
	<?php
}
// User notifications.
if ( class_exists( 'BuddyPress' ) && is_user_logged_in() && bp_is_active( 'notifications' ) ) {
	global $bp;
	?>
	<div class="user-notifications">
		<a class="bp-icon-wrap" href="/notificaciones" title="<?php esc_attr( 'Notifications', 'buddyx' ); ?>">
			<span class="fa fa-bel">
				<!-- <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/noti.png'; ?>" alt="" width="20px" height="20px"> -->
				<svg xmlns="http://www.w3.org/2000/svg" width="23" height="28" viewBox="0 0 23 28" fill="none">
				<g clip-path="url(#clip0_1_388)">
					<path d="M0 23.7998V20.9998H2.8V11.1998C2.78156 9.32932 3.39855 7.50797 4.55 6.0338C5.68081 4.55408 7.28816 3.51054 9.1 3.0798V2.0998C9.09546 1.82343 9.1478 1.54907 9.25377 1.29378C9.35974 1.03849 9.51707 0.807715 9.716 0.6158C10.1097 0.222539 10.6435 0.00164795 11.2 0.00164795C11.7565 0.00164795 12.2902 0.222539 12.684 0.6158C12.8829 0.807715 13.0403 1.03849 13.1462 1.29378C13.2522 1.54907 13.3045 1.82343 13.3 2.0998V3.0798C15.1118 3.51054 16.7192 4.55408 17.85 6.0338C19.0015 7.50797 19.6184 9.32932 19.6 11.1998V20.9998H22.4V23.7998H0ZM11.2 27.9998C10.4574 27.9998 9.7452 27.7048 9.2201 27.1797C8.695 26.6546 8.4 25.9424 8.4 25.1998H14C14 25.9424 13.705 26.6546 13.1799 27.1797C12.6548 27.7048 11.9426 27.9998 11.2 27.9998ZM5.6 20.9998H16.8V11.1998C16.8 9.71459 16.21 8.29021 15.1598 7.24C14.1096 6.1898 12.6852 5.5998 11.2 5.5998C10.4649 5.58837 9.73527 5.72783 9.05621 6.00957C8.37715 6.29131 7.7631 6.70933 7.252 7.2378C6.71993 7.74982 6.29875 8.3657 6.01457 9.04725C5.73039 9.72879 5.58928 10.4615 5.6 11.1998V20.9998Z" fill="#4A148C"/>
				</g>
				<defs>
					<clipPath id="clip0_1_388">
					<rect width="22.4" height="28" fill="white"/>
					</clipPath>
				</defs>
				</svg>
			</span>
			<?php if ( bp_notifications_get_unread_notification_count( bp_loggedin_user_id() ) > 0 ) : ?>
				<?php if ( bp_notifications_get_unread_notification_count( bp_loggedin_user_id() ) > 9 ) : ?>
					<sup class="count"><?php esc_html_e( '9+', 'buddyx' ); ?></sup>
				<?php else : ?>
					<sup class="count"><?php echo esc_html( bp_notifications_get_unread_notification_count( bp_loggedin_user_id() ) ); ?></sup>
				<?php endif; ?>
			<?php endif; ?>
		</a>
		<div id="bp-notify" class="bp-header-submenu bp-dropdown bp-notify">
			<?php
			if ( bp_has_notifications(
				array(
					'user_id'  => bp_loggedin_user_id(),
					'per_page' => 10,
					'max'      => 10,
				)
			) ) :
				?>
				<div class="bp-dropdown-inner">
					<?php
					while ( bp_the_notifications() ) :
						bp_the_notification();
						?>
						<div class="dropdown-item">
							<div class="dropdown-item-title notification ellipsis"><?php bp_the_notification_description(); ?></div>
							<p class="mute"><?php bp_the_notification_time_since(); ?></p>
						</div>
					<?php endwhile; ?>
				</div>
			<?php else : ?>
				<!-- <div class="alert-message">
					<div class="alert alert-warning" role="alert"><?php esc_html_e( 'No notifications found.', 'buddyx' ); ?></div>
				</div> -->
			<?php endif; ?>
			<div class="dropdown-footer">
				<a href="<?php echo esc_url( trailingslashit( bp_loggedin_user_domain() . bp_get_notifications_slug() . '/unread' ) ); ?>" class="button"><?php esc_html_e( 'All Notifications', 'buddyx' ); ?></a>
			</div>
		</div>
	</div>
	<?php
}
// User Account Details.
if (is_user_logged_in()) {
	$loggedin_user = wp_get_current_user();
	if ($loggedin_user instanceof WP_User) {
		$user_link = '#'; 

		if (function_exists('buddypress')) {
			$user_link = '#'; 

			// Check for BuddyPress version and function existence.
			if (version_compare(buddypress()->version, '12.0', '>=') && function_exists('bp_members_get_user_url')) {
				$user_link = bp_members_get_user_url(get_current_user_id());
			} elseif (function_exists('bp_core_get_user_domain')) {
				$user_link = bp_core_get_user_domain(get_current_user_id());
			}
		}

		// User link and profile display.
		echo '<div class="user-link-wrap">';
		echo '<a class="user-link" href="' . esc_url($user_link) . '">';
		echo '<span class="bp-user">' . esc_html($loggedin_user->display_name) . '</span>';
		echo get_avatar($loggedin_user->user_email, 100);
		echo '</a>';

		// User profile menu.
		wp_nav_menu(array(
			'theme_location' => 'user_menu',
			'menu_id'        => 'user-profile-menu',
			'fallback_cb'    => '',
			'container'      => false,
			'menu_class'     => 'user-profile-menu',
		));
		echo '</div>';
	}
} else {
	global $wbtm_buddyx_settings;
	$login_page_url = wp_login_url();
	if ( isset( $settings['buddyx_pages']['buddyx_login_page'] ) && ( $wbtm_buddyx_settings['buddyx_pages']['buddyx_login_page'] != '-1' ) ) {
		$login_page_id  = $wbtm_buddyx_settings['buddyx_pages']['buddyx_login_page'];
		$login_page_url = get_permalink( $login_page_id );
	}
		$registration_page_url = "/registro";
	if ( isset( $wbtm_buddyx_settings['buddyx_pages']['buddyx_register_page'] ) && ( $wbtm_buddyx_settings['buddyx_pages']['buddyx_register_page'] != '-1' ) ) {
		$registration_page_id  = $wbtm_buddyx_settings['buddyx_pages']['buddyx_register_page'];
		$registration_page_url = get_permalink( $registration_page_id );
	}
	?>
	<?php if ( true == get_theme_mod( 'site_login_link', true ) ) : ?>
		<div class="bp-icon-wrap">
			<a href="<?php echo esc_url( $login_page_url ); ?>" class="btn-login d-flex align-items-center" title="<?php esc_attr_e( 'Login', 'buddyx' ); ?>"> <i class="fa fa-user" aria-hidden="true"></i><?php //esc_html_e( 'Acceder', 'buddyx' ); ?> <span class="ml-2"><?php esc_html_e("Acceder", "libreriasocial"); ?></span></a>
		</div>
	<?php endif; ?>
	<?php
	if ( get_option( 'users_can_register' ) && true == get_theme_mod( 'site_register_link', true ) ) {
		?>
		<!-- <div class="bp-icon-wrap">
			<a href="<?php echo esc_url( $registration_page_url ); ?>" class="btn-register" title="<?php esc_attr_e( 'Register', 'buddyx' ); ?>"><span class="fa fa-sign-in"></span><?php esc_html_e( 'Registro', 'buddyx' ); ?></a>
		</div> -->
		<?php
	} elseif ( function_exists( 'bp_is_active' ) && bp_get_option( 'bp-enable-membership-requests' ) && true === get_theme_mod( 'site_register_link', true ) ) {
		?>
		<!-- <div class="bp-icon-wrap">
			<a href="<?php echo esc_url( $registration_page_url ); ?>" class="btn-register" title="<?php esc_attr_e( 'Register', 'buddyx' ); ?>"><span class="fa fa-sign-in"></span><?php esc_html_e( 'Register', 'buddyx' ); ?></a>
		</div> -->
		<?php
	}
}
