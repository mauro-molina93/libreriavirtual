<div class="profile-edit">
    <div class="row">
        <form action="" method="post" enctype="multipart/form-data" id="profile-form">
        <div class="col-md-6">

            <div class="input-group mb-3">
                <?php
                $avatar_url = get_avatar_url(get_current_user_id());
                ?>

                <?php //echo do_shortcode('[avatar_upload]'); ?>

                <div class="avatar-upload">
                    <div class="avatar-edit" style="margin-top: 65px;">
                        <input name="avatar" type='file' id="imageUpload"  />
                        <label for="imageUpload"></label>
                    </div>
                    <div class="avatar-preview">
                        <div id="imagePreview" style="background-image: url('<?php echo esc_url($avatar_url); ?>');">
                        </div>
                    </div>
                </div>
            </div>

                <div class="input-group">
                        <label >Fecha de nacimiento</label>
                        <div class="d-flex">
                            <div class="input-nacimient ml-2">
                                <select class="form-control" name="day">
                                <option value=""><?php echo esc_attr(get_user_meta(get_current_user_id(), 'dia_nacimiento', true)); ?></option>
                                    <?php
                                    for ($i = 1; $i <= 31; $i++) {
                                        echo '<option value="' . $i . '"';
                                        if ($i == get_user_meta(get_current_user_id(), 'dia_nacimiento', true)) {
                                            echo ' selected';
                                        }
                                        echo '>' . $i . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="input-nacimient ml-2">
                            <select class="form-control" name="month">
                                <?php
                                $numero_mes = get_user_meta(get_current_user_id(), 'mes_nacimiento', true);
                                $meses = array(
                                    1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio',
                                    7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre'
                                );
                                $nombre_mes = isset($meses[$numero_mes]) ? $meses[$numero_mes] : '';
                                ?>
                                <option value=""><?php echo esc_attr($nombre_mes); ?></option>
                                <?php
                                foreach ($meses as $numero => $nombre) {
                                    echo '<option value="' . $numero . '"';
                                    if ($numero == $numero_mes) {
                                        echo ' selected';
                                    }
                                    echo '>' . $nombre . '</option>';
                                }
                                ?>
                            </select>
                            </div>

                            <div class="input-nacimient ml-2">
                            <div class="input-nacimient ml-2">
                                <select class="form-control" name="year">
                                <option value=""><?php echo esc_attr(get_user_meta(get_current_user_id(), 'anio_nacimiento', true)); ?></option>
                                <?php
                                $anioActual = date("Y");
                                $anioNacimiento = get_user_meta(get_current_user_id(), 'anio_nacimiento', true);
                                $anioActual = intval(date("Y"))-6;
                                for ($i = $anioActual; $i >= 1900; $i--) {
                                    echo '<option value="' . $i . '"';
                                    if ($i == $anioNacimiento) {
                                        echo ' selected';
                                    }
                                    echo '>' . $i . '</option>';
                                }
                                ?>
                                </select>
                            </div>
                            </div>
                        </div>
                        <span class="sms">Esta información no se compartirá</span>
                </div>

                <?php
                    $user_id = get_current_user_id();
                    $codigo_pais = get_user_meta($user_id, 'codigo_pais', true);
                    $telefono = get_user_meta($user_id, 'telefono', true);
                    $recomendado = get_user_meta($user_id, 'recomendado', true);
                    
                    ?>

                    <div class="input-group phonedata mt-4">
                        <label for="">Teléfono </label><br>
                        <div class="d-flex">
                        <?php require get_stylesheet_directory() . '/inc/code-paises.php'; ?>
                        <select class="form-control" name="prefix" id="filtroSelect">
                            <?php if($codigo_pais){?>
                            <option value="<?php echo $codigo_pais; ?>"><?php echo $codigo_pais; ?></option>
                            <?php }else{?>
                                <option value="+34">+34</option>                           
                                <?php }?>
                            <?php asort($paises); foreach ($paises as $codigo => $prefijo): ?>
                                <option value="<?php echo $prefijo; ?>"><?php echo $prefijo; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <input type="tel" name="tel" id="tel" value="<?php echo esc_attr($telefono); ?>" onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
                           
                        </div>
                        <span class="sms"><?php esc_html_e('Esta información no se compartirá', 'libreriasocial'); ?></span>
                    </div>

                    <!--<div class="contenedor-checkbox">
                    <?php //if ($recomendado === 'on') {?>
                        <input type="checkbox" id="toggle-recomendados" name="checkrecomendados" checked>
                        <?php //}else{?>
                            <input type="checkbox" id="toggle-recomendados" name="checkrecomendados">
                        <?php //} ?>
                      <?php $user = wp_get_current_user(); if (!in_array('subscriber1', $user->roles)) {?>  <label for="toggle-recomendados" class="mt-3">  <?php esc_html_e('Añadir a recomendados', 'libreriasocial'); ?> </label><?php } ?>
                 
                   
                    </div>
                    <span class="mensajerecomendados" id="mensajerecomendado"><?php esc_html_e('Se descontará el 20% al precio establecido', 'libreriasocial'); ?></span>-->
                   <?php
                   //$recomendado = get_user_meta($user_id, 'recomendado', true);
                   $sharefriends = get_user_meta($user_id, 'sharefriends', true);
                   
                 
                   
                   if (isset($_POST['submit'])) {
                  
                        $recomendado_value = isset($_POST['checkrecomendados']) ? 'on' : 'off';
                       update_user_meta($user_id, 'recomendado', $recomendado_value);
                       $sharefriends_value = isset($_POST['sharefriends']) ? 'on' : 'off';
                       update_user_meta($user_id, 'sharefriends', $sharefriends_value);
                   }
                   
                   
                   ?>
                   
                    <div class="contenedor-checkbox1">
                    <!-- <?php if ($sharefriends === 'on') {?>
                        <input type="checkbox" id="toggle-sharefriends" name="sharefriends" checked>
                        <?php }else{?>
                        <input type="checkbox" id="toggle-sharefriends" name="sharefriends">
                        <?php } ?> -->

                        <input type="checkbox" id="toggle-sharefriends" name="sharefriendss" <?php echo $sharefriends === 'on' ? 'checked' : ''; ?>>


                        <label for="toggle-sharefriends" class="mt-3">  <?php esc_html_e('Mostrar amigos en mi perfil', 'libreriasocial'); ?> </label>
                    </div>
                    
        </div>

        <div class="col-md-6 dates">
            <div class="input-group">
                <label for="nombre"><?php esc_html_e('Nombre y apellidos', 'libreriasocial'); ?> <span>*</span></label>
                <input type="text" name="nombre" value="<?php echo esc_attr(get_user_meta(get_current_user_id(), 'first_name', true)); ?>" placeholder="<?php esc_html_e('Nombre', 'libreriasocial'); ?>" required>
            </div>

            <?php $user = wp_get_current_user(); if (!in_array('suscriber1', $user->roles)) {?>
            <div class="input-group">
                <label for="cuenta_bancaria"><?php esc_html_e('Cuenta Bancaria *', 'libreriasocial'); ?> </label>
                <input type="text" id="cuenta_bancaria" name="cuenta_bancaria" value="<?php echo esc_attr(get_user_meta(get_current_user_id(), 'cuenta_bancaria', true)); ?>" >
            </div>
            <?php } ?>
            
            <div class="input-group">
                <label for="email"><?php esc_html_e('Correo electrónico', 'libreriasocial'); ?> <span>*</span></label>
                <input type="text" name="email" value="<?php echo esc_attr(get_userdata(get_current_user_id())->user_email); ?>" required>
            </div>

            <div class="input-group">
                <label for="desc_perfil"><?php esc_html_e('Descripción del perfil', 'libreriasocial'); ?> </label>
                <textarea name="desc_perfil" maxlength="100"><?php echo esc_attr(get_user_meta(get_current_user_id(), 'desc_perfil', true)); ?></textarea>
            </div>
            <input type="hidden" name="oculto" value="editarperfil">
            <div class="input-group">
                <button id="update-profile-user" type="submit" name="update_profile" class="mt-2 boton-publicar"><?php  echo esc_html_e('Actualizar', 'libreriasocial'); ?></button>
            </div>

        </div>
      </form>
    </div>
</div>
<?php 
