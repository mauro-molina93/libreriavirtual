<div class="container setting">
 
  <div class="row">
            <div class="col-md-3 mb-3" style="margin: 0;padding: 0;">
                <ul class="nav nav-pills flex-column" id="myTab" role="tablist" style="margin: 0;">
                    <li class="nav-item mb-5">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            <span class="ml-3"><?php esc_html_e('Editar perfil', 'libreriasocial'); ?></span>
                            <i class="arrowbp ml-4 fa fa-angle-right" aria-hidden="true"></i></a>
                    </li>

                    <li class="nav-item mb-5 compras-pagos-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                            <span class="ml-3"><?php esc_html_e('Compras y pagos', 'libreriasocial'); ?></span>
                            <i class="arrowbp ml-4 fa fa-angle-right" aria-hidden="true"></i>
                            </a>
                    </li>

                    <li class="nav-item mb-5">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">
                            <i class="fa fa-lock" aria-hidden="true"></i> 
                            <span class="ml-3"><?php esc_html_e('Contraseña y seguridad', 'libreriasocial'); ?></span>
                            <i class="arrowbp fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </li>

                    <li class="nav-item mb-5">
                        <a class="nav-link" id="session-tab" data-toggle="tab" href="#session" role="tab" aria-controls="session-tab" aria-selected="false">
                            <i class="fa fa-sign-out" aria-hidden="true"></i> 
                            <span class="ml-3"><?php esc_html_e('Cerrar sesión', 'libreriasocial'); ?></span>
                            <i class="arrowbp ml-4 fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </li>
                </ul>
            </div>
  
        <div class="col-md-8 contenido-das" >
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <?php require get_stylesheet_directory() . '/template-parts/edit-profile.php'; ?>
                </div>

                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <?php require get_stylesheet_directory() . '/template-parts/cart-user.php'; ?>
                </div>

                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <?php require get_stylesheet_directory() . '/template-parts/password-reset.php'; ?>
                </div>

                <div class="tab-pane fade" id="session" role="tabpanel" aria-labelledby="session-tab">
                    <?php require get_stylesheet_directory() . '/template-parts/session-end.php'; ?>
                </div>
            </div>
        </div>

  </div>
  
  
  
</div>
