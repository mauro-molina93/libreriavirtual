<?php 
/**
 * Review Comments Template
 *
 * Closing li is left out on purpose!.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/review.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://woo.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">

	<div id="comment-<?php comment_ID(); ?>" class="comment_container">

		<?php
		/**
		 * The woocommerce_review_before hook
		 *
		 * @hooked woocommerce_review_display_gravatar - 10
		 */
		do_action( 'woocommerce_review_before', $comment );
		?>

		<div class="comment-text">

			<?php
			/**
			 * The woocommerce_review_before_comment_meta hook.
			 *
			 * @hooked woocommerce_review_display_rating - 10
			 */
			do_action( 'woocommerce_review_before_comment_meta', $comment );

			/**
			 * The woocommerce_review_meta hook.
			 *
			 * @hooked woocommerce_review_display_meta - 10
			 */
			?>
			<div class="d-flex">
			<?php
			do_action( 'woocommerce_review_meta', $comment );

			do_action( 'woocommerce_review_before_comment_text', $comment );

			do_action( 'woocommerce_review_comment_text', $comment );
			?>
			</div>
			<?php
			/**
			 * The woocommerce_review_comment_text hook
			 *
			 * @hooked woocommerce_review_display_comment_text - 10
			 */
			

			do_action( 'woocommerce_review_after_comment_text', $comment );
			?>
			
			<div class="comment-reply">
			<div>
				<?php
				  $comment_date = get_comment_time('U');
				  $human_time_diff = human_time_diff($comment_date, current_time('timestamp'));
				?>
				<span class="date-comment"><?php echo $human_time_diff; ?></span>
			</div>

				<?php
				comment_reply_link(
					array_merge(
						$args,
						array(
							'depth'     => $depth,
							'max_depth' => $args['max_depth'],
							'reply_text' => 'Responder', 
						)
					)
				);

				$current_user = get_current_user_id();
				$comment_author_id = (int) get_comment()->user_id;


				if ($current_user === $comment_author_id) {
					$comment_id = get_comment_ID();
					$delete_nonce = wp_create_nonce('delete-comment-nonce');
				?>
				<form method="post">
					<input type="hidden" name="delete_comment_id" value="<?php echo $comment_id; ?>" />
					<input type="hidden" name="delete_comment_nonce" value="<?php echo $delete_nonce; ?>" />
					<input type="submit" name="delete_comment_button" value="Eliminar" />
				</form>
				<?php
				}
				?>
		
			<?php
			$current_user = get_current_user_id();
			$comment_author_id = (int) get_comment()->user_id;
			if ( $current_user === $comment_author_id && current_user_can( 'edit_comment', $comment->comment_ID ) ) { 
				$edited_comment_content = get_comment_text( $comment->comment_ID ); 
				?>
			<div class="comment-edit-form">
				<a href="#" class="edit-comment-link"><?php esc_html_e('Editar', 'libreriasocial'); ?></a>

				<form method="post" class="edit-comment-form" style="display: none;">
					<textarea name="edited_comment_content" rows="4" cols="50"><?php echo esc_textarea( $edited_comment_content ); ?></textarea>
					<input type="hidden" name="edit_comment_id" value="<?php echo $comment->comment_ID; ?>" />
					<input type="submit" name="submit_edited_comment" class="boton-publicar" value="Editar" />
				</form>
			</div>

			<script>
			document.addEventListener("DOMContentLoaded", function() {
        var editLinks = document.querySelectorAll('.edit-comment-link');
        editLinks.forEach(function(link) {
            link.addEventListener('click', function(e) {
                e.preventDefault();
                var form = this.closest('.comment-edit-form').querySelector('.edit-comment-form');
                form.style.display = 'block';
            });
        });
    });
			</script>
			<?php

			if ( isset( $_POST['submit_edited_comment'] ) ) {
				$edited_comment_id = intval( $_POST['edit_comment_id'] );

				if ( $edited_comment_id ) {
					$edited_comment_content = sanitize_textarea_field( $_POST['edited_comment_content'] ); 
					wp_update_comment( array(
						'comment_ID' => $edited_comment_id,
						'comment_content' => $edited_comment_content,
					) );

					wp_safe_redirect( get_permalink() );
					exit();
				}
			}
		}
		?>	

			</div>
		</div>
	</div>

	<?php

		if (isset($_POST['delete_comment_button'])) {
			$delete_nonce = $_POST['delete_comment_nonce'];
			if (!wp_verify_nonce($delete_nonce, 'delete-comment-nonce')) {
				die('Nonce no válido');
			}

			$delete_comment_id = intval($_POST['delete_comment_id']);
			$current_user_id = get_current_user_id();
			// $comment_author_id = (int) get_comment($delete_comment_id);
			$comment = get_comment($delete_comment_id);
			$comment_author_id = (int) $comment->user_id;

			if (is_user_logged_in() && $current_user_id === $comment_author_id) {
				wp_delete_comment($delete_comment_id, true);
				wp_redirect(get_permalink());
				exit();
			}
		}

	?>