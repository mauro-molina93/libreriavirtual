<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

namespace BuddyX\Buddyx;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' );
require get_stylesheet_directory() . '/menu-lateral.php'; 
require get_stylesheet_directory() . '/inc/users.php';
$woocommerce_sidebar = get_theme_mod( 'woocommerce_sidebar_option', buddyx_defaults( 'woocommerce-sidebar-option' ) );
$producto_id = get_the_ID();
do_action( 'buddyx_before_content' );

if ( class_exists( 'WooCommerce' ) ) { ?>
		<?php if ( is_woocommerce() ) { ?>
			<?php if ( $woocommerce_sidebar == 'left' || $woocommerce_sidebar == 'both' ) : ?>
				<aside id="secondary" class="woo-left-sidebar widget-area">
					<div class="sticky-sidebar">
						<?php buddyx()->display_woocommerce_left_sidebar(); ?>
					</div>
				</aside>
			<?php endif; ?>
		<?php } ?>
	<?php
}

		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );

?>

		<?php
		while ( have_posts() ) :
			the_post();
			?>

           <section class="content-product-single comntainer">
             <div class="seccion-producto">
                <div class="row">
                <div class="col-md-3 carousel-producto">
                        <?php 
                        global $product;
                        $gallery_ids = $product->get_gallery_image_ids();
                        if ($gallery_ids) {
                            ?>
                            <!-- Slider de Bootstrap para la galería de imágenes -->
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <!-- Primera imagen será la imagen destacada -->
                                    <div class="carousel-item active">
                                        <?php the_post_thumbnail(); ?>
                                    </div>
                                    <!-- Iterar sobre las imágenes de la galería -->
                                    <?php foreach ($gallery_ids as $index => $gallery_id) { ?>
                                        <div class="carousel-item elemento-producto-img">
                                            <?php echo wp_get_attachment_image($gallery_id, 'full', false, array('class' => 'd-block w-100')); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <!-- Controles de navegación -->
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon bg-fondo-controls" aria-hidden="true"></span>
                                    <span class="sr-only bg-fondo-controls">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon bg-fondo-controls" aria-hidden="true"></span>
                                    <span class="sr-only bg-fondo-controls">Next</span>
                                </a>
                            </div>
                        <?php } else { the_post_thumbnail(); } ?>
                    </div>

    


                    <div class="col-md-5" id="seccion-grande">
                            <div class="datos-producto">
                                <?php 
                                $author_id = get_post_field( 'post_author', get_the_ID() ); 
                                $author_name = get_the_author_meta( 'display_name', $author_id ); 
                                $author_avatar = get_avatar( $author_id, 32 ); 
                                $last_activity = bp_get_user_last_activity( $author_id );
                                $last_activity_text = '';

                                if ( $last_activity ) {
                                    $last_activity_text = sprintf( __( '%s', 'libreriasocial' ), bp_core_time_since( $last_activity ) );
                                }
                            ?>
                            <div class="d-flex">
                                <div class="imagen-autor">
                                    <a href="<?php echo bp_core_get_user_domain( $author_id ); ?>">
                                        <?php echo $author_avatar; ?>
                                    </a>
                                </div>

                                <div class="datos-autor d-flex flex-column">
                                <a href="<?php echo bp_core_get_user_domain( $author_id ); ?>"><span class="ml-2 nombre"><?php echo esc_html( $author_name ); ?></span></a>
                                    <?php if ( ! empty( $last_activity_text ) ) : ?>
                                        <span class="ml-2 actividad"><?php echo $last_activity_text; ?></span>
                                    <?php endif; ?>
                                </div>
                                
                            </div>
                            <?php
                            $titulo = get_the_title(); 
                            $titulo_corto = (strlen($titulo) > 50) ? substr($titulo, 0, 50) . '...' : $titulo; 
                            ?>
                            <h2 class="titulo-libro"><?php echo $titulo_corto; ?></h2>
                            <!-- <h2 class="titulo-libro"><?php the_title(); ?></h2> -->
                            <div class="descripcion-libro">
                                <?php
                                $content = get_the_content();
                                $limited_content = wp_trim_words( $content, 75, '...' );

                               ?>
                                    <span id="des-corta">
                                        <?php  echo $limited_content; ?>
                                    </span>
                               <?php

                                if (strlen($content) > strlen($limited_content)) {
                                    ?>
                                    <span id="moreContent" style="display: none;">
                                        <?php echo $content; ?>
                                    </span>
                                    <?php
                                        $fullContent = "Aquí va el contenido completo que deseas mostrar.";
                                        $shortContent = substr($fullContent, 0, 100); 

                                        $isExpanded = false; 
                                    
                                        ?>

                                        <a href="#" class="showMoreLink d-block text-start" id="showMoreLink">
                                        Leer más
                                        <?php
                                        // if ($isExpanded) {
                                        //     esc_html_e('Leer Menos', 'libreriasocial');
                                        // } else {
                                        //     esc_html_e('Leer Más', 'libreriasocial');
                                        // }
                                        
                                        ?>
                                        </a>
                                    <div id="moreContent" style="display: <?php echo ($isExpanded ? 'inline' : 'none'); ?>">
                                        <?php echo $fullContent; ?>
                                    </div>
                                    <script>
                                        var showMoreLink = document.getElementById('showMoreLink');
                                        var moreContent = document.getElementById('moreContent');
                                        var desCorta = document.getElementById('des-corta');

                                        showMoreLink.addEventListener('click', function(e) {
                                            e.preventDefault();

                                            if (moreContent.style.display === 'none') {
                                                // alert("lerr menos");
                                                moreContent.style.display = 'inline';
                                                desCorta.style.display = 'none';
                                                showMoreLink.innerHTML = 'Leer menos';
                                                
                                            } else {
                                                // alert("lerr mas");
                                                moreContent.style.display = 'none';
                                                desCorta.style.display = 'block';
                                                showMoreLink.innerHTML = 'Leer más';
                                               
                                            }
                                        });
                                    </script>
                                    <?php
                                }
                                ?>
                            </div>      
                        </div>
                    </div>

                    <?php
                        $terms = get_the_terms( get_the_ID(), 'product_cat' );
                        $categoria = ''; 
                        if ( $terms && ! is_wp_error( $terms ) ) {
                            foreach ( $terms as $term ) {
                                $categoria .=  $term->name; 
                            }   
                        }
                    ?>


                    <div class="col-md-4 seccion-borde-lateral" id="seccion-pequena">
                       <div id="datos-producto" class="mb-5">
                        <div class="categoria"><strong><?php esc_html_e('Categoría', 'libreriasocial'); ?></strong>: <span><?php echo $categoria;?></span></div>
                        <div class="tema"><strong><?php esc_html_e('Tema', 'libreriasocial'); ?></strong>: <span><?php the_field('tema'); ?></span></div>
                        <div class="idioma"><strong><?php esc_html_e('Idioma', 'libreriasocial'); ?></strong>: <span><?php the_field( 'idioma' ); ?></span></div>
                        <?php
                            global $product;

                            if ($product->is_downloadable()) {
                                $downloads = $product->get_downloads();
                                if ($downloads) {
                                    $first_download = reset($downloads);
                                    $file_format = pathinfo($first_download['file'], PATHINFO_EXTENSION);
                                    echo '<div class="idioma"><strong>' . esc_html__('Formato', 'libreriasocial') . '</strong>: ' . esc_html($file_format) . '</div>';
                                }
                            }
                            ?>

                        <div class="mayorEdad">
                            <?php 
                             $apto = get_post_meta($post->ID, 'mayorEdad', true);
                            // var_dump($apto);
                            if($apto == "on"){
                                $si =  "+18";
                            }else{
                                $si =  "Apto para todos";
                            }
                     
                            ?>
                        <div class="apto"><strong><?php esc_html_e('Apto', 'libreriasocial'); ?></strong>: <?php echo $si; ?></div>
                            
                    </div>
                       </div>
                    <?php
                        $user_id = get_current_user_id();
                        $usuario_dio_like = get_user_meta($user_id, 'like_producto_' . $producto_id, true);

                        if ($user_id === 0){
                            $icon_class = 'fa fa-heart-o';
                           
                        }

                        if ($usuario_dio_like) {
                            $icon_class = 'fas fa-heart';
                        } else {
                            $icon_class = 'fa fa-heart-o';  
                        }

                        $productos_guardados = get_user_meta($user_id, 'productos_guardados', true);
                        $user_id = get_current_user_id();
                        
                        if (producto_esta_guardado_para_usuario($user_id, $producto_id)) {
                            $icon_class_guardado = 'fa-bookmark';
                        } else {
                            $icon_class_guardado = 'fa-bookmark-o';
                        }

                        
                    ?>

                    <div class="form-reviews" id="form-reviews">
                        <div class="delizable">
                            <div class="d-flex back" id="back"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="ml-1 mb-2"><?php echo esc_html_e('Ver detalles', 'libreriasocial'); ?></span></div>
                           
                             <?php 
                            if (comments_open()) {
                                comments_template();
                            }?>
                        </div>
                     
                    </div>

                    <script>
                    // jQuery(document).ready(function($) {
                    //     var input = $('.single-product .glsr-field.glsr-field-textarea textarea');
                    //     console.log(input);
                    //     if (input.length) {
                    //         input.attr('placeholder', 'Añade un comentario...');
                    //     }

                      
                    //     var submitButton = document.querySelector('button.glsr-button');

                    //     // Verificar si se encontró el botón antes de modificarlo
                    //     if (submitButton) {
                    //     // Eliminar el atributo 'data-loading' o cambiar su valor a una cadena vacía
                    //     submitButton.removeAttribute('data-loading');
                    //     // Opcionalmente, también podrías establecer el atributo 'aria-busy' en 'false'
                    //     submitButton.setAttribute('aria-busy', 'false');
                    //     } else {
                    //     console.log("No se encontró el botón de publicar.");
                    //     }

                    // });
                    </script>

                    <div class="mt-3 acciones-libros d-flex">
                        <div class="reaccion">
                            <i class="<?php echo $icon_class; ?> heart-icon" data-producto-id="<?php echo $producto_id; ?>"></i>
                        </div>
                        <div class="reaccion"> <i class="fas fa-comment" id="comentario-icono"></i></div>
                        <?php if ( is_user_logged_in() ) { ?>
                        <div class="reaccion guardar-producto" data-producto-id="<?php echo $producto_id; ?>">
                            <i class="fa <?php echo $icon_class_guardado; ?>"></i>
                        </div>
                        <?php } ?>
                    </div>
              
                    

                        <div class="d-flex reacciones contadores" id="ocultar1">
                            <span id="contador-<?php echo $producto_id; ?>" class="com">
                                <?php
                                    $contador_likes = get_post_meta($producto_id, 'contador_likes', true);
                                    if ($contador_likes === '' || $contador_likes === null) {
                                        $contador_likes = 0;
                                    }
                                    echo $contador_likes . esc_html(" me gusta");
                                ?>
                            </span>
                            <?php

                            global $product;
                            $args = array(
                                'post_id' => $product->get_id(),
                                'status' => 'approve',
                                'parent' => 0, 
                            );

                            $comments_query = get_comments($args);
                            $comment_count = count($comments_query);
                            $reply_count = 0;
                            foreach ($comments_query as $comment) {
                                $args = array(
                                    'post_id' => $product->get_id(),
                                    'status' => 'approve',
                                    'parent' => $comment->comment_ID, 
                                );
                                $replies = get_comments($args);
                                $reply_count += count($replies);
                            }

                            $total_comment_count = $comment_count + $reply_count;
                            ?>

                            <span class="pl-1 com"><?php echo $total_comment_count; ?> <span><?php esc_html_e("comentarios"); ?></span></span>

                        </div>

                        <div class="rating" id="ocultar2">
                            <?php
                                global $product;
                                $average_rating = $product->get_average_rating();
                                
                                if ($average_rating > 0) {
                                    $rating_html = wc_get_rating_html($average_rating);
                                    echo '<div class="woocommerce-product-rating">' . $rating_html . '</div>';
                                }
                            ?>
                        </div>

                    <div class="d-flex valor" id="ocultar3">
                        <div class="precio">
                            <span class="precio-libro">
                                <?php 
                                $product = wc_get_product(get_the_ID()); 
                                echo $product->get_price_html();
                                ?>
                            </span>
                        </div>
                        
                    <?php
                        $product_id = get_the_ID();
                        $sales_count = get_post_meta($product_id, 'total_sales', true);
                        // var_dump($sales_count);
                         if ($sales_count < 0) { $sales_count = 0; }
                       
                    ?>
                    <span class="vistas" id="ocultar4"><?php echo $sales_count . esc_html__(" ventas este mes", "libreriasocial"); ?></span>

                    </div>
                    <div class="group-boton d-flex" id="ocultar5">
                        <form class="cart" method="post" action="<?php echo esc_url( wc_get_cart_url() ); ?>">
                            <input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>">
                            <?php if (is_user_logged_in()) { if($product->get_price() == 0 && $product->is_downloadable()){}else{?>
                            <button type="submit" class="boton-publicar"><?php esc_html_e('Comprar', 'libreriasocial'); ?></button>
                            <?php }} ?>
                        </form>

                        <form class="cart" method="post" enctype='multipart/form-data'>
                            <input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>">
                            <?php if (is_user_logged_in()) { ?>
                                <?php if ($product->get_price() == 0 && $product->is_downloadable() ) { 
                                      $file_path = $product->get_downloads();
                                      foreach ($file_path as $download) {
                                        // Obtener la URL de descarga
                                        $file_url = $download['file'];
                                        echo '<a class="boton-publicar" href="'.$file_url.'">Descargar</a>';
                                    }
                                   
                                }else{   ?>
                              <button type="submit" class=" boton-publicar"><?php esc_html_e('Agregar al carrito', 'libreriasocial'); ?></button>
                                                                        
                             <?php  }} ?>
                        </form>
                    </div>

                    </div>

                   
                </div>
             </div>
           </section>


		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>

	<?php

	if ( class_exists( 'WooCommerce' ) ) {
		?>
		<?php if ( is_woocommerce() ) { ?>
			<?php if ( $woocommerce_sidebar == 'right' || $woocommerce_sidebar == 'both' ) : ?>
				<aside id="secondary" class="woo-primary-sidebar widget-area">
					<div class="sticky-sidebar">
						<?php buddyx()->display_woocommerce_right_sidebar(); ?>
					</div>
				</aside>
			<?php endif; ?>
		<?php } ?>
		<?php
	}

	do_action( 'buddyx_after_content' );

	get_footer( 'shop' );

	/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
