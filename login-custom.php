<?php 
 //ob_start();

 get_header();
 ?>
<div class="container" style="height: 100vh;">
    <div class="row">
            <div class="col-md-6 seccion-color seccion-responsive">
                <div class="d-flex flex-column align-items-center pt-4">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_stylesheet_directory_uri() . '/assents/images/logosi.png'; ?>" alt="" width="200px" height="200px" class="pt-5"></a>
                    <h4 class="text-center text-white title-registro"><?php esc_html_e('¡Bienvenido a la red social para escritores y lectores!', 'libreriasocial'); ?></h4>
                    <p class="text-white text-center"><?php esc_html_e('Lugar donde los amantes de las letras pueden reunirse, compartir sus historias y conectarse con mentes
    creativas afines', 'libreriasocial'); ?></p>
                </div>
            </div>

        <div class="col-md-6 login">
          <h2 class="title-page-registro text-center my-5 login-t" style=""><?php esc_html_e('Iniciar sesión', 'libreriasocial'); ?></h2>
               
                <?php wp_login_form(); ?>

                <!-- <div class="g-recaptcha" data-sitekey="6LcsN7UpAAAAAKVjZs0oNffyJ8U8xdkBocBqfH0Z" data-action="LOGIN"></div> -->
                <!-- <p><a href="<?php echo wp_lostpassword_url(); ?>"><?php esc_html_e('¿Has olvidado la contraseña?', 'libreriasocial'); ?></a></p> -->
                <?php
                 $login_error_code = isset($_GET['login_error']) ? $_GET['login_error'] : '';
    
                if ($login_error_code) {
                    switch ($login_error_code) {
                        case 'invalid_username':
                            $error_message = 'El nombre de usuario es incorrecto.';
                            break;
                        case 'incorrect_password':
                            $error_message = 'La contraseña es incorrecta.';
                            break;
                        default:
                            $error_message = 'Ha ocurrido un error al iniciar sesión.';
                            break;
                    }
                    ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error_message; ?>
                    </div>
                <?php } ?>

                <p class="register"><?php esc_html_e('¿No tienes cuenta?', 'libreriasocial')?> <a href="<?php echo home_url().'/registro/'?>"> <?php esc_html_e('Crear cuenta', 'libreriasocial'); ?></a></p>
            </div>
            </div>
        </div>
    </div>
</div>


 <div id="mensaje"></div>
 <?php


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    $nombre = $_POST['nombre'];
    $apellidos = $_POST['apellidos'];
    $username = $_POST['username'];
    $email = $_POST['email'];
    $contrasena = $_POST['contrasena'];
    $documento = $_POST['documento'];
    $direccion = $_POST['direccion'];
    $cuenta_bancaria = $_POST['cuenta_bancaria'];
    $telefono = $_POST['telefono'];
    $mayor_edad = isset($_POST['mayor_edad']) ? 1 : 0; 

    $userdata = array(
        'user_login' => $username,
        'user_pass' => $contrasena,
        'user_email' => $email,
        'first_name' => $nombre,
        'last_name' => $apellidos,
    );
    $user_id = wp_insert_user($userdata);

    
    if (!is_wp_error($user_id)) {
        update_user_meta($user_id, 'documento', $documento);
        update_user_meta($user_id, 'direccion', $direccion);
        update_user_meta($user_id, 'cuenta_bancaria', $cuenta_bancaria);
        update_user_meta($user_id, 'telefono', $telefono);
        update_user_meta($user_id, 'mayor_edad', $mayor_edad);

        echo '<div id="mensaje">Registro exitoso</div>';
    } else {
        echo '<div id="mensaje">Error al registrar usuario</div>';
    }
}
get_footer();