<?php 
 //ob_start();
 /**
  * Template Name: Login Pass To remeber
  */

 get_header();
 ?>
<div class="container" style="height: 100vh;">
    <div class="row">
            <div class="col-md-6 seccion-color seccion-responsive">
                <div class="d-flex flex-column align-items-center pt-4">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_stylesheet_directory_uri() . '/assents/images/logosi.png'; ?>" alt="" width="400px" height="400px" class="pt-5"></a>
                    <h4 class="text-center text-white title-registro"><?php esc_html_e('¡Bienvenido a la red social para escritores y lectores!', 'libreriasocial'); ?></h4>
                    <p class="text-white text-center"><?php esc_html_e('Lugar donde los amantes de las letras pueden reunirse, compartir sus historias y conectarse con mentes
    creativas afines', 'libreriasocial'); ?></p>
                </div>
            </div>

        <div class="col-md-6 login login-remember-page">
            <h2 class="title-page-registro text-center my-5" style=""><?php esc_html_e('Recupera tu contraseña', 'libreriasocial'); ?></h2>
            <p class="text-center text-to-remeber"><?php esc_html_e('Te enviaremos un correo a:', 'libreriasocial'); ?></p>
            <form id="lost-password-form" action="<?php echo esc_url(network_site_url('wp-login.php?action=lostpassword', 'login_post')); ?>" method="post">
                <p class="input-remeber">
                    <!-- <label for="user_login"><?php _e('Username or Email'); ?></label> -->
                    <input type="text" name="user_login" id="user_login" class="input" value="" size="20" autocapitalize="off" />
                    <script>
                        document.addEventListener('DOMContentLoaded', function() {
                            var userLoginInput = document.getElementById('user_login');
                            userLoginInput.placeholder = 'Introduzca su nombre de usuario o correo electrónico';
                        });
                    </script>
                </p>

                <input type="hidden" name="redirect_to" value="<?php echo esc_url(home_url('/acceder')); ?>" />

                <p class="submit reset-pass-btn">
                    <input type="submit" name="submit" class="boton-publicar" value="<?php esc_attr_e('Enviar'); ?>" />
                </p>
            </form>
        </div>
            </div>
        </div>
    </div>
</div>


 <?php
get_footer();