<?php 
 //ob_start();

 get_header();
 ?>
<div class="container page-registro" style="height: 100vh;">
    <div class="row">
            <div class="col-md-6 seccion-color ">
                <div class="d-flex flex-column align-items-center pt-4">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_stylesheet_directory_uri() . '/assents/images/logosi.png'; ?>" alt=""  class="pt-5"></a>
                    <h4 class="text-center text-white title-registro"><?php esc_html_e('¡Bienvenido a la red social para escritores y lectores!', 'libreriasocial'); ?></h4>
                    <p class="text-white text-center"><?php esc_html_e('Lugar donde los amantes de las letras pueden reunirse, compartir sus historias y conectarse con mentes
    creativas afines', 'libreriasocial'); ?></p>
                </div>
            </div>

        <div class="col-md-6 registro">
            <h2 class="title-page-registro text-center my-5"><?php esc_html_e('Crear cuenta', 'libreriasocial'); ?></h2>
            <form id="formulario-registro" method="post">
                <div class="col-md-12" id="opciones-usuario">
                    <label for="nombre"></label>
                    <input type="text" name="nombre" id="nombre" autocomplete="off" required placeholder="<?php esc_attr_e('Nombre y apellidos*', 'lireriasocial')?>"><br>

                    <!-- <label for="apellidos"></label>
                    <input type="text" name="apellidos" id="apellidos" required placeholder="<?php esc_attr_e('Apellidos*', 'lireriasocial')?> "><br> -->

                    <label for="username"></label>
                    <input type="text" name="username" id="username" autocomplete="off" required placeholder="<?php esc_attr_e('Nombre de Usuario*', 'lireriasocial')?>"><br>

                    <label for="email"></label>
                    <input type="email" name="email" id="email" autocomplete="off" required placeholder="<?php esc_attr_e('Correo Electrónico*', 'libreriasocial'); ?>"><br>

                    <input type="hidden" class="valor_email" id="valor_email">
                    <label for="contrasena"></label>
                    <div class="password-input">
                    <input type="password" name="contrasena" autocomplete="off" id="contrasena" required placeholder="<?php esc_attr_e('Contraseña*', 'libreriasocial'); ?>">
                    <button type="button" id="togglePassword"><img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/eye.png'; ?>" alt="" width="20px" height="20px"></button>
                    </div>

                    <script>
                        const togglePasswordButton = document.getElementById('togglePassword');
                        const passwordInput = document.getElementById('contrasena');

                        togglePasswordButton.addEventListener('click', function() {
                            if (passwordInput.type === 'password') {
                            passwordInput.type = 'text';
                            // togglePasswordButton.textContent = 'Ocultar contraseña';
                            } else {
                            passwordInput.type = 'password';
                            // togglePasswordButton.textContent = 'Mostrar contraseña';
                            }
                        });
                    </script>


                    <label for="contrasena2"></label>
                    <div class="password-input">
                    <input type="password" name="contrasena2" id="contrasena2" autocomplete="off" required placeholder="<?php esc_attr_e('Confirmar contraseña* ', 'libreriasocial'); ?>"><br>
                    <button type="button" id="togglePassword2"><img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/eye.png'; ?>" alt="" width="20px" height="20px"></button>
                    </div>

                    <script>
                        const togglePasswordButton2 = document.getElementById('togglePassword2');
                        const passwordInput2 = document.getElementById('contrasena2');

                        togglePasswordButton2.addEventListener('click', function() {
                            if (passwordInput2.type === 'password') {
                            passwordInput2.type = 'text';
                            // togglePasswordButton.textContent = 'Ocultar contraseña';
                            } else {
                            passwordInput2.type = 'password';
                            // togglePasswordButton.textContent = 'Mostrar contraseña';
                            }
                        });
                    </script>

                    <div class="mensaje-pass d-flex">
                        <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/i.png'; ?>" alt="">
                        <span class="mesnaje"><?php esc_html_e('La contraseña debe tener al menos 8 caracteres, una mayúscula, una minúscula, un número y un caracter especial', 'libreriasocial'); ?></span>
                    </div>
                </div>

                <div class="col-md-12 " id="opciones-escritor">
                    <div id="go_back_registro" class="mb-2 iratras-registro">
                        <div class="d-flex align-items-center">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i>
                            <span class="ml-2"><?php esc_html_e('Ir atrás', 'libreriasocial'); ?></span>
                        </div>
                    </div>

                    <label for="documento"></label>
                    <input type="text" name="documento" autocomplete="off" id="documento" placeholder="<?php esc_attr_e('Documento de Identidad: ', 'libreriasocial'); ?>">

                    <label for="direccion"> </label>
                    <input type="text" name="direccion" autocomplete="off" id="direccion" placeholder="<?php esc_attr_e('Dirección: ', 'libreriasocial'); ?>">
                    <input type="hidden" name="autor" id="autor" value="1">

                    <label for="cuenta_bancaria"> </label>
                    <input type="text" name="cuenta_bancaria" autocomplete="off" id="cuenta_bancaria" placeholder="<?php esc_attr_e('Cuenta Bancaria*', 'libreriasocial'); ?>">

                    <label for="telefono"></label>
                    <input type="number" name="telefono" autocomplete="off" id="telefono" placeholder="<?php esc_attr_e('Número de Teléfono:', 'libreriasocial'); ?> ">
                    <script>
                        // Obtener el input del teléfono
                        var inputTelefono = document.getElementById('telefono');

                        // Agregar un listener para el evento 'input'
                        inputTelefono.addEventListener('input', function() {
                            var valor = this.value;
                           
                            var valorNumerico = '';

                            for (var i = 0; i < valor.length; i++) {
                                if (!isNaN(parseInt(valor[i]))) {
                                    valorNumerico += valor[i];
                                }
                            }
                            this.value = valorNumerico;
                        });
                    </script>
                    <div class="contenedor-checkbox" style="display: flex; align-items:center;justify-content:center;margin-left:10%;">
                        <input type="checkbox" name="mayor_edad" id="mayor_edad">
                        <label for="mayor_edad" class=""><?php esc_html_e('+18 años de edad', 'libreriasocial'); ?></label>
                    </div>
                    <div class="contenedor-checkbox" style="display: flex; margin-left:10%;">
                    <input type="checkbox" name="terms" id="terms"> 
                    <label for="terms" class=""><?php esc_html_e('Leer Términos y condiciones', 'libreriasocial'); echo descargar_terminos();?></label>
                    </div>
                </div>
         
                <div class="col-md-12">
                <input type="hidden" name="oculto" value="registro">
                <div class="g-recaptcha" data-sitekey="6LcsN7UpAAAAAKVjZs0oNffyJ8U8xdkBocBqfH0Z" data-action="LOGIN"></div>
               

                <script>
                //     document.addEventListener("DOMContentLoaded", function() {
                //         setTimeout(function() {
       
                //         var recaptchaTextarea = document.getElementById('g-recaptcha-response');
                //         console.log(recaptchaTextarea);
        
                //         if (recaptchaTextarea) {
                //         recaptchaTextarea.setAttribute('required', 'required');
                //         console.log(recaptchaTextarea);
                //         console.log(recaptchaTextarea.value);
                //        }

                //        let form = document.getElementById('submit');

                //     //    form.addEventListener('click', function (event) {
                //     //      if (recaptchaTextarea.value !== '') {
                //     //          console.log("es diferente con un valor de " + recaptchaTextarea.value);
                //     //     } else {
                //     //          Swal.fire({ title:"Introduzca el CAPTCHA",text:"",icon:"error"});
                //     //      }
                //     //     });

   
                //     }, 5000); 
                // });

                </script>

                    <div class="d-flex justify-content-center botones-acciones">
                        <button type="submit" id="submit" class="boton-publicar"><?php esc_html_e('Registrarse', 'libreriasocial'); ?></button>
                        <a href="#" class="boton-publicar" id="boton-escitor"><?php esc_html_e('¿Vas a publicar?', 'libreriasocial'); ?></a>
                        <a href="<?php echo esc_url(home_url()); ?>" class="boton-publicar" id="boton-cancelar"><?php esc_html_e('Cancelar', 'libreriasocial'); ?></a>
                    </div>
                   <p class="register crear"><?php esc_html_e('¿Ya tienes cuenta? ', 'libreriasocial')?> <a href="<?php echo home_url().'/acceder/'?>"> <?php esc_html_e('Inicia sesión', 'libreriasocial'); ?></a></p>
                </div>
            </form>
        </div>
    </div>
</div>


 <div id="mensaje"></div>
<?php
get_footer();