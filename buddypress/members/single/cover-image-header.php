<?php
/**
 * BuddyPress - Users Cover Image Header
 *
 * @since 3.0.0
 * @version 7.0.0
 */

?>

<?php if ( ! bp_is_user_change_avatar() && ! bp_is_user_change_cover_image() ) : ?>
 
	
	<div class="item-header-cover-image-wrapper mt-5" style="width: 75%; margin-left: 20%; border: none;">
		<div id="item-header-cover-image">
			<div id="item-header-avatar">
				<?php if ( bp_is_my_profile() && ! bp_disable_avatar_uploads() ) { ?>
				<a href="<?php bp_members_component_link( 'profile', 'change-avatar' ); ?>" class="link-change-profile-image bp-tooltip" data-bp-tooltip-pos="up" data-bp-tooltip="<?php esc_attr_e( 'Change Profile Photo', 'buddyx' ); ?>">
					<i class="fa fa-camera"></i>
				</a>
				<?php } ?>
				<div class="avatar-wrap">
				<?php
					echo get_avatar( bp_displayed_user_id(), 96 ); 
					$user = get_userdata( bp_displayed_user_id() );
				?>
				</div>
			</div><!-- #item-header-avatar -->
			<input type="hidden" id="id_user" name="id_user" value="<?php echo bp_displayed_user_id();?>">

			<div id="item-header-content" class="flex justify-content-around">

			<h2 class="user-nicename"><?php  echo $user->user_nicename;// bp_profile_field_data( 'field=name' ); ?></h2>
			
				<?php if ( bp_nouveau_member_has_meta() ) : ?>
					<div class="item-meta">
						<p class="display-name"><?php  echo $user->display_name;?></p>
						<?php 
							$user_id = bp_displayed_user_id();
							$user_description = wpautop(get_user_meta($user_id, 'desc_perfil', true));
							echo $user_description;
						?>

					</div>
					
				<?php endif; ?>
				<div class="datos-inventory">
		
				<?php
				if ( function_exists( 'bp_member_type_list' ) ) :
					bp_member_type_list(
						bp_displayed_user_id(),
						array(
							'label'        => array(
								'plural'   => __( 'Member Types', 'buddyx' ),
								'singular' => __( 'Member Type', 'buddyx' ),
							),
							'list_element' => 'span',
						)
					);
				endif;
				?>

				<div class="member-header-actions-wrap">		
					
					<?php
					echo do_shortcode('[follow_button]');
					// $user_id = bp_displayed_user_id();
					// $followed_users = get_user_meta($user_id, 'followed_users', true);
					// $followers_count = count($followed_users);
					
					if (!empty($followed_users)) {
						$follower_names = array();
					
						foreach ($followed_users as $follower_id) {
							$follower_data = get_userdata($follower_id);
					
							if ($follower_data) {
								$follower_names[] = $follower_data->display_name;
							}
						}
					
						// Ahora puedes usar $follower_names para mostrar los nombres de los seguidores
						foreach ($follower_names as $follower_name) {
							echo $follower_name . '<br>';
						}
					}
					//echo 'Cantidad de seguidores: ' . $followers_count;

				
					?>

					<?php
					if ( is_user_logged_in() && bp_is_active( 'friends' ) ) {
						bp_add_friend_button( bp_displayed_user_id() );
					}
					?>

					<?php
					if ( is_user_logged_in() && bp_is_active( 'messages' ) ) {
						bp_send_private_message_button( bp_displayed_user_id() );
					}
					?>

					<!-- <?php
					bp_nouveau_member_header_buttons(
						array(
							'container'         => 'ul',
							'button_element'    => 'button',
							'container_classes' => array( 'member-header-actions' ),
						)
					);
					?> -->
					
					<!--<a class="boton-publicar mensaje" href="<?php //echo home_url('/mensajes');?>">Mensaje privado</a>-->
				</div>

				<div class="datosDelUsuario">
				
				<div class="segundo-dato">
	<?php echo do_shortcode('[following_count]') . ' seguidos'; ?>
	<div id="seguidos">
	<svg xmlns="http://www.w3.org/2000/svg" width="14" height="9" viewBox="0 0 14 9" fill="none">
<path d="M13 1L7 7L1 1" stroke="#4A148C" stroke-width="2" stroke-linecap="round"/>
</svg>
	</div>
</div>

<div class="primer-dato">
	<?php echo do_shortcode('[follower_count]') . ' seguidores'; ?>
	<div id="seguidores">
	<svg xmlns="http://www.w3.org/2000/svg" width="14" height="9" viewBox="0 0 14 9" fill="none">
<path d="M13 1L7 7L1 1" stroke="#4A148C" stroke-width="2" stroke-linecap="round"/>
</svg>
	</div>
</div>

<div class="tercer-dato">
	<?php
		
		$args = array(
			'post_type' => 'product',
			'author'    => bp_displayed_user_id(),
			'posts_per_page' => -1, // para obtener todos los productos
		);

		$products_query = new WP_Query( $args );
		$products_count = $products_query->post_count;
		?>

	<p><?php echo $products_count; ?> <?php echo  esc_html_e(' publicaciones', 'libreriasocial'); ?></p>
	<div id="publicaciones">
	<svg xmlns="http://www.w3.org/2000/svg" width="14" height="9" viewBox="0 0 14 9" fill="none">
<path d="M13 1L7 7L1 1" stroke="#4A148C" stroke-width="2" stroke-linecap="round"/>
</svg>
	</div>
</div>
</div>

				</div><!-- .member-header-actions-wrap -->
				<script>
					document.addEventListener("DOMContentLoaded", function() {
					let link = document.querySelector("a.friendship-button");

					link.addEventListener("click", function(event) {
						event.preventDefault(); // Para evitar que se siga el enlace

						if (link.innerHTML.trim() === "Añadir amigo") {
							link.innerHTML = "Cancelar solicitud de amistad";
						
						} else if (link.innerHTML.trim() === "Cancelar solicitud de amistad") {
							link.innerHTML = "Añadir amigo";
						
						} else if (link.innerHTML.trim() === "Cancelar amistad") {
							link.innerHTML = "Añadir amigo";
						}
					});
				});
				</script>
			</div><!-- #item-header-content -->
			
			<hr class="separador-hr my-2">
		</div><!-- #item-header-cover-image -->
			<div class="cuerpo-perfil">
				<div class="amigos">
					<?php echo do_shortcode('[mostrar_amigos]'); ?>
				</div>
				<div class="productos-perfil mt-5">
					<?php echo do_shortcode('[mostrar_productos_creados]'); ?>
				</div>
				
			</div>
	</div><!-- .item-header-cover-image-wrapper -->

	
	<?php
endif;
?>
