<?php
/**
 * BuddyPress - Members Home
 *
 * @since   1.0.0
 * @version 3.0.0
 */

$bp_nouveau_appearance = bp_get_option( 'bp_nouveau_appearance' );
?>

<?php bp_nouveau_member_hook( 'before', 'home_content' ); ?>

<?php require get_stylesheet_directory() . '/menu-lateral.php'; ?>

<div id="item-header" role="complementary" data-bp-item-id="<?php echo esc_attr( bp_displayed_user_id() ); ?>" data-bp-item-component="members" class="users-header single-headers">

	<?php bp_nouveau_member_header_template_part(); ?>

</div>

</div>
<?php bp_nouveau_member_hook( 'after', 'home_content' ); ?>
