<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package buddyx
 */

namespace BuddyX\Buddyx;

?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<?php do_action( 'buddyx_head_top' ); ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<?php do_action( 'buddyx_head_bottom' ); ?>
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			<?php if (is_user_logged_in()) {
				$user_id = get_current_user_id();
				$meta_value = get_user_meta($user_id, 'account_status', true);
				if ($meta_value === 'pending') { ?>
					Swal.fire({
						title: "Su cuenta no ha sido validada, espere recibir su correo.",
						text: "",
						icon: "error"
					}).then(function() {
						window.location.href = '<?php echo wp_logout(); ?>'; 
					});
				<?php }
			} ?>
		});
	</script>
</head>

<body <?php body_class(); ?>>

<?php do_action( 'buddyx_body_top' ); ?>

<?php buddyx_site_loader(); ?>
<?php buddyx_wp_body_open(); ?>
<div id="page" class="site">
<?php do_action( 'buddyx_page_top' ); ?>
	<a class="skip-link screen-reader-text" href="<?php echo esc_url( '#primary' ); ?>"><?php esc_html_e( 'Skip to content', 'buddyx' ); ?></a>

	<?php do_action( 'buddyx_header_before' ); ?>

	<?php if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'header' ) ) { ?>
		<div class="site-header-wrapper bg-general">
			<div class="container">
				<header id="masthead" class="site-header">
	
					<?php get_template_part( 'template-parts/header/custom_header' ); ?>

					<?php  get_template_part( 'template-parts/header/branding' ); ?>

					<?php get_template_part( 'template-parts/header/navigation' ); ?>
				</header><!-- #masthead -->
			</div>
		</div>

		
		<div class="buscador-buddypress menu-icons-wrapper buscador-movil">
				<div class="search" <?php echo apply_filters( 'buddyx_search_slide_toggle_data_attrs', '' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>>
					<?php get_search_form(); ?>  
				</div>
		</div>
	<?php } ?>

	<?php do_action( 'buddyx_header_after' ); ?>

<?php
$classes = get_body_class();
if ( ! in_array( 'page-template-full-width', $classes ) ) {
	?>
	<div class="container2">
<?php } ?>
