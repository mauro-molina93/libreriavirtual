<?php

function agregar_metabox_compartir() {
    add_meta_box(
        'metabox-compartir',
        'Datos Extras',
        'contenido_metabox_compartir',
        'product', 
        'normal',
        'default'
    );
}
add_action('add_meta_boxes', 'agregar_metabox_compartir');


function contenido_metabox_compartir($post) {
    $compartir = get_post_meta($post->ID, 'compartir', true);
    $amigosAmigos = get_post_meta($post->ID, 'amigosAmigos', true);
    $mayorEdad = get_post_meta($post->ID, 'mayorEdad', true);
    $recomendados = get_post_meta($post->ID, 'recomendados', true);

    //Amigos de mis amigos
    $amigosAmigosChecked = $amigosAmigos === 'amigosAmigos' ? 'checked' : '';

    //Check de recomendados
    $check_recomedados = get_post_meta($post->ID, 'checkrecomendados', true);

    //Obtener precios
    $precio_con_descuento = get_post_meta($post->ID, 'precio_con_descuento', true );
    $precio_con_iva = get_post_meta($post->ID, 'precioconiva', true);

    //Obtener porcentajes de iva + Descuentos
    $iva = get_post_meta($post->ID, 'iva', true);
    $descuento = get_post_meta($post->ID, 'descuento', true);
    $descuento_recomendados = get_post_meta($post->ID, 'descuento_recomendados', true);

    //Precios con descuentos aplicados
    $precio_descuento_sin_recomendar = get_post_meta($post->ID, 'precio_descuento_sin_recomendar', true);
    $precio_descuento_recomendado = get_post_meta($post->ID, 'precio_descuento_recomendado', true);

    if (gettype($compartir) == "array") {
        $cadena = implode(', ', $compartir);
    }else { 
        $cadena = $compartir;
    }


    // var_dump($recomendados);
    ?>

    <div class="input-group">
        <strong><?php echo esc_html_e('Compartir para: ', 'libreriasocial'); ?></strong>
        <div>
            <label for="share_solo"><?php esc_html_e('Solo yo', 'libreriasocial'); ?></label>
            <input type="radio" name="share" id="share_solo" value="solo" <?php checked($cadena, 'solo') ?>>
        </div>
        <div>
            <label for="share_amigos"><?php esc_html_e('Amigos', 'libreriasocial'); ?></label>
            <input type="radio" name="share" id="share_amigos" value="amigos" <?php checked($cadena, 'amigos') ?>>
        </div>
        <div>
            <label for="share_todos"><?php esc_html_e('Todos', 'libreriasocial'); ?></label>
            <input type="radio" name="share" id="share_todos" value="todos" <?php checked($cadena, 'todos') ?>>
        </div>
        <div>
            <label for="amigosAmigos"><?php esc_html_e('Amigos de mis amigos', 'libreriasocial'); ?></label>
            <?php
            $amigosAmigosValue = get_post_meta($post->ID, 'amigosAmigos', true);
            $amigosAmigosChecked = !empty($amigosAmigosValue) ? 'checked' : '';
            ?>
            <input type="checkbox" name="amigosAmigos" id="amigosAmigos" value="amigosAmigos" <?php echo $amigosAmigosChecked; ?>>
        </div>
    </div> 

    <hr>

    <div class="input-group">
        <strong for=""><? echo esc_html_e('Opciones de recomendados', 'libreriasocial'); ?></strong>
        
        <?php
            if ($check_recomedados === 'on') {
                ?>
                    <p><?php esc_html_e('Este libro fue marcado como recomendado'); ?></p>
                <?php
            }else {
                ?>
                    <p><?php esc_html_e('Este libro no marcado como recomendado'); ?></p>
                <?php
            }
        ?>

        <label for="" style="width: 200px; display:inline-block"><?php echo "Precio con iva de " . $iva . "% : " ?></label>
        <input type="number" name="precioiva" id="precioiva"  value="<?php echo $precio_con_iva; ?>" disabled>
        <br><br>

        <?php
            if ($check_recomedados === ''){
                ?>
                    <label for="" style="width: 200px; display:inline-block"><?php echo "Precio con descuento de " . $descuento . "%"; ?></label>
                    <input type="number" name="precioiva" id="precioiva"  value="<?php echo $precio_descuento_sin_recomendar; ?>" disabled>
                    <br><br>
                <?php
            }else {
                ?>
                    <label for="" style="width: 200px; display:inline-block"><?php echo "Precio con descuento de " . $descuento_recomendados . "% por ser recomendado"; ?></label>
                    <input type="number" name="precioiva" id="precioiva"  value="<?php echo $precio_descuento_recomendado; ?>" disabled>
                    <br><br>
                <?php
            }
        ?>

        <label for="" style="width: 200px; display:inline-block"><?php esc_html_e('Precio a recibir por el autor', 'libreriasocial'); ?></label>
        <input type="number" name="preciocondescuento" id="preciocondescuento"  value="<?php echo $precio_con_descuento; ?>" disabled>
    </div> 


    <hr>

    <div class="input-group">
        <label for="mayorEdad"><?php esc_attr_e('Contenido +18 años de edad', 'libreriasocial'); ?> </label>
        <input type="checkbox" name="mayorEdad" id="mayorEdad" <?php checked($mayorEdad, 'on') ?>>
    </div>



    <?php
}


function guardar_metabox_compartir($post_id) {
    $compartir = isset($_POST['share']) ? $_POST['share'] : array();
    $mayorEdad = isset($_POST['mayorEdad']) ? $_POST['mayorEdad'] : '';
    $recomendados = isset($_POST['recomendados']) ? $_POST['recomendados'] : '';


    $amigosAmigos = isset($_POST['amigosAmigos']) ? $_POST['amigosAmigos'] : '';
    $amigosAmigosValue = !empty($amigosAmigos) ? 'amigosAmigos' : false;

    update_post_meta($post_id, 'compartir', $compartir);
    update_post_meta($post_id, 'mayorEdad', $mayorEdad);
    update_post_meta($post_id, 'amigosAmigos', $amigosAmigos);
    update_post_meta($post_id, 'recomendados', $recomendados);
}

add_action('save_post', 'guardar_metabox_compartir');

