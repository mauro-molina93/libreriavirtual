<?php
global $wpdb;
$usuarios = $wpdb->prefix . 'users';
$registros = $wpdb->get_results("SELECT * FROM $usuarios", ARRAY_A );

require 'agregar-comentario-historia.php'; 
$usuario_logueado = get_current_user_id();

$friends = friends_get_friend_user_ids(get_current_user_id());

$registros = array_filter($registros, function($registro) use ($friends) {
    return in_array($registro['ID'], $friends);
});

$usuario_logueado_id = get_current_user_id();
$usuario_logueado_encontrado = in_array($usuario_logueado_id, array_column($registros, 'ID'));

if (!$usuario_logueado_encontrado) {
    $usuario_logueado_info = array(
        'ID' => $usuario_logueado_id,
        'display_name' => get_the_author_meta('display_name', $usuario_logueado_id),
    );
    array_unshift($registros, $usuario_logueado_info);
}
?>
<div class="bg-claro">
<div class="contenedor-usuarios d-flex container-horizontal-scroll">
    <?php foreach ($registros as $registro): ?>
        <?php
        $user_id = $registro['ID'];
        $count_posts_de = count_user_posts($user_id);

        if ($count_posts_de > 0 || $user_id == get_current_user_id() ) {
            if ($user_id == get_current_user_id() && $count_posts_de == 0) {
                ?>
                    <div class="usuario">
                        <a href="/publicar">
                            <?php  $avatar_url = get_avatar_url($user_id); ?>
                            <img class="usuario-avatar" src="<?php echo $avatar_url; ?>" />
							<div class="add-history">+</div>
                        </a>
						
                        <h2 class="usuario-nombre"><?php echo "Agregar historias destacadas";//echo $registro['display_name'];  ?></h2>
                    </div>
                <?php
            } else {
                ?>
                <div class="usuario">
                    <a href="#" data-toggle="modal" data-target="#usuario_<?php echo $registro['ID']; ?>">
                        <?php  $avatar_url = get_avatar_url($user_id); ?>
                        <img class="usuario-avatar" src="<?php echo $avatar_url; ?>" />
                    </a>
                    <h2 class="usuario-nombre"><?php echo $registro['display_name']; ?></h2>
                </div>
                <?php
            }
        ?>

        
        <?php } endforeach; ?>

    <!-- Modals -->
    <?php foreach ($registros as $registro): ?>
    <div style="height: 100vh;" class="modal fade" id="usuario_<?php echo $registro['ID']; ?>" tabindex="-1" role="dialog" aria-labelledby="usuario_<?php echo $registro['ID']; ?>_label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width:60rem; width: 60rem; display: flex; justify-content: center; padding: 0; margin:0 auto;">
            <div class="modal-content" style="position:relative; z-index: 999999999999 !important;background: none; border-none !important;">
                <div class="modal-body" style="background: none;">
                    <?php
                    $args = array(
                        'author' => $registro['ID'], 
                        'post_type' => 'post', 
                        'posts_per_page' => -1, 
                    );
                    
                    $author_query = new WP_Query($args);
                    $post_count = $author_query->post_count;
                    if ($author_query->have_posts()) {
                        ?>
                        <div id="carouselExampleControls_<?php echo $registro['ID']; ?>" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" style="">
                                <?php
                                $count = 0;
                                while ($author_query->have_posts()): $author_query->the_post();
                                    setup_postdata($post);
                                    $avatar_url = get_avatar_url($registro['ID']);
                                    ?>
                                    <?php $post_date = get_the_date('Y-m-d H:i:s', $post->ID); ?>
                                    <div class="carousel-item <?php echo ($count == 0) ? 'active' : ''; ?>" style="position: relative; ">
                                        <div class="historia-vista" style="position: absolute;">
                                            <div class="d-flex">
                                                <?php $author_id = get_post_field( 'post_author', get_the_ID() ); ?>
                                                <a href="<?php echo bp_core_get_user_domain( $author_id ); ?>">
                                                    <img class="usuario-avatar" src="<?php echo $avatar_url; ?>" />
                                                </a>
                                                <div class="d-flex flex-column justify-content-center" style="width: 95%; margin-left: 2%; ">
                                                
                                                <div class="d-flex cantidad-historia mb-2">
                                                    <?php
                                                    
                                                    $inner_div_width = 100 / $post_count;
                                                    for ($i = 0; $i < $post_count; $i++) {
                                                        echo '<div style="flex-grow: 1;"></div>'; 
                                                    }

                                                    ?>
                                                    
                                        
                                                </div>
                                                <h5 class="modal-title" id='usuario_<?php echo $registro['ID']; ?>_label'>
                                                    <?php echo $registro['display_name']; ?>
                                                </h5>

                                                <?php
                                                $post_date_obj = new DateTime($post_date);
                                                $current_date = new DateTime();
                                                $interval = $current_date->diff($post_date_obj);
                                                ?>

                                                <span class="post-date">
                                                    <?php 
                                                    
                                                    if ($interval->y > 0) {
                                                        echo 'Hace ' . $interval->y . ' años';
                                                    } elseif ($interval->m > 0) {
                                                        echo 'Hace ' . $interval->m . ' meses';
                                                    } elseif ($interval->d > 0) {
                                                        echo 'Hace ' . $interval->d . ' días';
                                                    } elseif ($interval->h > 0) {
                                                        echo 'Hace ' . $interval->h . ' horas';
                                                    } elseif ($interval->i > 0) {
                                                        echo 'Hace ' . $interval->i . ' minutos';
                                                    } else {
                                                        echo 'Hace unos segundos';
                                                    }
                                                    ?>
                                                </span>
                                            </div>
                                            
                                            </div>
                                        </div>
                                        <?php the_post_thumbnail('large', array('class' => 'd-block')); ?>
                                        <div class="footer-info">
                                        <div class="description-history">
										
                                        <?php if ( get_the_content() ) {
                                             the_content(); }else{
                                                echo '&nbsp;';}
                                                ?>
										
										<?php 
															
                                                            global $post;
                                                            $comentario_autor_id = $post->post_author;
                                                            $usuario_actual_id = get_current_user_id();
                                                        
                                                            if ($usuario_actual_id != $comentario_autor_id) { 
                                                       ?>
                                                       <form action="" method="post" >
                                                           <div class="comments-history">
                                                               <input type="hidden" name="oculto" value="smsprivado">
                                                               <textarea name="message_content" placeholder="Escribe tu mensaje aquí" required></textarea>
                                                               <input type="hidden" name="post_id" value="<?php echo get_the_ID(); ?>">
                                                               <button type="submit"><?php esc_html_e('Enviar', 'libreriasocial'); ?></button>
                                                           </div>
               
                                                           <div class="ml-2 heart-history">
                                                               <img class="heart-like" src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/corazon.png'; ?>" alt="" width="35px" height="35px">
                                                           </div>
                                                       </form>
                                                       <?php } ?>
										
										                                   
                                        </div>
                                    </div>
                                    </div>
                                    <?php
                                    $count++;
                                endwhile;
                                ?>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls_<?php echo $registro['ID']; ?>" role="button" data-slide="prev">
                                <div class="contenedor-controles"><span class="carousel-control-prev-icon" aria-hidden="true"></span></div>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls_<?php echo $registro['ID']; ?>" role="button" data-slide="next">
                                <div class="contenedor-controles"><span class="carousel-control-next-icon" aria-hidden="true"></span></div>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <?php
                        wp_reset_postdata();
                    }
                    ?>
                </div>
                <!-- <div class="modal-footer">
                   
                </div> -->
            </div>
        </div>
    </div>
<?php endforeach; ?>

</div>
</div>