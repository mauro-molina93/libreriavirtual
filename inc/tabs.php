<div id="exTab1" class="container">	
<ul  class="nav nav-pills tabs <?php echo is_user_logged_in() ? '' : 'mt-5'; ?>">
			<li class="active">
       			 <a  href="#1a" data-toggle="tab" class="active show"><?php echo esc_html_e('Novedades', 'libreriasocial'); ?></a>
			</li>
			<li>
				<a href="#2a" data-toggle="tab"><?php echo esc_html("Recomendados", 'libreriasocial'); ?></a>
			</li>
			<li>
				<a href="#3a" data-toggle="tab"><?php esc_html_e('Más vendidos', 'libreriasocial'); ?></a>
			</li>
  		    <li>
			<!-- <a href="#4a" data-toggle="tab"><?php echo esc_html_e('Filtros', 'libreriasocial'); ?> -->
	
			<button id="clear-filters" style="display:none"><span><?php esc_html_e('Restablecer', 'libreriasocial'); ?></span></button>
			<label class="dropdown filtros" style="margin-bottom: 0;padding-bottom: 2px">
				<div class="dd-button" style="display: flex !important; align-items:center;">
					<!-- <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/filtros.png'; ?>" alt="" width="20px" height="20px"> -->
					<div class="ml-2">
						<svg xmlns="http://www.w3.org/2000/svg" width="14" height="12" viewBox="0 0 17 16" fill="none">
						<g clip-path="url(#clip0_10_928)">
							<path d="M7.02032 16C6.75511 16 6.50075 15.8946 6.31322 15.7071C6.12568 15.5196 6.02032 15.2652 6.02032 15V9.00001L0.220322 1.60001C0.109132 1.46087 0.0397594 1.293 0.0202849 1.11596C0.000810293 0.938918 0.0320352 0.759987 0.110323 0.600005C0.178268 0.416075 0.303201 0.258664 0.4669 0.150731C0.630598 0.0427974 0.8245 -0.0100119 1.02032 5.3274e-06H15.0203C15.2103 -0.013787 15.3998 0.031713 15.5628 0.130227C15.7258 0.228741 15.8542 0.375414 15.9303 0.550005C16.0086 0.709987 16.0398 0.888918 16.0204 1.06596C16.0009 1.243 15.9315 1.41087 15.8203 1.55001L10.0203 9.00001V15C10.0203 15.2652 9.91497 15.5196 9.72743 15.7071C9.53989 15.8946 9.28554 16 9.02032 16H7.02032ZM8.02032 8.30001L13.0203 2.00001H3.07032L8.02032 8.30001Z" fill="#4A148C"/>
						</g>
						<defs>
							<clipPath id="clip0_10_928">
							<rect width="16.04" height="16" fill="white"/>
							</clipPath>
						</defs>
						</svg>
					</div>
					<span id="btn-filter"><?php echo esc_html_e('Filtros', 'libreriasocial'); ?></span>
				</div>

				<input type="checkbox" class="dd-input" id="test">

				<ul class="dd-menu" id="menu">
					<?php require get_stylesheet_directory() . '/inc/filtros.php'; ?>
				</ul>
			</label>

				<script>
					
					var autorDropdown = document.getElementById('autor-dropdown-menu');

					autorDropdown.addEventListener('change', function() {
						var selectedOption = autorDropdown.querySelector('input[type="radio"]:checked');
						var selectedUserId = selectedOption ? selectedOption.value : null;

					
					});
				</script>
				<!-- </a> -->
			</li>
		</ul>

			<div class="tab-content clearfix">
			  <div class="tab-pane active" id="1a">
			  		<?php require get_stylesheet_directory() . '/inc/Home/queris-product.php'; ?>
			  </div>

			  <div class="tab-pane" id="2a">
			  		<?php require get_stylesheet_directory() . '/inc/Home/queris-product-recomendados.php'; ?>
			  </div>

              <div class="tab-pane" id="3a">
			  		<?php require get_stylesheet_directory() . '/inc/Home/queris-product-mas-vendidos.php'; ?>
			  </div>

             
			</div>
  </div>