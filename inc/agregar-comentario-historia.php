<!-- <?php
// Inicializar la sesión si no está iniciada
if (!session_id()) {
    session_start();
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["comment_content"]) {
    // Verificar si todos los campos del formulario están presentes
    if (isset($_POST['user_id']) && isset($_POST['post_id']) && isset($_POST['comment_content'])) {
        // Obtener los datos del formulario
        $user_id = $_POST['user_id'];
        $post_id = $_POST['post_id'];
        $comment_content = $_POST['comment_content'];

        // Crear el comentario en la entrada actual
        $comment_data = array(
            'comment_post_ID' => $post_id,
            'comment_author' => $user_id, // Puedes usar el ID del usuario como nombre del autor
            'comment_content' => $comment_content,
            'comment_approved' => 1 // Aprobamos el comentario automáticamente
        );

        // Insertar el comentario en la base de datos
        $comment_id = wp_insert_comment($comment_data);

        // Redireccionar de vuelta a la entrada
        if ($comment_id) {
            // Redireccionar después de establecer la variable de sesión
            $_SESSION['comment_posted'] = true;
            wp_redirect(get_permalink($post_id));
            exit;
        } else {
            echo "Error al publicar el comentario.";
        }
    }
}

// Verificar si se ha establecido la variable de sesión para mostrar un mensaje de éxito
if (isset($_SESSION['comment_posted']) && $_SESSION['comment_posted']) {
    echo "<script>alert('Comentario publicado con éxito');</script>";
    unset($_SESSION['comment_posted']); // Limpiar la variable de sesión
}
?> -->