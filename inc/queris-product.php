<?php

    $args = array( 
        'post_type' => 'product',
        'posts_per_page' => 4 
    );

    $libros = new \WP_Query($args);
    ?>

     <div class="libros-home row px-4"> <?php
        while ($libros->have_posts()) : $libros->the_post();
        $producto_id = get_the_ID();
    ?>

  <div class="col-md-6">  
    <div class="libro my-4">
        <div class="contenedor-libro d-flex">
            <div class="imagen-libro">
                <?php the_post_thumbnail(); ?>
            </div>

            <div class="contenido-libro pl-2">
                <!-- Author -->
                <div class="autor d-flex">
                    <?php 
                        $author_id = get_post_field( 'post_author', get_the_ID() ); 
                        $author_name = get_the_author_meta( 'display_name', $author_id ); 
                        // $author_avatar = get_avatar( $author_id, 32 ); 
                        
                        $author_icon = bp_core_fetch_avatar( array( 'item_id' => $author_id, 'type' => 'thumb', 'html' => false ) );
                    ?>
                    <div class="imagen-autor">
                        <?php echo $author_icon; ?>
                    </div>
                    <div class="datos-autor">
                        <span><?php echo esc_html( $author_name ); ?></span>
                    </div>
                </div>

                <!-- Author -->

                <!-- contenido -->

                <div class="descripcion-libro">
                    <h2 class="titulo-libro"><?php the_title(); ?></h2>
                    <p class="extract-medium">
                        <?php echo wp_trim_words(get_the_excerpt(), 6, '...'); ?>
                        <span><a href="<?php the_permalink(); ?>" class="showMoreLink d-block"><?php esc_html_e('Leer más', 'libreriasocial'); ?></a></span>
                    </p>

                    <p class="extract-large">
                        <?php echo wp_trim_words(get_the_excerpt(), 20, '...'); ?>
                        <span><a href="<?php the_permalink(); ?>" class="showMoreLink d-block"><?php esc_html_e('Leer más', 'libreriasocial'); ?></a></span>
                    </p>
                </div>
              
                <div class="funciones">
                    <div class="d-flex">
                        <img src="<?php get_stylesheet_directory() . './assents/svg/buscar.png' ?>" alt="">
                    </div>

                    <?php
                        $user_id = get_current_user_id();
                        $usuario_dio_like = get_user_meta($user_id, 'like_producto_' . $producto_id, true);

                        if ($user_id === 0){
                            $icon_class = 'fa fa-heart-o';
                           
                        }

                        if ($usuario_dio_like) {
                            $icon_class = 'fas fa-heart';
                        } else {
                            $icon_class = 'fa fa-heart-o';  
                        }

                        $productos_guardados = get_user_meta($user_id, 'productos_guardados', true);
                        $user_id = get_current_user_id();
                        
                        if (producto_esta_guardado_para_usuario($user_id, $producto_id)) {
                            $icon_class_guardado = 'fa-bookmark';
                        } else {
                            $icon_class_guardado = 'fa-bookmark-o';
                        }

                        
                    ?>

                    <div class="acciones-libros d-flex">
                        <div class="reaccion">
                            <i class="<?php echo $icon_class; ?> heart-icon" data-producto-id="<?php echo $producto_id; ?>"></i>
                        </div>
                        <div class="reaccion"> <i class="fas fa-comment"></i></div>
                        <div class="reaccion guardar-producto" data-producto-id="<?php echo $producto_id; ?>">
                            <i class="fa <?php echo $icon_class_guardado; ?>"></i>
                        </div>
                    </div>
              
                    <div id="contador-<?php echo $producto_id; ?>">
                        <?php
                            $contador_likes = get_post_meta($producto_id, 'contador_likes', true);
                            echo $contador_likes . esc_html(" me gusta");
                        ?>
                    </div>

                    <div class="rating">
                        <?php 
                            echo do_shortcode('[cantidad_resenas producto_id="' . $product_id . '"]');
                        ?>
                        </div>
                    </div>

                    <div class="precio">
                        <span class="precio-libro">
                            <?php 
                            $product = wc_get_product(get_the_ID()); 
                            echo $product->get_price_html();
                            ?>
                        </span>
                    </div>

                    <div class="mayorEdad">
                            <?php 
                            //  $apto = get_post_meta($post->ID, 'mayorEdad', true);
                            // var_dump($apto);
                            // if($apto == "on"){
                            //     echo "Es apto";
                            // }else{
                            //     echo "No es apto";
                            // }
                               
                            ?>
                    </div>

                 

                    <div class="acciones">
                        <a class="boton" href="<?php the_permalink(); ?>"><?php echo esc_html_e('Leer más', 'libreriasocial'); ?></a>
                        <form class="cart" method="post" enctype='multipart/form-data'>
                            <input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>">
                            <button type="submit" class="boton"><?php esc_html_e('Agregar al carrito', 'libreriasocial'); ?></button>
                        </form>
                    </div>
                </div>
                <!-- contenido -->

            </div>
        </div>
    </div>
</div>

    <?php endwhile; wp_reset_postdata(); ?>
   

    </div>