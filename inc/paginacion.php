<div class="pagination" id="paginador">
    <?php
        echo paginate_links( array(
            'total' => $libros->max_num_pages,
            'current' => max( 1, get_query_var( 'page' ) ),
        ) );
    ?>
</div>