<div class="container mt-5">
        <form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <input type="text" value="" name="s" id="s" placeholder="Buscar usuarios">
            <input type="submit" id="searchsubmit" value="Buscar">
            <input type="hidden" name="bp_search" value="true">
        </form>

        <?php
       
        $search_terms = isset($_GET['s']) ? $_GET['s'] : '';
        
        if (!empty($search_terms) && bp_has_members('search_terms=' . urlencode($search_terms))) :
        ?>
            <ul>
                <?php while (bp_members()) : bp_the_member(); ?>
                    <li><a href="<?php bp_member_permalink(); ?>"><?php bp_member_name(); ?></a></li>
                <?php endwhile; ?>
            </ul>
<?php
            if ( bp_is_active( 'members' ) ) {
    
    echo "BuddyPress está activo. Los usuarios están en la red de BuddyPress.";
} else {
    echo "BuddyPress no está activo. Los usuarios no están en la red de BuddyPress.";
} ?>
        <?php endif; ?>
        
<?php
?>
    <div class="row">
        <div class="col-md-6">
            <h2>Recientes</h2>

        </div>

        <div class="col-md-6">
            <h2>Borrar todo</h2>
        </div>
    </div>
</div>