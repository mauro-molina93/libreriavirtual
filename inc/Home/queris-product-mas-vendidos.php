<?php

    $args = array( 
    'post_type'      => 'product',
    //'posts_per_page' => 4,
    'meta_key'       => 'total_sales',
    'orderby'        => 'meta_value_num', 
    'order'          => 'DESC',
    'page'          => $page
    );

    $libros = new \WP_Query($args);
    ?>

     <div class="libros-home row d-flex px-4"> <?php
        while ($libros->have_posts()) : $libros->the_post();
        $producto_id = get_the_ID();

        $author = intval(get_the_author_meta('ID'));
          // Obtener el valor del campo compartir
        $compartir = get_post_meta($producto_id, 'compartir', true);
        $amigosAmigosValue = get_post_meta($producto_id, 'amigosAmigos', true);

        //visualizar solo yo
        if (in_array('solo', $compartir) && get_the_author_meta('ID') !== $current_user_id ) {
            continue; 
        } 

        //visualizar amigo
        if (in_array('amigos', $compartir) && ($amigosAmigosValue != 'amigos_de_amigos')) {
           
            //si es anonimo
            if ($current_user_id === 0) {                    
                continue;
            } else {
             
                // echo "aqui";
                global $wpdb;
                $usuarios = $wpdb->prefix . 'users';
                $registros = $wpdb->get_results("SELECT * FROM $usuarios", ARRAY_A );
        
                $friends = friends_get_friend_user_ids($author);
                if (!in_array($current_user_id, $friends) && !( ($author == $current_user_id) )) {
                    continue; 
                }

            }
        }

        //visualizar amigo de mis amigos 
        if (in_array('amigos', $compartir) && ($amigosAmigosValue == 'amigos_de_amigos') ) {
            if ($current_user_id === 0) {
                continue;
            } else {
               
                global $wpdb;
                $usuarios = $wpdb->prefix . 'users';
                $registros = $wpdb->get_results("SELECT * FROM $usuarios", ARRAY_A );
        
                $author = intval(get_the_author_meta('ID'));
                $friends = friends_get_friend_user_ids($author);

                $amigos_de_mis_amigos = array();
                foreach ($friends as $amigo) {
                    $x = friends_get_friend_user_ids($amigo);
                  
                    $amigos_de_mis_amigos = array_merge($amigos_de_mis_amigos, $x); 
                    }

                $amigos_de_mis_amigos = array_unique($amigos_de_mis_amigos);

                if (!in_array($current_user_id, $amigos_de_mis_amigos) && !( ($author == $current_user_id) )) {
                    continue; 
                }
            }  
        }
            
        $productos_mostrados[] = $producto_id;
          ?>


  <div class="col-md-6">  
  <div class="libro my-2 <?php echo is_user_logged_in() ? 'libro-altura' : 'libro-altura-nologin'; ?>"">
        <div class="contenedor-libro d-flex">
            <div class="imagen-libro">
            <div class=" carousel-producto">
                        <?php 
                        global $product;
                        $gallery_ids = $product->get_gallery_image_ids();
                        if ($gallery_ids) {
                            ?>
                            <!-- Slider de Bootstrap para la galería de imágenes -->
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <!-- Primera imagen será la imagen destacada -->
                                    <div class="carousel-item active">
                                        <?php the_post_thumbnail(); ?>
                                    </div>
                                    <!-- Iterar sobre las imágenes de la galería -->
                                    <?php foreach ($gallery_ids as $index => $gallery_id) { ?>
                                        <div class="carousel-item elemento-producto-img">
                                            <?php echo wp_get_attachment_image($gallery_id, 'full', false, array('class' => 'd-block w-100')); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <!-- Controles de navegación -->
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon bg-fondo-controls" aria-hidden="true"></span>
                                    <span class="sr-only bg-fondo-controls">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon bg-fondo-controls" aria-hidden="true"></span>
                                    <span class="sr-only bg-fondo-controls">Next</span>
                                </a>
                            </div>
                        <?php } else { the_post_thumbnail(); } ?>
                    </div>
            </div>

            <div class="contenido-libro pl-2">
                <!-- Author -->
                <div class="autor d-flex">
                    <?php 
                        $author_id = get_post_field( 'post_author', get_the_ID() ); 
                        $author_name = get_the_author_meta( 'display_name', $author_id ); 
                        $author_avatar = get_avatar( $author_id, 32 ); 
                    ?>
                    <div class="imagen-autor">
                        <a href="<?php echo bp_core_get_user_domain( $author_id ); ?>">
                            <?php echo $author_avatar; ?>
                        </a>
                    </div>
                    <?php 
                         $last_activity = bp_get_user_last_activity( $author_id );
                         $last_activity_text = '';

                         if ( $last_activity ) {
                             $last_activity_text = sprintf( __( '%s', 'libreriasocial' ), bp_core_time_since( $last_activity ) );
                         }
                    ?>
                    <div class="datos-autor d-flex flex-column">
                        <span class="ml-2 nombre"><?php echo esc_html( $author_name ); ?></span>
                        <?php if ( ! empty( $last_activity_text ) ) : ?>
                            <span class="ml-2 actividad"><?php echo $last_activity_text; ?></span>
                        <?php endif; ?>
                    </div>
                </div>

                <!-- Author -->

                <!-- contenido -->

                <div class="descripcion-libro">
                    <?php
                    $titulo = get_the_title(); 
                    $titulo_corto = (strlen($titulo) > 50) ? substr($titulo, 0, 50) . '...' : $titulo; 
                    ?>
                    <h2 class="titulo-libro"><?php echo $titulo_corto; ?></h2>
                    <p class="extract-medium">
                        <?php echo wp_trim_words(get_the_excerpt(), 10, '...'); ?>
                        <span><a href="<?php the_permalink(); ?>" class="showMoreLink d-block"><?php esc_html_e('Leer más', 'libreriasocial'); ?></a></span>
                    </p>

                    <p class="extract-large">
                        <?php echo wp_trim_words(get_the_excerpt(), 20, '...'); ?>
                        <span><a href="<?php the_permalink(); ?>" class="showMoreLink d-block"><?php esc_html_e('Leer más', 'libreriasocial'); ?></a></span>
                    </p>
                </div>

                <div class="funciones">
                    <div class="d-flex">
                        <img src="<?php get_stylesheet_directory() . './assents/svg/buscar.png' ?>" alt="">
                    </div>

                    <?php
                        $user_id = get_current_user_id();
                        $usuario_dio_like = get_user_meta($user_id, 'like_producto_' . $producto_id, true);

                        if ($user_id === 0){
                            $icon_class = 'fa fa-heart-o';
                           
                        }

                        if ($usuario_dio_like) {
                            $icon_class = 'fas fa-heart';
                        } else {
                            $icon_class = 'fa fa-heart-o';  
                        }

                        $productos_guardados = get_user_meta($user_id, 'productos_guardados', true);
                        $user_id = get_current_user_id();
                        
                        if (producto_esta_guardado_para_usuario($user_id, $producto_id)) {
                            $icon_class_guardado = 'fa-bookmark';
                        } else {
                            $icon_class_guardado = 'fa-bookmark-o';
                        }

                        
                    ?>

                    <div class="acciones-libros d-flex">
                        <div class="reaccion">
                            <i class="<?php echo $icon_class; ?> heart-icon" data-producto-id="<?php echo $producto_id; ?>"></i>
                        </div>
                        <div class="reaccion"> <i class="fas fa-comment"></i></div>
                        <?php if ((is_user_logged_in()) || (in_array( 'invitadoLib', $user->roles ) ) ) { ?>
                        <div class="reaccion guardar-producto" data-producto-id="<?php echo $producto_id; ?>">
                            <i class="fa <?php echo $icon_class_guardado; ?>"></i>
                        </div>
                        <?php } ?>
                    </div>
              
                    <div class="d-flex">
                    <div id="contador-<?php echo $producto_id; ?>" class="cotador-megusta">
                        <?php
                            $contador_likes = get_post_meta($producto_id, 'contador_likes', true);
                            if ($contador_likes === '' || $contador_likes === null) {
                                $contador_likes = 0;
                            }
                            echo $contador_likes . esc_html(" me gusta");
                        ?>
                    </div>

                    <div class="cotador-megusta">
                        <?php 
                            $product_id = get_the_ID();
                            $glsr_reviews = get_post_meta($product_id, '_glsr_reviews', true);
                        ?>
                         <?php

                            global $product;
                            $args = array(
                                'post_id' => $product->get_id(),
                                'status' => 'approve',
                                'parent' => 0, 
                            );

                            $comments_query = get_comments($args);
                            $comment_count = count($comments_query);
                            $reply_count = 0;
                            foreach ($comments_query as $comment) {
                                $args = array(
                                    'post_id' => $product->get_id(),
                                    'status' => 'approve',
                                    'parent' => $comment->comment_ID, 
                                );
                                $replies = get_comments($args);
                                $reply_count += count($replies);
                            }

                            $total_comment_count = $comment_count + $reply_count;
                            ?>

                            <span class="ml-3 com"><?php echo $total_comment_count; ?> <span><?php esc_html_e("comentarios"); ?></span></span>

                        <!-- <span class="pl-2 com"><?php echo $glsr_reviews; ?> <span><?php  esc_html_e(" comentarios "); ?></span></span> -->
                    </div>
                    </div>

                    <div class="rating">
                        <?php 
                            echo do_shortcode('[cantidad_resenas producto_id="' . $product_id . '"]');
                        ?>
                    </div>

                    <div class="precio">
                        <span class="precio-libro">
                            <?php 
                            $product = wc_get_product(get_the_ID()); 
                            echo $product->get_price_html();
                            ?>
                        </span>
                    </div>

                    <div class="mayorEdad">
                            <?php 
                            //  $apto = get_post_meta($post->ID, 'mayorEdad', true);
                            // var_dump($apto);
                            // if($apto == "on"){
                            //     echo "Es apto";
                            // }else{
                            //     echo "No es apto";
                            // }
                               
                            ?>
                    </div>

                 

                    <div class="acciones d-flex">
                        <a class="boton" href="<?php the_permalink(); ?>"><?php echo esc_html_e('Leer más', 'libreriasocial'); ?></a>
                        <form class="cart" method="post" enctype='multipart/form-data'>
                            <input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>">
                            <?php if (is_user_logged_in()) { ?>
                                <?php if ( ($product->get_price() == 0 || $product->get_price() == "") && $product->is_downloadable() ) { 
                                      $file_path = $product->get_downloads();
                                      foreach ($file_path as $download) {
                                        // Obtener la URL de descarga
                                        $file_url = $download['file'];
                                        echo '<div class="entre"></div><a class="boton" href="'.$file_url.'" >Descargar</a>';
                                    }                                   
                                }else{   ?>
                            <button type="submit" class="boton"><?php esc_html_e('Agregar al carrito', 'libreriasocial'); ?></button>
                            <?php } ?>
                            <?php  } ?>

                        </form>
                    </div>
                </div>
                <!-- contenido -->

            </div>
        </div>
    </div>
</div>

    <?php endwhile; wp_reset_postdata(); ?>
   

    </div>

    <?php require get_stylesheet_directory() . '/inc/paginacion.php'; ?>