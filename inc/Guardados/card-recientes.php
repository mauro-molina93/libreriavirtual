<?php $current_user_id = get_current_user_id(); ?>
<div class=" mb-2 container-horizontal-scroll">
    <div class="row">
        <div class="card-group">
            <?php
                $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => 3,
                    'orderby' => 'post_date', // Ordenar por fecha de publicación
                    'order' => 'DESC', // Orden descendente, los últimos primero
                );

                $product_card = new \WP_Query($args);

                while( $product_card->have_posts() ) :
                    $product_card->the_post();
                    $author_id = get_post_field( 'post_author', get_the_ID() ); 
                    $author_name = get_the_author_meta( 'display_name', $author_id ); 

                    $producto_id = get_the_ID();

                    $author = intval(get_the_author_meta('ID'));
                      // Obtener el valor del campo compartir
                    $compartir = get_post_meta($producto_id, 'compartir', true);
                    $amigosAmigosValue = get_post_meta($producto_id, 'amigosAmigos', true);
        
                    //visualizar solo yo
                    if (in_array('solo', $compartir) && get_the_author_meta('ID') !== $current_user_id ) {
                        continue; 
                    } 
        
                    //visualizar amigo
                    if (in_array('amigos', $compartir) && ($amigosAmigosValue != 'amigos_de_amigos')) {
                       
                        //si es anonimo
                        if ($current_user_id === 0) {                    
                            continue;
                        } else {
                         
                            // echo "aqui";
                            global $wpdb;
                            $usuarios = $wpdb->prefix . 'users';
                            $registros = $wpdb->get_results("SELECT * FROM $usuarios", ARRAY_A );
                    
                            $friends = friends_get_friend_user_ids($author);
                            if (!in_array($current_user_id, $friends) && !( ($author == $current_user_id) )) {
                                continue; 
                            }
        
                        }
                    }
        
                    //visualizar amigo de mis amigos 
                    if (in_array('amigos', $compartir) && ($amigosAmigosValue == 'amigos_de_amigos') ) {
                        if ($current_user_id === 0) {
                            continue;
                        } else {
                           
                            global $wpdb;
                            $usuarios = $wpdb->prefix . 'users';
                            $registros = $wpdb->get_results("SELECT * FROM $usuarios", ARRAY_A );
                    
                            $author = intval(get_the_author_meta('ID'));
                            $friends = friends_get_friend_user_ids($author);
        
                            $amigos_de_mis_amigos = array();
                            foreach ($friends as $amigo) {
                                $x = friends_get_friend_user_ids($amigo);
                              
                                $amigos_de_mis_amigos = array_merge($amigos_de_mis_amigos, $x); 
                                }
            
                            $amigos_de_mis_amigos = array_unique($amigos_de_mis_amigos);
        
                            if (!in_array($current_user_id, $amigos_de_mis_amigos) && !( ($author == $current_user_id) )) {
                                continue; 
                            }
                        }  
                    }
                        
                    $productos_mostrados[] = $producto_id;
                    ?>
                    <div class="card mx-3 d-flex card-libro">
                        <?php the_post_thumbnail(); ?>
                        <div class="card-body">
                            <h5 class="card-title"><?php the_title(); ?></h5>
                            <?php 
                                $product = wc_get_product(get_the_ID()); 
                                $precio_producto = $product->get_price_html();
                                ?>
                            <p class="card-text"><?php echo wp_trim_words(get_the_content(), 20, '...'); ?></p>
                            <p class="author-card"><?php echo esc_html_e("Autor: ", "libreriasocial") . esc_html( $author_name ); ?></p>
                            <strong><?php echo $precio_producto; ?></strong>
                            <a href="<?php the_permalink(); ?>" class="boton-publicar" style="background: none !important;">Ver publicacion</a>
                        </div>
                    </div>
                    <?php
                endwhile;
                wp_reset_postdata();
            ?>
        </div>
    </div>
</div>