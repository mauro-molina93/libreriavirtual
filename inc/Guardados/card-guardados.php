<div class=" container-horizontal-scroll mb-2">
    <div class="row">
        <div class="card-group">
            <?php
                $args = array(
                    'post_type' => 'product',
                );

                $product_card = new \WP_Query($args);
                $user_id = get_current_user_id();
                $productos_guardados = get_user_meta($user_id, 'productos_guardados', true);

                while( $product_card->have_posts() ) :
                    $product_card->the_post();
                    $author_id = get_post_field( 'post_author', get_the_ID() ); 
                    $author_name = get_the_author_meta( 'display_name', $author_id ); 

                    if ($user_id === 0){
                        echo "Usuario no logueado";
                        exit();
                    }
                    
                    if (empty($productos_guardados)){
                        echo "No hay productos guardados";
                       // exit();
                       break;
                    }
               
                    if (in_array(get_the_ID(), $productos_guardados)) {
                        ?>
                        <div class="card mx-3 d-flex card-libro card-guardados">
                            <?php the_post_thumbnail(); ?>
                            <div class="card-body">
                                <h5 class="card-title"><?php the_title(); ?></h5>
                                <?php 
                                    $product = wc_get_product(get_the_ID()); 
                                    $precio_producto = $product->get_price_html();
                                    ?>
                                <p class="card-text"><?php echo wp_trim_words(get_the_content(), 20, '...'); ?></p>
                                <p class="author-card"><?php echo esc_html_e("Autor: ", 'libreriasocial') . esc_html( $author_name ); ?></p>
                                <strong><?php echo $precio_producto; ?></strong>
                                <a href="<?php the_permalink(); ?>" class="boton-publicar" style="background: none !important;">Ver publicacion</a>
                            </div>
                        </div>
                        <?php
                    }
                endwhile;
                wp_reset_postdata();
            ?>
        </div>
    </div>
</div>