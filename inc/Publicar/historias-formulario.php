<div class="container mt-4">
    <form method="post"  enctype="multipart/form-data" class="formulario-historia"  onsubmit="submitForm(event)">
        <div class="row justify-content-center mt-4">
            <div class="col-md-3">
                <!-- <div class="creador-historia" id="creador-historias">
                    <button id="eliminar-imagen" class="btn btn-danger" style="display:none;">Eliminar imagen</button>
                    <h4 class="titulo-historia"><?php esc_html_e('Arrastra tu foto aquí', 'libreriasocial'); ?></h4>
                    <label for="imagenhistoria" class="btn btn-outline-primary"><?php esc_html_e('Selecciona del ordenador', 'libreriasocial'); ?></label>
                    <input type="file" name="imagenhistoria" id="imagenhistoria">
                </div> -->
                <div id="modal" class="modal">
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <img id="modal-image" src="" alt="Modal Image">
                </div>
                </div>
                <div class="form__container" id="upload-container">
                    <div id="desaparecer_cargando">
                        <h4 class="titulo-historia"><?php esc_html_e('Arrastra tu foto aquí', 'libreriasocial'); ?></h4>
                        <label for="imagenhistoria" class="boton-publicar"><?php esc_html_e('Selecciona del ordenador', 'libreriasocial'); ?></label>
                    </div>
                    <div id="overlay" class="overlay"></div>
                    <label class="dropdown" id="eliminar-h-container">
                        <div class="dd-button" style="display: flex !important; align-items:center;">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/threepoints.png'; ?>" alt="" width="46px" height="11px">
                        </div>

                        <input type="checkbox" class="dd-input" id="test">

                        <ul class="dd-menu">
                            <li id="eliminar-h"><?php esc_html_e('Eliminar', 'libreriasocial'); ?></li>
                            <li id="visualizar"><?php esc_html_e('Visualizar', 'libreriasocial'); ?></li>
                        </ul>
                    </label>
                    <input class="form__file" id="upload-files" name="imagenhistoria" type="file" accept="image/*" multiple>
                </div>
                <div id="files-list-container" class="form__files-container"></div>
            </div>

            <div class="col-md-6 campos-historias">
                <!-- <div class="input-group mb-4">
                    <label for="historia-titulo"><?php esc_html_e('Título de la historia', 'libreriasocial'); ?> <span>*</span></label>
                    <input type="text" name="historia-titulo" id="nombre" placeholder="<?php esc_attr_e('Introduzca el título', 'libreriasocial'); ?>" required>
                </div> -->

                <div class="input-group mb-4">
                    <label for="historia-descripcion"><?php esc_html_e('Descripción', 'libreriasocial'); ?></label>
                    <textarea name="historia-descripcion" id="desc" placeholder="<?php esc_attr_e('Introduzca la descripción', 'libreriasocial'); ?>" ></textarea>
                </div>
                <input type="hidden" name="oculto" value="historia">
                <div class="d-flex step">  
                <!-- <input type="button" id="envio" name="btn_EnviarCorreo" value="Enviar Ahora">   -->
                    <input type="submit" id="btn-enviar" name="botonpublicarh" value="<?php esc_attr_e('Publicar', 'libreriasocial'); ?>"  class="boton-publicar">
                </div>
            </div>
        </div>
    </form>
</div>
