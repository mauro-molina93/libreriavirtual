<?php
ob_start();
    if (isset($_POST['botonpublicarh']) && isset($_FILES['imagenhistoria'])) {
        // Procesar datos del formulario
        if (isset($_POST['historia-titulo']) && isset($_POST['historia-descripcion'])) {
            $titulo = sanitize_text_field($_POST['historia-titulo']);
            $descripcion = sanitize_textarea_field($_POST['historia-descripcion']);

            // Subir imagen
            $imagen_id = subir_imagen('imagenhistoria');

            // Crear la entrada de WordPress
            $nueva_entrada = array(
                'post_title'    => $titulo,
                'post_content'  => $descripcion,
                'post_status'   => 'publish',
                'post_type'     => 'post',
                'post_author'   => get_current_user_id(),
                'meta_input'    => array(
                    '_thumbnail_id' => $imagen_id
                )
            );

            // Insertar la entrada
            $entrada_id = wp_insert_post($nueva_entrada);

            // Redireccionar después de enviar el formulario si es necesario
            // Esto evita que el formulario se envíe nuevamente al recargar la página
            wp_redirect(home_url('/'));
           //
           
        }
    }

    // Función para subir la imagen y obtener su ID adjunta
    function subir_imagen($input_name) {
        if (isset($_FILES[$input_name])) {
            $uploadedfile = $_FILES[$input_name];

            $upload_overrides = array('test_form' => false);
            $movefile = wp_handle_upload($uploadedfile, $upload_overrides);

            if ($movefile && !isset($movefile['error'])) {
                $file = $movefile['file'];
                $url = $movefile['url'];
                $type = $movefile['type'];

                $attachment = array(
                    'post_mime_type' => $type,
                    'post_title' => sanitize_file_name($file),
                    'post_content' => '',
                    'post_status' => 'inherit'
                );

                $attach_id = wp_insert_attachment($attachment, $file);
                require_once(ABSPATH . 'wp-admin/includes/image.php');
                $attach_data = wp_generate_attachment_metadata($attach_id, $file);
                wp_update_attachment_metadata($attach_id, $attach_data);

                return $attach_id;
            }
        }

        return false;
    }
?>