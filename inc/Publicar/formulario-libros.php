<div class="container mt-5">
    <form method="post" class="formulario-crear-publicacion" enctype="multipart/form-data" id="formulario-libro">
      
        <div class="row" id="pas1">

            <div class="col-md-3">
                <div class="d-flex2 flex-hp">
                    <!-- <div class="creador-historia" id="creador-historia">
                    <button id="eliminar-imagen" class="btn btn-danger" style="display:none;">Eliminar imagen</button>
                        <h4 class="titulo-historia"><?php esc_html_e('Arrastra tu foto aquí', 'libreriasocial'); ?></h4>
                        <label for="filepublic" class="btn btn-outline-primary"><?php esc_html_e('Selecciona del ordenador', 'libreriasocial'); ?></label>
                        <input type="file" name="filepublic" id="filepublic">
                    </div> -->
                    <div id="modal2" class="modal">
                    <div class="modal-content">
                        <span class="close" id="close2">&times;</span>
                        <img id="modal-image2" src="" alt="Modal Image">
                    </div>
                    </div>
                    <div class="form__container form-libro" id="upload-container2">
                    <div id="desaparecer_cargando2">
                        <h4 class="titulo-historia"><?php esc_html_e('Arrastra tu foto aquí', 'libreriasocial'); ?></h4>
                        <label for="filepublic" class="boton-publicar"><?php esc_html_e('Selecciona del ordenador', 'libreriasocial'); ?></label>
                    </div>
                    <div id="overlay2" class="overlay"></div>
                    <label class="dropdown" id="eliminar-h-container2">
                        <div class="dd-button" style="display: flex !important; align-items:center;">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/threepoints.png'; ?>" alt="" width="46px" height="11px">
                        </div>

                        <input type="checkbox" class="dd-input" id="test">

                        <ul class="dd-menu">
                            <li id="eliminar-h2"><?php esc_html_e('Eliminar', 'libreriasocial'); ?></li>
                            <li id="visualizar2"><?php esc_html_e('Visualizar', 'libreriasocial'); ?></li>
                        </ul>
                    </label>
                    <input class="form__file" id="upload-files2" name="filepublic" type="file" accept="image/*" multiple>
                </div>
                <div id="files-list-container2" class="form__files-container"></div>
                   
                    </div>

                <div id="imagenes-galleria">
                    <!-- <div class="feacture image" id="p-i-l"></div> -->
                    <div class="upload__box">
                        <div class="upload__btn-box">
                            <label class="upload__btn">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/add.png' ?>" alt="">
                            <p><?php esc_html_e('Agregar imágenes', 'libreriasocial'); ?></p>
                            <input type="file" multiple="" accept="image/*" data-max_length="20" class="upload__inputfile" name="imagenes[]" >
                            </label>
                        </div>
                        <div class="upload__img-wrap"></div>
                    </div>
                </div>
            </div>

            <div id="paso2-1">
            <div class="col-md-7">
                <div class="input-group">
                    <label for="titulo"><?php esc_html_e('Título del libro o escrito', 'libreriasocial'); ?> <span>*</span></label>
                    <input type="text" autocomplete="off" name="titulo" id="nombre-libro" placeholder="<?php esc_attr_e('Introduzca el título del libro', 'libreriasocial'); ?>" required>
                </div>

                <div class="input-group">
                    <label for="descripcion"><?php esc_html_e('Resumen del libro o escrito', 'libreriasocial'); ?> <span>*</span></label>
                    <textarea name="descripcion" id="desc-libro" placeholder="<?php esc_attr_e('Introduzca el resumen', 'libreriasocial'); ?>" required></textarea>
                </div>
            </div>

            <div class="col-md-5">

                <div class="input-group">
                    <label for="categoria_nombre"><?php esc_html_e('Género literario', 'libreriasocial'); ?> <span>*</span></label>
                    <select name="categoria_nombre" id="categoria_nombre" required>
                        <option value=""><?php esc_html_e('Seleccione GL', 'libreriasocial'); ?></option>
                        <?php
                        $categorias = get_terms(array(
                            'taxonomy' => 'product_cat',
                            'hide_empty' => false,
                        ));

                        foreach ($categorias as $categoria) {
                            echo '<option value="' . esc_attr($categoria->name) . '">' . esc_html($categoria->name) . '</option>';
                        }
                        ?>
                    </select>

                </div>

                <div class="input-group">
                    <label for="tema"><?php esc_html_e('Tema', 'libreriasocial'); ?> <span>*</span></label>
                    <select name="tema" id="tema" required>
                        <option value=""><?php esc_html_e('Seleccione el tema', 'libreriasocial'); ?></option>
                        <?php
                        if (function_exists('acf_get_field_groups')) {
                            $field_groups = acf_get_field_groups();

                            if (!empty($field_groups)) {
                                foreach ($field_groups as $group) {
                                    $fields = acf_get_fields($group['key']);
                                    if (!empty($fields)) {
                                        foreach ($fields as $field) {
                                            if ($field['name'] === 'tema') {
                                                if (isset($field['choices']) && !empty($field['choices'])) {
                                                    foreach ($field['choices'] as $value => $label) {
                                                        echo '<option value="' . esc_attr($value) . '">' . esc_html($label) . '</option>';
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="input-group">
                    <label for="idioma"><?php esc_html_e('Idioma', 'libreriasocial'); ?> <span>*</span></label>
                    <select name="idioma" id="idioma" required>
                        <option value=""><?php esc_html_e('Seleccione el idioma', 'libreriasocial'); ?></option>
                        <?php
                        if (function_exists('acf_get_field_groups')) {
                            $field_groups = acf_get_field_groups();

                            if (!empty($field_groups)) {
                                foreach ($field_groups as $group) {
                                    $fields = acf_get_fields($group['key']);
                                    if (!empty($fields)) {
                                        foreach ($fields as $field) {
                                            if ($field['name'] === 'idioma') {
                                                if (isset($field['choices']) && !empty($field['choices'])) {
                                                    foreach ($field['choices'] as $value => $label) {
                                                        echo '<option value="' . esc_attr($value) . '">' . esc_html($label) . '</option>';
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="input-group">
                    <?php 
                        $user_id = get_current_user();
                        $user_email = $current_user->user_email;
                    ?>

                    <label for="correo_e"><?php esc_html_e('Correo Electrónico', 'libreriasocial'); ?> <span>*</span></label>
                    <input type="text" name="correo_e" id="correo" placeholder="<?php esc_attr_e('Introduzca su correo', 'libreriasocial'); ?>" value="<?php echo $user_email; ?>" style="color:#000" required>
                </div>


                <div class="d-flex step justify-content-between">
                    <!-- <a class="boton-publicar" id="prev-publicar" href=""><?php esc_html_e('Anterior', 'libreriasocial'); ?></a> -->
                    <a class="boton-publicar" href="/"><?php esc_html_e('Cancelar', 'libreriasocial'); ?></a>   
                    <a class="boton-publicar" id="next-publicar"><?php esc_html_e('Siguiente', 'libreriasocial'); ?></a>                    
                </div>
            </div>
            </div>

            <div class="paso3" id="paso3">
               <div class="col-md-6">

               <div class="input-group">
                        <label for="catp"><?php esc_html_e('Categoría de pago', 'libreriasocial'); ?> <span>*</span></label>
                        <select name="catp" id="catp" required>
                        <option value=""> <?php esc_html_e('Seleccione la categoría de pago'); ?> </option>
                        <?php
                        if (function_exists('acf_get_field_groups')) {
                            $field_groups = acf_get_field_groups();

                            if (!empty($field_groups)) {
                                foreach ($field_groups as $group) {
                                    $fields = acf_get_fields($group['key']);
                                    if (!empty($fields)) {
                                        foreach ($fields as $field) {
                                            if ($field['name'] === 'categoria_pago') {
                                                if (isset($field['choices']) && !empty($field['choices'])) {
                                                    foreach ($field['choices'] as $value => $label) {
                                                        echo '<option value="' . esc_attr($value) . '">' . esc_html($label) . '</option>';
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </select>
                </div>

                
                <script>
                document.addEventListener('DOMContentLoaded', function() {
                    var catpSelect = document.getElementById('catp');
                    var precioInput = document.getElementById('precio');

                    function togglePrecioInput() {
                        var selectedValue = catpSelect.value;
                        if (selectedValue === 'libredepago') {
                            precioInput.disabled = true;
                            precioInput.value = parseFloat('0.00').toFixed(2);
                            precioInput.classList.add('btn', 'btn-secondary', 'disabled');
                        } else {
                            precioInput.disabled = false;
                            precioInput.value = '';
                            precioInput.classList.remove('btn', 'btn-secondary', 'disabled');
                        }
                    }

                    togglePrecioInput();
                    catpSelect.addEventListener('change', function(event) {
                        togglePrecioInput();
                    });
                });
                </script>



                <div class="input-group">
                    <label for="precio"><?php esc_html_e('Precio'); ?> <span>*</span></label>
                    <input type="text" autocomplete="off" name="precio" id="precio" placeholder="<?php esc_attr_e('Introduzca el precio del producto', 'libreriasocial'); ?> " required>
                    
                    <div class="contenedor-checkbox">
                        <input type="checkbox" id="toggle-recomendados" name="checkrecomendados">
                        <label for="toggle-recomendados" class="mt-3">  <?php esc_html_e('Añadir a recomendados', 'libreriasocial'); ?> </label>
                    </div>

                    <div class="agregar-recomendados" style="display: none;">
                        
                        <span class="mensajerecomendados" id="mensajerecomendado"><?php esc_html_e('Se descontará el 20% al precio establecido', 'libreriasocial'); ?></span>
                    </div>
                </div>
                <script>
                    document.addEventListener('DOMContentLoaded', function() {
                        var precioInput = document.getElementById('precio');

                        precioInput.addEventListener('input', function(event) {
                            var valor = event.target.value.trim();
                       
                            var numeros = valor.replace(/[^\d,.]/g, ''); 
                           
                            numeros = numeros.replace(/,/g, '.');
                           
                            if (numeros.length > 7) {
                                numeros = numeros.substr(0, 7);
                            }

                            event.target.value = numeros;
                        });
                    });
                </script>


                <div class="mt-5 precios-libro-publicar" id="calculorecomendados">
                    <div><strong> <?php esc_html_e('Precio del producto', 'libreriasocial'); ?>:</strong> <span id="precio-producto"> </span></div>
                    <div><strong> <?php esc_html_e('Descuento', 'libreriasocial'); ?>:</strong> <span id="descuento-precioproducto"> </span></div>
                    <div class="total_recibir"><?php esc_html_e('Total a recibir', 'libreriasocial'); ?>: <strong class="precio-total" id="precio-total"> </strong></div>
                </div>
                

                <script>
                    document.addEventListener('DOMContentLoaded', function () {
                      
                    });
                </script>
               </div>

               <div class="col-md-6">

               <div class="input-group share-container">
                    <label for="share"><?php esc_html_e('Compartir para', 'libreriasocial'); ?></label>

                    <div class="contenedor-radio">
                        <input type="radio" name="share[]" id="solo" value="solo" checked>
                        <label for="solo" class="form__radio-label">
                            <span class="form__radio-button"></span>
                            <?php esc_html_e('Solo yo', 'libreriasocial'); ?>
                        </label>
                    </div>

                    <div class="contenedor-radio">
                        <input type="radio" name="share[]" id="todos" value="todos">
                        <label for="todos" class="form__radio-label">
                            <span class="form__radio-button"></span>
                            <?php esc_html_e('Todos', 'libreriasocial'); ?>
                        </label>
                    </div>

                    <div class="contenedor-radio">
                        <input type="radio" name="share[]" id="amigos" value="amigos">
                        <label for="amigos" class="form__radio-label">
                            <span class="form__radio-button"></span>
                            <?php esc_html_e('Amigos', 'libreriasocial'); ?>
                        </label>
                    </div>


                    <!-- <div class="contenedor-radio">
                        <input type="radio"  id="amigos" name="share[]" value="amigos">
                        <label for="amigos" class="form__radio-labe">
                            <span class="form__radio-button"></span>
                            <?php esc_html_e('Amigos', 'libreriasocial'); ?>    
                        </label>
                    </div> -->
    
                </div>

                <!-- <div class="contenedor-checkbox">
                        <input type="checkbox" id="toggle-recomendados" name="checkrecomendados">
                        <label for="toggle-recomendados" class="mt-3">  <?php esc_html_e('Añadir a recomendados', 'libreriasocial'); ?> </label>
                    </div> -->
                <div class="contenedor-amistad" id="mostrarAmistad">
                        <div class="contenedor-checkbox" id="amigos_dAmigos">
                            <input type="checkbox" name="amigosAmigos" id="amigos_de_amigos" value="amigos_de_amigos">
                            <label for="amigos_de_amigos" class="form__radio-label">
                               
                                <?php esc_html_e('Amigos de mis amigos', 'libreriasocial'); ?>
                            </label>
                        </div>

                        <!-- <div class="contenedor-radio">
                            <input type="radio" name="share[]" id="amigosAmigos" value="amigosAmigos">
                            <label for="amigosAmigos" class="form__radio-label">
                                <span class="form__radio-button"></span>
                                <?php esc_html_e('Amigos de mis amigos', 'libreriasocial'); ?>
                            </label>
                        </div> -->
                </div>

               
                <script>
                    document.addEventListener('DOMContentLoaded', function() {
                        var amigosRadioButton = document.getElementById('amigos');
                        var amigosDiv = document.getElementById('amigos_dAmigos');

                        function toggleAmigosDiv() {
                            if (amigosRadioButton.checked) {
                                amigosDiv.style.display = 'block';
                                console.log("checkeado"); 
                            } else {
                                amigosDiv.style.display = 'none';
                                console.log("no checkeado"); 
                            }
                        }

                        toggleAmigosDiv();

                        var radioButtons = document.querySelectorAll('input[type="radio"]');
                        for (var i = 0; i < radioButtons.length; i++) {
                            radioButtons[i].addEventListener('change', function(event) {
                                if (event.target !== amigosRadioButton) {
                                    amigosDiv.style.display = 'none';
                                }
                                toggleAmigosDiv();
                                console.log("cambie de evento");
                            });
                        }
                    });
                </script>

                <div class="input-group d-flex mayorDeEdad">
                    <div class="contenedor-checkbox">
                        <input type="checkbox" name="mayorEdad" id="mayorEdad">
                        <label for="mayorEdad" class="mayorEdad"><?php esc_attr_e('Apto para +18 años de edad', 'libreriasocial'); ?> </label>
                    </div>
                </div>

                <div class="input-group">
                    <label for="fileLibro" class="adjunto">
                        <?php esc_html_e('Adjuntar Archivo', 'libreriasocial'); ?>
                        <input type="file" name="fileLibro" id="fileLibro" class="fileLibro" placeholder="<?php esc_attr_e('Adjuntar archivo', 'libreriasocial'); ?>" accept=".pdf,.epub">
                        <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/adjunto.png' ?>">
                    </label>
                </div>
                <div id="file-name-preview"></div> 
                <a href="#" id="clear-file"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                <input type="hidden" name="oculto" value="libro">
                <input type="hidden" name="iva" value="<?php echo esc_attr(get_option('iva_value')); ?>">
                <input type="hidden" name="descuento" value="<?php echo esc_attr(get_option('descuento')); ?>">
                <input type="hidden" name="descuento_recomendados" value="<?php echo esc_attr(get_option('descuento_recomendados')); ?>">

                <div class="d-flex step">    
                    <a href="#" class="boton-publicar" id="back_step2"><?php esc_html_e('Anterior', 'libreriasocial'); ?></a>
                    <input type="submit" name="botonpublicar" value="<?php esc_attr_e('Publicar', 'libreriasocial'); ?>"  class="boton-publicar" id="publicar-libro">
                </div>
               </div>
            </div>
        </div>
    </form>
</div>

