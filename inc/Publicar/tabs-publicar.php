<div id="exTab1" class="container mt-5">	
<ul  class="nav nav-pills tabs tbs-publicar">
			<li class="active show">
                <a class="active show" href="#1a" data-toggle="tab">Historia</a>
			</li>
			<li>
                <a href="#2a" id="tab-publicar" data-toggle="tab">Publicación</a>
			</li>
		</ul>

			<div class="tab-content clearfix">
			  <div class="tab-pane active" id="1a">
                <?php require 'historias-formulario.php'; ?>
			  </div>

			  <div class="tab-pane form-tbs-libro" id="2a">
                <?php require 'formulario-libros.php'; ?>
			  </div>

			</div>
  </div>