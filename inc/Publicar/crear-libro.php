<?php 
ob_start();
if (isset($_POST['oculto']) && $_POST['oculto'] === 'libro' && $_SERVER['REQUEST_METHOD'] === 'POST' ) {
   $precio="0";
    $titulo = isset($_POST['titulo']) ? $_POST['titulo'] : '';
    $precio = isset($_POST['precio']) ? $_POST['precio'] : '0';
    $descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : '';
    $categoria_nombre = isset($_POST['categoria_nombre']) ? $_POST['categoria_nombre'] : ''; 
    $categoria_pago = isset($_POST['catp']) ? $_POST['catp'] : '';
    $correo_e = isset($_POST['correo_e']) ? $_POST['correo_e'] : '';
    $tema = isset($_POST['tema']) ? $_POST['tema'] : '';
    $idioma = isset($_POST['idioma']) ? $_POST['idioma'] : '';
    $compartir = isset($_POST['share']) ? $_POST['share'] : array();
    $amigosAmigos = isset($_POST['amigosAmigos']) ? $_POST['amigosAmigos'] : '';
    $mayorEdad = isset($_POST['mayorEdad']) ? $_POST['mayorEdad'] : '';
    $recomendados = isset($_POST['recomendados']) ? $_POST['recomendados'] : '';
    
    $check_recomendados = isset($_POST['checkrecomendados']) ? $_POST['checkrecomendados'] : '';
    
    $categoria = get_term_by('name', $categoria_nombre, 'product_cat');

    //Procesamiento del precio del producto 
    $iva = isset($_POST['iva']) ? floatval(sanitize_text_field($_POST['iva'])) : 0;
    $descuento = isset($_POST['descuento']) ? floatval(sanitize_text_field($_POST['descuento'])) : 0;
    $descuento_recomendados = isset($_POST['descuento_recomendados']) ? floatval(sanitize_text_field($_POST['descuento_recomendados'])) : 0;


    if (!$categoria) {
        $categoria_id = wp_insert_term($categoria_nombre, 'product_cat');
        if (is_wp_error($categoria_id)) {
            exit;
        }
        $categoria_id = $categoria_id['term_id'];
    } else {
        $categoria_id = $categoria->term_id;
    }

    $producto = array(
        'post_title' => $titulo,
        'post_content' => $descripcion,
        'post_status' => 'publish',
        'post_type' => 'product'
    );

    
    $producto_id = wp_insert_post($producto);

    if (!is_wp_error($producto_id)) {
        wp_set_object_terms($producto_id, array($categoria_id), 'product_cat');
    }

    $precio_con_iva = 0;
    $precio_descuento_sin_recomendar = 0;
    $precio_descuento_recomendado = 0;

    if ($precio) {
        $precio_con_iva = $precio * ($iva/100);
        $precio_descuento_sin_recomendar = ($precio - $precio_con_iva) * ($descuento/100);
        $precio_descuento_recomendado = ($precio - $precio_con_iva) * ($descuento_recomendados/100);

        update_post_meta($producto_id, 'precio_descuento_sin_recomendar', $precio_descuento_sin_recomendar);
        update_post_meta($producto_id, 'precio_descuento_recomendado', $precio_descuento_recomendado);
        update_post_meta($producto_id, 'precioconiva', $precio_con_iva);

        update_post_meta($producto_id, 'iva', $iva);
        update_post_meta($producto_id, 'descuento', $descuento);
        update_post_meta($producto_id, 'descuento_recomendados', $descuento_recomendados);

        // Ejemplo de uso en cálculo del precio final con descuento

        if ($check_recomendados === 'on'){
            $precioconiva = $precio * ($iva/100);
            $precio_con_descuento = $precio - (($precio - $precioconiva) * ($descuento_recomendados / 100)) - $precioconiva;
        } else {
            $precioconiva = $precio * ($iva/100);
            $precio_con_descuento = $precio - (($precio - $precioconiva) * ($descuento / 100)) - $precioconiva;
        }

        $precio_con_descuento = number_format($precio_con_descuento, 2);
        update_post_meta($producto_id, 'precio_con_descuento', $precio_con_descuento);

        update_post_meta($producto_id, '_regular_price', $precio);
        update_post_meta($producto_id, '_price', $precio);
    }else{
        update_post_meta($producto_id, '_regular_price', 0);
        update_post_meta($producto_id, '_price', 0);
    }

    $imagen = isset($_FILES['filepublic']) ? $_FILES['filepublic'] : '';
    $imagen_nombre = $imagen['name'];
    $imagen_temp = $imagen['tmp_name'];

    $upload_dir = wp_upload_dir();
    $imagen_nombre_limpio = str_replace(' ', '_', $imagen_nombre); 
    $imagen_ruta = $upload_dir['path'] . '/' . $imagen_nombre_limpio;

    if (move_uploaded_file($imagen_temp, $imagen_ruta)) {

        require_once(ABSPATH . 'wp-admin/includes/image.php');
        require_once(ABSPATH . 'wp-admin/includes/file.php');
        require_once(ABSPATH . 'wp-admin/includes/media.php');

        $adjunto_id = wp_insert_attachment(
            array(
                'post_mime_type' => 'image/jpeg', 
                'post_title' => $imagen_nombre_limpio, 
                'post_content' => '',
                'post_status' => 'inherit'
            ),
            $imagen_ruta,
            $producto_id
        );

        set_post_thumbnail($producto_id, $adjunto_id);
    }

    update_field('correo_e', $correo_e, $producto_id);
    update_post_meta($producto_id, 'categoria_pago', $categoria_pago);
    update_post_meta($producto_id, 'idioma', $idioma);
    update_post_meta($producto_id, 'tema', $tema);

    update_post_meta($producto_id, 'compartir', $compartir);
    if ($recomendados === 'si') {
        // Guardar la opción "Sí"
        insertar_solicitud_recomendados($titulo,'libro');
        update_post_meta($producto_id, 'recomendados', 'si');
        
    } elseif ($recomendados === 'no') {
        // Guardar la opción "No"
        update_post_meta($producto_id, 'recomendados', 'no');
    }
    update_post_meta($producto_id, 'mayorEdad', $mayorEdad);
    update_post_meta($producto_id, 'checkrecomendados', $check_recomendados);
    update_post_meta($producto_id, 'amigosAmigos', $amigosAmigos);

    update_post_meta($producto_id, '_virtual', 'yes');
    update_post_meta($producto_id, '_downloadable', 'yes');

    if (!empty($_FILES['fileLibro']['tmp_name'])) {
        $fileLibro = isset($_FILES['fileLibro']) ? $_FILES['fileLibro'] : '';
        $fileLibro_nombre = $fileLibro['name'];
        $fileLibro_nombre = preg_replace('/\s/', '-', $fileLibro_nombre);
        $fileLibro_temp = $fileLibro['tmp_name'];

    
        $upload_dir = wp_upload_dir();
       
        $fileLibro_ruta = $upload_dir['path'] . '/' . $fileLibro_nombre;


        $temp_file_path = wp_upload_dir()['basedir']  . '/woocommerce_uploads/'.date("Y").'/'.date("m").'/' . $fileLibro_nombre;
        $upload_dir = wp_upload_dir();
        $year_month_folder = $upload_dir['basedir'] . '/woocommerce_uploads/' . date("Y") . '/' . date("m") . '/';

        if (!file_exists($year_month_folder)) {
            wp_mkdir_p($year_month_folder);
        }
        
        if (move_uploaded_file($fileLibro_temp, $temp_file_path)) {
            $file_path =md5(wp_upload_dir()['baseurl'] . '/woocommerce_uploads/'.date("Y").'/'.date("m").'/' . $fileLibro_nombre);
            $file_array = array(
                'id' => $file_path,
                'name' => $fileLibro_nombre,
                'file' => wp_upload_dir()['baseurl'] . '/woocommerce_uploads/'.date("Y").'/'.date("m").'/' . $fileLibro_nombre
            );

            $_file_paths[  $file_path  ] = $file_array;
            update_post_meta($producto_id, '_downloadable_files', $_file_paths);

            
        }
    }


// Galería de imágenes 
if (!empty($_FILES['imagenes']['tmp_name'])) {
    $imagenes = isset($_FILES['imagenes']) ? $_FILES['imagenes'] : '';
    $gallery_images = array(); 

    foreach ($imagenes['tmp_name'] as $key => $tmp_name) {
        $imagen_nombre = $imagenes['name'][$key];
        $imagen_temp = $tmp_name;

        $upload_dir = wp_upload_dir();
        $imagen_ruta = $upload_dir['path'] . '/' . $imagen_nombre;

        if (move_uploaded_file($imagen_temp, $imagen_ruta)) {
            require_once(ABSPATH . 'wp-admin/includes/image.php');
            require_once(ABSPATH . 'wp-admin/includes/file.php');
            require_once(ABSPATH . 'wp-admin/includes/media.php');

            $adjunto_id = wp_insert_attachment(
                array(
                    'post_mime_type' => $imagenes['type'][$key],
                    'post_title' => $imagen_nombre,
                    'post_content' => '',
                    'post_status' => 'inherit',
                    'post_parent' => $producto_id
                ),
                $imagen_ruta,
                $producto_id
            );

            if (!is_wp_error($adjunto_id)) {
                $gallery_images[] = $adjunto_id;
            }
        }
    }

    $existing_gallery = get_post_meta($producto_id, '_product_image_gallery', true);

    if (!empty($existing_gallery)) {
        $existing_gallery_ids = explode(',', $existing_gallery);
        $gallery_images = array_merge($existing_gallery_ids, $gallery_images);
    }

    if (!empty($gallery_images)) {
        $updated_gallery = implode(',', $gallery_images);
        update_post_meta($producto_id, '_product_image_gallery', $updated_gallery);
    }
}


   $product_permalink = get_permalink($producto_id);

   if (!empty($product_permalink)) {
       wp_redirect($product_permalink);
       exit;
   } else {
       wp_redirect(home_url());
       exit;
   }
    
?><pre><?//var_dump($_POST); exit(); ?></pre><?php
 exit;
}


if (isset($_POST['oculto']) && $_POST['oculto'] === 'historia' && $_SERVER['REQUEST_METHOD'] === 'POST') {

    // Procesar datos del formulario
    if (isset($_POST['historia-descripcion'])) {
        // $titulo = sanitize_text_field($_POST['historia-titulo']);
        $descripcion = sanitize_textarea_field($_POST['historia-descripcion']);

        $usuario_actual = wp_get_current_user();
        $nombre_usuario = $usuario_actual->display_name;

        // Generar el título dinámico
        $titulo = 'Historia de: ' . $nombre_usuario . ', en el día: ' . date('Y-m-d');

        // Subir imagen
        $imagen_id = subir_imagen('imagenhistoria');

        $categoria_id = get_cat_ID('historia');
        // Crear la entrada de WordPress
        $nueva_entrada = array(
            'post_title'    => $titulo,
            'post_content'  => $descripcion,
            'post_status'   => 'publish',
            'post_type'     => 'post',
            'post_category' => array($categoria_id), 
            'post_author'   => get_current_user_id(),
            'meta_input'    => array(
                '_thumbnail_id' => $imagen_id
            )
        );

        // Insertar la entrada
        $entrada_id = wp_insert_post($nueva_entrada);

        // Redireccionar después de enviar el formulario si es necesario
        // Esto evita que el formulario se envíe nuevamente al recargar la página
        wp_redirect(home_url('/'));
       //
     
    }
}

// Función para subir la imagen y obtener su ID adjunta
function subir_imagen($input_name) {
    if (isset($_FILES[$input_name])) {
        $uploadedfile = $_FILES[$input_name];

        $upload_overrides = array('test_form' => false);
        $movefile = wp_handle_upload($uploadedfile, $upload_overrides);

        if ($movefile && !isset($movefile['error'])) {
            $file = $movefile['file'];
            $url = $movefile['url'];
            $type = $movefile['type'];

            $attachment = array(
                'post_mime_type' => $type,
                'post_title' => sanitize_file_name($file),
                'post_content' => '',
                'post_status' => 'inherit'
            );

            $attach_id = wp_insert_attachment($attachment, $file);
            require_once(ABSPATH . 'wp-admin/includes/image.php');
            $attach_data = wp_generate_attachment_metadata($attach_id, $file);
            wp_update_attachment_metadata($attach_id, $attach_data);

            return $attach_id;
        }
    }

    return false;
}

if (isset($_POST['oculto']) && $_POST['oculto'] === 'editarperfil' && $_SERVER['REQUEST_METHOD'] === 'POST') {
  
    $user_id = get_current_user_id();
    $nombre = sanitize_text_field($_POST['nombre']);
    $cuenta_bancaria = sanitize_text_field($_POST['cuenta_bancaria']);
    $email = sanitize_email($_POST['email']);
    $desc_perfil = sanitize_textarea_field($_POST['desc_perfil']);

    update_user_meta($user_id, 'cuenta_bancaria', $cuenta_bancaria);
    update_user_meta($user_id, 'desc_perfil', $desc_perfil);

    $user_data = array(
        'ID' => $user_id,
        'user_email' => $email,
        'first_name' => $nombre
    );
    wp_update_user($user_data);

    
    $day = isset($_POST['day']) ? sanitize_text_field($_POST['day']) : '';
    update_user_meta($user_id, 'dia_nacimiento', $day);

    $month = isset($_POST['month']) ? sanitize_text_field($_POST['month']) : '';
    update_user_meta($user_id, 'mes_nacimiento', $month);

    $year = isset($_POST['year']) ? sanitize_text_field($_POST['year']) : '';
    update_user_meta($user_id, 'anio_nacimiento', $year);

    $sharefriends = isset($_POST['sharefriendss']) && $_POST['sharefriendss'] === 'on' ? 'on' : 'off';
    update_user_meta($user_id, 'sharefriends', $sharefriends);

    // var_dump($sharefriends);
    // exit();

    if (isset($_FILES['avatar'])) {
        $file = $_FILES['avatar'];
        
        // Directorio donde se guardarán las imágenes
        $upload_dir = wp_upload_dir();
        $avatar_dir = $upload_dir['basedir'] . '/avatars/';
        
        // Crear la carpeta 'avatar' si no existe
        if (!file_exists($avatar_dir)) {
            mkdir($avatar_dir);
        }
        
        // Obtener el ID de usuario actual
        $user_id = get_current_user_id();
        
        // Crear una carpeta con el nombre del ID de usuario si no existe
        $user_dir = $avatar_dir . $user_id . '/';
        if (!file_exists($user_dir)) {
            mkdir($user_dir);
        }
        
        // Subir la imagen al servidor
        $uploaded_file = wp_handle_upload($file, array('test_form' => false));
        
        if (!empty($uploaded_file['file'])) {
            // Obtener la ruta de la imagen subida
            $image_path = $uploaded_file['file'];
            
            // Cargar la imagen para redimensionarla
            $image = wp_get_image_editor($image_path);
            
            if (!is_wp_error($image)) {
                // Redimensionar la imagen a 150x150 y guardarla como 'null.jpg'
                $image->resize(150, 150, true);
                $image->save($user_dir . time().'-bpfull.jpg');
                
                // Redimensionar la imagen a 50x50 y guardarla como 'thumbnail.jpg'
                $image->resize(50, 50, true);
                $image->save($user_dir . time().'-bpthumb.jpg');
                
                // Actualizar la URL de la imagen del perfil en la tabla wp_usermeta
                update_user_meta($user_id, 'bp_avatar', $user_dir . 'null.jpg');
                
                echo 'La imagen se ha subido y redimensionado correctamente.';
            } else {
                echo 'Error al redimensionar la imagen.';
            }
        } else {
            echo 'Error al subir la imagen.';
        }
    }

    $prefix = isset($_POST['prefix']) ? sanitize_text_field($_POST['prefix']) : '';
    update_user_meta($user_id, 'codigo_pais', $prefix);

    $tel = isset($_POST['tel']) ? sanitize_text_field($_POST['tel']) : '';
    update_user_meta($user_id, 'telefono', $tel);

    
   

    // $recomendado = isset($_POST['recomendados']) ? sanitize_text_field($_POST['recomendados']) : 'no';
    
    // if ($recomendado === 'no') {
    //     // Guardar la opción "No"
    //     update_user_meta($user_id, 'recomendado', $recomendado);   
        
    // } elseif ($recomendado === 'si') {
    //     // Guardar la opción "Sí"
    //     insertar_solicitud_recomendados($nombre,'autor');
    //     update_user_meta($user_id, 'recomendado', $recomendado);        
    // }



    wp_redirect('/configuracion');

    // var_dump($_POST);
    // exit;
}


if (isset($_POST['oculto']) && $_POST['oculto'] === 'registro' && $_SERVER['REQUEST_METHOD'] === 'POST') { 
    $publicacion_value = isset($_POST['publicacion_value']) ? intval($_POST['publicacion_value']) : 0;

    $nombre = $_POST['nombre'];
    // $apellidos = $_POST['apellidos'];
    $username = $_POST['username'];
    $email = $_POST['email'];
    $contrasena = $_POST['contrasena'];
    $documento = $_POST['documento'];
    $direccion = $_POST['direccion'];
    $cuenta_bancaria = $_POST['cuenta_bancaria'];
    $telefono = $_POST['telefono'];
    $mayor_edad = isset($_POST['mayor_edad']) ? 1 : 0; 
 
    $userdata = array(
        'user_login' => $username,
        'user_pass' => $contrasena,
        'user_email' => $email,
        'first_name' => $nombre,
        // 'last_name' => $apellidos,
    );
    $user_id = wp_insert_user($userdata);

    
    if (!is_wp_error($user_id)) {
        update_user_meta($user_id, 'documento', $documento);
        update_user_meta($user_id, 'direccion', $direccion);
        update_user_meta($user_id, 'cuenta_bancaria', $cuenta_bancaria);
        update_user_meta($user_id, 'telefono', $telefono);
        update_user_meta($user_id, 'mayor_edad', $mayor_edad);

        echo '<div id="mensaje">Registro exitoso</div>';
    } else {
        echo '<div id="mensaje">Error al registrar usuario</div>';
    }

    if ($publicacion_value === 1) {
        $user = new WP_User($user_id);
        // $user->add_role('shop_manager');
        update_user_meta($user_id, 'account_status', 'pending');

        $admin_email = get_option('admin_email');
        $additional_email = 'info@siescribo.com';
        $user_info = get_userdata($user_id);

        if ($user_info) {
            $user_name = $user_info->display_name;
        } 

        $subject = 'Nueva solicitud de escritor';
        $message = 'Se ha registrado un nuevo usuario de nombre ' . $user_name . '. Por favor, revisa la cuenta y actívala si es necesario.';
        $message .= "\n\n";
        $message .= 'Se adjunta Contrato actual con los términos y condiciones';
        $message .= "\n";
        $message .= 'Puedes revisar los usuarios pendientes en el siguiente enlace:';
        $message .= "\n";
        $message .= admin_url('admin.php?page=listado_autores');

        $subject_user = 'Bienvenido a nuestro siescribo.com';
        $message_user = 'Hola ' . $user_name . ', Su cuenta ha sido registrada con éxito, la misma está en proceso de validación. En breve recibirá un correo con las indicaciones. Se adjunta Contrato actual con los términos y condiciones';
        
        $file_url = enviar_terminos();
        $file_name = 'Términos y condiciones.pdf';
        $tmp_file_path = download_url($file_url);
      
        //renombro el fichero pq se envia con la extensión .temp
        rename($tmp_file_path, dirname($tmp_file_path) . '/' . $file_name);
        $attachments = array(dirname($tmp_file_path) . '/' . $file_name);        

        wp_mail($admin_email, $subject, $message,'', $attachments);
        wp_mail($additional_email, $subject, $message,'', $attachments);

        wp_mail($user_info->user_email, $subject_user, $message_user,'', $attachments);
    } else {
        $user = new WP_User($user_id);
        $user->add_role('suscriber1');
        $user_info = get_userdata($user_id);
        if ($user_info) {
            $user_name = $user_info->display_name;
        } 

        $subject = 'Bienvenido a nuestro sitio web';
        $message = 'Hola ' . $user_name . ', su cuenta ha sido registrada con éxito. Puede loguearse con su usuario y contraseña.';
        
        wp_mail($user_info->user_email, $subject, $message);

    }
    

    // var_dump($publicacion_value);
    // exit();
    $login_url = wp_login_url();
    wp_redirect($login_url);
    exit;
}


// // Obtener el ID del remitente (usuario actual)
// $sender_id = get_current_user_id();

// function enviar_mensaje_privado($sender_id, $recipient_id, $message_content) {
//     global $wpdb;
//     $messages_table = $wpdb->prefix . 'bm_message_messages';
//     $date_sent = current_time('mysql', 1);

//     // Insertar el mensaje en la tabla de mensajes
//     $result = $wpdb->insert(
//         $messages_table,
//         array(
//             'thread_id' => 1,
//             'sender_id' => $sender_id,
//             'recipient_id' => $recipient_id,
//             'message' => $message_content,
//             'date_sent' => $date_sent
//         ),
//         array('%d', '%d', '%d', '%s', '%s')
//     );

//     // Verificar si se insertó correctamente
//     if ($result !== false) {
//         return true; // Mensaje enviado correctamente
//     } else {
//         // Si hay un error, podemos mostrar el error SQL para depurar
//         $wpdb->print_error();
//         echo "Hubo un error al insertar los datos en la base de datos.";
//         return false; // Error al enviar el mensaje
//     }
// }


// // Verificar si se envió el formulario
// if (isset($_POST['oculto']) && $_POST['oculto'] === 'smsprivado' && $_SERVER["REQUEST_METHOD"] == "POST") {
//     // Verificar si se han recibido los datos del formulario
//     if (isset($_POST['recipient_id']) && isset($_POST['message_content'])) {
//         // Obtener los datos del formulario
//         $recipient_id = intval($_POST['recipient_id']);
//         $message_content = sanitize_text_field($_POST['message_content']);

//         // Asegurarse de que el ID del destinatario no esté vacío
//         if (!empty($recipient_id)) {
//             // Asegurarse de que el contenido del mensaje no esté vacío
//             if (!empty($message_content)) {
//                 // Enviar el mensaje privado utilizando la función personalizada
//                 $mensaje_enviado = enviar_mensaje_privado($sender_id, $recipient_id, $message_content);

//                 if ($mensaje_enviado) {
//                     // El mensaje se ha enviado correctamente
//                     echo "Mensaje enviado correctamente.";
//                     exit();
//                 } else {
//                     // Hubo un error al enviar el mensaje
//                     echo "Hubo un error al enviar el mensaje.";
//                     echo $sender_id . "<br>";
//                     echo $recipient_id . "<br>";
//                     echo $message_content . "<br>";
//                     exit();
//                 }
//             } else {
//                 // El contenido del mensaje está vacío
//                 echo "El contenido del mensaje está vacío.";
//                 exit();
//             }
//         } else {
//             // El ID del destinatario está vacío
//             echo "El ID del destinatario está vacío.";
//             exit();
//         }
//     } else {
//         // No se recibieron todos los datos del formulario
//         echo "No se recibieron todos los datos del formulario.";
//         exit();
//     }
// }



//send message story to private chat
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['oculto']) && $_POST['oculto'] === 'smsprivado') {
    $id_story = $_POST['post_id'];
    $message = $_POST["message_content"];
    send_message_story_chat($id_story,$message);
}



// Verifica si se ha enviado el formulario
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['accept_friend'])) {
    $bp_notify_id = $_POST['bp_notify_id'];
    $initiator_user_id =  (int) $_POST['initiator_user_id'];
    $friend_user_id = get_current_user_id(); 
    
    // Actualiza la fila en la tabla wp_friends
    global $wpdb;
    $table_name = $wpdb->prefix . 'bp_friends';
    $wpdb->update(
        $table_name,
        array('is_confirmed' => 1), 
        array('initiator_user_id' => $initiator_user_id, 'friend_user_id' => $friend_user_id),
        array('%d'), 
        array('%d', '%d') 
    );

  
    // var_dump($initiator_user_id);
    // var_dump($friend_user_id);
    // var_dump($bp_notify_id);
    //  exit();
}


if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['delete_friend'])) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'bp_notifications';
    $bp_notify_id = $_POST['delete_bp_notify_id'];

    // Eliminar la notificación de la tabla de notificaciones de BuddyPress
    $wpdb->delete(
        $table_name,
        array('id' => $bp_notify_id),
        array('%d')
    );

    // echo "Notificación eliminada con éxito";
    // exit();
}


// Editar libro 

if (isset($_POST['oculto']) && $_POST['oculto'] === 'editarlibro' && $_SERVER['REQUEST_METHOD'] === 'POST' ) {
    // var_dump($_FILES);
    // die();

    $product_id = isset($_POST['product_id']) ? intval($_POST['product_id']) : 0;

    if ($product_id > 0) {
        $titulo = isset($_POST['titulo']) ? sanitize_text_field($_POST['titulo']) : '';
        $descripcion = isset($_POST['descripcion']) ? wp_kses_post($_POST['descripcion']) : '';

        if (!empty($titulo)) {
            wp_update_post(array(
                'ID' => $product_id,
                'post_title' => $titulo
            ));
        }

        wp_update_post(array(
            'ID' => $product_id,
            'post_content' => $descripcion
        ));

        $precio = isset($_POST['precio']) ? floatval($_POST['precio']) : 0.00;
        if ($precio > 0) {
            update_post_meta($product_id, '_regular_price', $precio);
            update_post_meta($product_id, '_price', $precio);
        }else{
            update_post_meta($product_id, '_regular_price', 0);
            update_post_meta($product_id, '_price', 0);
        }

        $adjunto_id = get_post_thumbnail_id($product_id);
        
        if (!empty($_FILES['filepublic-editar']['tmp_name'])) {
            $imagen = $_FILES['filepublic-editar'];
            $imagen_temp = $imagen['tmp_name'];

            $upload_dir = wp_upload_dir();
            $imagen_nombre = sanitize_file_name($imagen['name']); 
            $imagen_ruta = $upload_dir['path'] . '/' . $imagen_nombre;
            move_uploaded_file($imagen_temp, $imagen_ruta);
        
            $metadata = wp_generate_attachment_metadata($adjunto_id, $imagen_ruta);
            wp_update_attachment_metadata($adjunto_id, $metadata);
        
            $attachment_id = wp_insert_attachment(array(
                'post_mime_type' => $imagen['type'],
                'post_title' => $imagen_nombre,
                'post_content' => '',
                'post_status' => 'inherit'
            ), $imagen_ruta, $product_id);
        
            set_post_thumbnail($product_id, $attachment_id);
        } 
            
        $categoria_nombre = isset($_POST['categoria_nombre']) ? sanitize_text_field($_POST['categoria_nombre']) : '';
        $categoria = get_term_by('name', $categoria_nombre, 'product_cat');
        
        if ($categoria) {
            $categoria_id = $categoria->term_id;
        } else {
            $categoria_id = $categoria_nombre;
        }
        
        wp_set_object_terms($product_id, $categoria_id, 'product_cat');
        

        $categoria_pago = isset($_POST['categoria_pago']) ? sanitize_text_field($_POST['categoria_pago']) : '';
        if (!empty($categoria_pago)) {
            wp_set_object_terms($product_id, 'categoria_pago', $categoria_pago);
        }

        $categoria_pago = isset($_POST['catp']) ? sanitize_text_field($_POST['catp']) : '';
        if (!empty($categoria_pago)) {
            update_post_meta($product_id, 'categoria_pago', $categoria_pago);
        }


        $tema = isset($_POST['tema']) ? sanitize_text_field($_POST['tema']) : '';
        if (!empty($tema)) {
            update_post_meta($product_id, 'tema', $tema);
        }

        $idioma = isset($_POST['idioma']) ? sanitize_text_field($_POST['idioma']) : '';
        if (!empty($idioma)) {
            update_post_meta($product_id, 'idioma', $idioma);
        }

        $mayorEdad = isset($_POST['mayorEdad']) ? sanitize_text_field($_POST['mayorEdad']) : '';
        update_post_meta($product_id, 'mayorEdad', $mayorEdad);

        $checkrecomendados = isset($_POST['checkrecomendados']) ? sanitize_text_field($_POST['checkrecomendados']) : '';
        update_post_meta($product_id, 'checkrecomendados', $checkrecomendados);

        $compartir_value = isset($_POST['share']) ? $_POST['share'] : array();
        update_post_meta($product_id, 'compartir', $compartir_value);


        $iva = isset($_POST['iva']) ? floatval(sanitize_text_field($_POST['iva'])) : 0;
        $descuento = isset($_POST['descuento']) ? floatval(sanitize_text_field($_POST['descuento'])) : 0;
        $descuento_recomendados = isset($_POST['descuento_recomendados']) ? floatval(sanitize_text_field($_POST['descuento_recomendados'])) : 0;

        $precio_con_iva = 0;
        $precio_descuento_sin_recomendar = 0;
        $precio_descuento_recomendado = 0;

        $precio_con_iva = $precio * ($iva/100);
        $precio_descuento_sin_recomendar = ($precio - $precio_con_iva) * ($descuento/100);
        $precio_descuento_recomendado = ($precio - $precio_con_iva) * ($descuento_recomendados/100);

       

        // Ejemplo de uso en cálculo del precio final con descuento

        if ($checkrecomendados === 'on'){
            $precioconiva = $precio * ($iva/100);
            $precio_con_descuento = $precio - (($precio - $precioconiva) * ($descuento_recomendados / 100)) - $precioconiva;
        } else {
            $precioconiva = $precio * ($iva/100);
            $precio_con_descuento = $precio - (($precio - $precioconiva) * ($descuento / 100)) - $precioconiva;
        }

       
        $uploaded_images = $_FILES['imagenes'];
// var_dump($uploaded_images);
// echo "<br>";
// var_dump($_FILES);
// exit();

        if (!empty($uploaded_images['name'][0])) {
            // Obtener la galería de imágenes existente
            $product_images = get_post_meta($product_id, '_product_image_gallery', true);
            $product_images = explode(',', $product_images);

            for ($i = 0; $i < count($uploaded_images['name']); $i++) {
                $tmp_name = $uploaded_images['tmp_name'][$i];
                if (!empty($tmp_name) && getimagesize($tmp_name)) {
                    $upload_dir = wp_upload_dir();
                    $image_name = $uploaded_images['name'][$i];
                    $image_path = $upload_dir['path'] . '/' . $image_name;
                    move_uploaded_file($tmp_name, $image_path);

                    $attachment_id = wp_insert_attachment(array(
                        'post_mime_type' => $uploaded_images['type'][$i],
                        'post_title' => $image_name,
                        'post_content' => '',
                        'post_status' => 'inherit'
                    ), $image_path, $product_id);

                    $product_images[] = $attachment_id;
                }
            }

            update_post_meta($product_id, '_product_image_gallery', implode(',', $product_images));
        }

        // Obtener la galería de imágenes actualizada
        $product_images = get_post_meta($product_id, '_product_image_gallery', true);
        $product_images = explode(',', $product_images);

        echo json_encode($product_images);

        //eliminar imagenes destacada del producto 
        // Recorrer los IDs de las imágenes a eliminar
        foreach ($_POST['deleted_images'] as $deleted_image_id) {
            delete_post_meta($product_id, '_product_image_gallery', $deleted_image_id);
            wp_delete_attachment($deleted_image_id, true);
        }
            
        update_post_meta($product_id, 'precio_descuento_sin_recomendar', $precio_descuento_sin_recomendar);
        update_post_meta($product_id, 'precio_descuento_recomendado', $precio_descuento_recomendado);
        update_post_meta($product_id, 'precioconiva', $precio_con_iva);

        update_post_meta($product_id, 'iva', $iva);
        update_post_meta($product_id, 'descuento', $descuento);
        update_post_meta($product_id, 'descuentox_recomendados', $descuento_recomendados);

        $precio_con_descuento = number_format($precio_con_descuento, 2);
        update_post_meta($product_id, 'precio_con_descuento', $precio_con_descuento);

        $product_id = isset($_POST['product_id']) ? intval($_POST['product_id']) : 0;

        $correo_e = isset($_POST['correo_e']) ? $_POST['correo_e'] : '';
        update_field('correo_e', $correo_e, $product_id);

        if (!empty($_FILES['fileLibro-editar']['tmp_name'])) {
            $fileLibro = $_FILES['fileLibro-editar'];
            $fileLibro_nombre = $fileLibro['name'];
            $fileLibro_nombre = preg_replace('/\s/', '-', $fileLibro_nombre);
            $fileLibro_temp = $fileLibro['tmp_name'];

            $upload_dir = wp_upload_dir();
            $fileLibro_ruta = $upload_dir['path'] . '/' . $fileLibro_nombre;

            if (move_uploaded_file($fileLibro_temp, $fileLibro_ruta)) {
                $existing_files = get_post_meta($product_id, '_downloadable_files', true);
                if (!empty($existing_files)) {
                    foreach ($existing_files as $file) {
                        wp_delete_file($file['file']);
                    }
                }

                $file_path = md5($upload_dir['baseurl'] . '/' . $fileLibro_nombre);
                $file_array = array(
                    'id' => $file_path,
                    'name' => $fileLibro_nombre,
                    'file' => $upload_dir['baseurl'] . '/' . $fileLibro_nombre
                );

                $new_files = array($file_path => $file_array);
                update_post_meta($product_id, '_downloadable_files', $new_files);
            }
        }
       
        $current_user = wp_get_current_user();
        $username = $current_user->user_login;
        $redirect_url = '/members/' . $username . '/profile';
    
        wp_redirect($redirect_url);
        exit;
        
    }
}