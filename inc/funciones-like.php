<?php 
function enviar_notificacion_like_producto($producto_id) {
    $user_id = get_current_user_id();
    $author_id = get_post_field('post_author', $producto_id);

    // Verificar si el autor del producto es diferente al usuario que dio like
    if ($user_id !== $author_id) {
        bp_notifications_add_notification(array(
            'user_id'           => $author_id,
            'item_id'           => $producto_id,
            'secondary_item_id' => $user_id, 
            'component_name'    => 'likes',
            'component_action'  => 'like_action',
            'date_notified'     => bp_core_current_time(),
            'is_new'            => 1,
        ));
    }
}

// 2. Registrar el componente "likes"
function likes_filter_notifications_get_registered_components($component_names = array()) {
    if (!is_array($component_names)) {
        $component_names = array();
    }
    $component_names[] = 'likes';
    return $component_names;
}
add_filter('bp_notifications_get_registered_components', 'likes_filter_notifications_get_registered_components');

// 3. Formatear la notificación para el componente "likes"
function likes_format_buddypress_notifications($action, $item_id, $secondary_item_id, $total_items, $format = 'string') {
    if ('like_action' === $action) {
        $like_text = "Tu producto ha recibido un like";

        if ('string' === $format) {
            return $like_text;
        } else {
            return array(
                'text' => $like_text,
            );
        }
    }
}
add_filter('bp_notifications_get_notifications_for_user', 'likes_format_buddypress_notifications', 10, 5);


function actualizar_contador($producto_id) {
    $contador_likes = intval(get_post_meta($producto_id, 'contador_likes', true));
    echo esc_html($contador_likes) . " me gusta";
  }
  
  function obtener_contador_likes_ajax() {
    $producto_id = get_the_ID();
    $user_id = get_current_user_id();
    // $author_id = get_post_field('post_author', $producto_id);
    // $author_id = get_post_field('post_author', $producto_id);

    if (isset($_POST['producto_id'])) {
        $producto_id = intval($_POST['producto_id']);
        $contador_likes = intval(get_post_meta($producto_id, 'contador_likes', true));
        echo esc_html($contador_likes) . " me gusta";
        $cont_viejo = $contador_likes;
       
        $product = get_post($producto_id);
        foreach ($product as $field => $value) {
            if ($field === 'post_author') {
                $author_id = $value;
            }
        }
        $contador_likes_viejo = intval(get_post_meta($producto_id, 'contador_likes', true));
        
        $usuario_dio_like = get_user_meta($user_id, 'like_producto_' . $producto_id, true);
        // var_dump($usuario_dio_like);
        // echo "user_id: " . $user_id;
        // echo "author_id: " . $author_id;
        // echo "product_id: " . $producto_id;
        // echo $usuario_dio_like;

        // echo "contador_likes: " . $contador_likes;
        // echo "contador_likes: " . $cont_viejo;

        if($usuario_dio_like){
            if ($user_id != $author_id) {
                enviar_notificacion_like_producto($producto_id);
                // echo "pase";
            }else{
                // echo "no pase";
            }
        }

        
    }

   
    // Verificar si el autor del producto es diferente al usuario que dio like
    
    wp_die();
  }
  add_action('wp_ajax_obtener_contador_likes', 'obtener_contador_likes_ajax');
  add_action('wp_ajax_nopriv_obtener_contador_likes', 'obtener_contador_likes_ajax');
  
  
  function actualizar_contador_ajax() {
    if (isset($_POST['producto_id'])) {
        $producto_id = intval($_POST['producto_id']);
        $user_id = get_current_user_id();
        
        if ($user_id === 0) {
            echo "<script>";
            echo "swal('Debes iniciar sesión', '', 'error').then(function() { window.location.href = 'ruta-de-inicio-de-sesion'; });";
            echo "</script>";
            exit();
      }
      if (current_user_can('invitadoLib')) {
        echo "error";
        exit;
    }
        $usuario_dio_like = get_user_meta($user_id, 'like_producto_' . $producto_id, true);
  
        if ($usuario_dio_like) {
            $contador_likes = intval(get_post_meta($producto_id, 'contador_likes', true)) - 1;
            delete_user_meta($user_id, 'like_producto_' . $producto_id);
        } else {
            $contador_likes = intval(get_post_meta($producto_id, 'contador_likes', true)) + 1;
            update_user_meta($user_id, 'like_producto_' . $producto_id, true);
        }
        
        update_post_meta($producto_id, 'contador_likes', $contador_likes);
  
        actualizar_contador($producto_id);
    }
    wp_die();
  }
  
  add_action('wp_ajax_actualizar_contador', 'actualizar_contador_ajax');
  add_action('wp_ajax_nopriv_actualizar_contador', 'actualizar_contador_ajax');
  
  function get_current_user_id_ajax() {
      if (is_user_logged_in()) {
          $user_id = get_current_user_id();
          echo $user_id;
      } else {
          echo '0';
      }
      wp_die();
  }
  
  add_action('wp_ajax_get_current_user_id', 'get_current_user_id_ajax');
  add_action('wp_ajax_nopriv_get_current_user_id', 'get_current_user_id_ajax');
  
  function agregar_metabox_contador_likes() {
    add_meta_box(
        'contador_likes_metabox',
        'Contador de Likes',
        'renderizar_metabox_contador_likes',
        'product',
        'side',
        'default'
    );
  }
  add_action('add_meta_boxes', 'agregar_metabox_contador_likes');
  
  function renderizar_metabox_contador_likes($post) {
    $contador_likes = get_post_meta($post->ID, 'contador_likes', true);
    echo '<p>Contador de Likes: ' . esc_html($contador_likes) . '</p>';
  } 


  /* Sistema de guardados */

  function guardar_producto_ajax() {
    if (is_user_logged_in()) {
        $user_id = get_current_user_id();
        $producto_id = isset($_POST['producto_id']) ? intval($_POST['producto_id']) : 0;
        $productos_guardados = get_user_meta($user_id, 'productos_guardados', true);

        if (!is_array($productos_guardados)) {
            $productos_guardados = array();
        }

        // Verificar si el producto ya está guardado
        $key = array_search($producto_id, $productos_guardados);

        if ($key !== false) {
            // Si está guardado, eliminarlo
            unset($productos_guardados[$key]);
            update_user_meta($user_id, 'productos_guardados', $productos_guardados);
            echo 'Producto eliminado con éxito';
        } else {
            // Si no está guardado, agregarlo
            $productos_guardados[] = $producto_id;
            update_user_meta($user_id, 'productos_guardados', $productos_guardados);
            echo 'Producto guardado con éxito';
        }
    } else {
        echo 'Usuario no autenticado';
    }
    if (current_user_can('invitadoLib')) {
        echo "error";
        exit;
    }
    wp_die();
}

add_action('wp_ajax_guardar_producto', 'guardar_producto_ajax');
add_action('wp_ajax_nopriv_guardar_producto', 'guardar_producto_ajax');


function producto_esta_guardado_para_usuario($user_id, $producto_id) {
    $productos_guardados = get_user_meta($user_id, 'productos_guardados', true);
    
    if (in_array($producto_id, (array) $productos_guardados)) {
        return true;
    } else {
        return false;
    }
}


// Verificar si el producto está guardado para el usuario actual
// function producto_esta_guardado($producto_id) {
//     if (is_user_logged_in()) {
//         $user_id = get_current_user_id();
//         $productos_guardados = obtener_productos_guardados_por_usuario($user_id);
//         return in_array($producto_id, $productos_guardados);
//     }
//     return false; // Si el usuario no está autenticado, no se puede determinar si el producto está guardado
// }

// // // Función para obtener los productos guardados por un usuario
// function obtener_productos_guardados_por_usuario($user_id) {
//     // Obtener los productos guardados para el usuario específico
//     $productos_guardados = get_user_meta($user_id, 'productos_guardados', true);
    
//     // Si no hay productos guardados, devolver un array vacío
//     if (!is_array($productos_guardados)) {
//         return array();
//     }
    
//     // Devolver los IDs de los productos guardados
//     echo $productos_guardados;
// }

