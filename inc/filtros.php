<li>
<div id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link d-flex" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/arrow.png'; ?>" alt="" width="15px" height="15px">
            <span> <?php esc_html_e('Autor', 'libreriasocial'); ?> </span>
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        <?php
        $users = get_users(array(
          'role__in' => array('shop_manager', 'administrator')
        ));

        foreach ($users as $user) {
          ?>
          <div class="d-flex contenedor-checkbox">
            <input type="checkbox" id="<?php echo $user->ID; ?>" name="users[]" value="<?php echo $user->ID; ?>">
            <label for="<?php echo $user->ID; ?>" class="d-flex flex-row-reverse"><span><?php echo $user->display_name; ?></span></label>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
</div>
					
</li>
<li>

<div id="accordion2">
  <div class="card">
    <div class="card-header" id="headingOne2">
      <h5 class="mb-0">
        <button class="btn btn-link d-flex" data-toggle="collapse" data-target="#collapseOne2" aria-expanded="true" aria-controls="collapseOne2">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/arrow.png'; ?>" alt="" width="15px" height="15px">
            <span><?php esc_html_e('Precio', 'libreriasocial'); ?></span>
        </button>
      </h5>
    </div>

    <div id="collapseOne2" class="collapse " aria-labelledby="headingOne2" data-parent="#accordion2">
      <div class="card-body">
        <form id="price-filter-form">
          <div class="form-group">
            <label for="min-price"><?php esc_html_e('Precio Mínimo:', 'libreriasocial'); ?></label>
            <input type="number" class="form-control" id="min-price" name="min_price">
          </div>
          <div class="form-group mb-0">
            <label for="max-price"><?php esc_html_e('Precio Máximo:', 'libreriasocial'); ?></label>
            <input type="number" class="form-control" id="max-price" name="max_price">
          </div>
          <a href="#" class="btn btn-primary boton-publicar" id="filter-price"><?php esc_html_e('Filtros', 'libreriasocial'); ?></a>
        </form>
      </div>
    </div>
  </div>
</div>

</li>
<li>

<?php
if (function_exists('acf_get_field_groups')) {
    $field_groups = acf_get_field_groups();

    if (!empty($field_groups)) {
        foreach ($field_groups as $group) {
            $fields = acf_get_fields($group['key']);
            if (!empty($fields)) {
                foreach ($fields as $field) {
                    if ($field['name'] === 'idioma') {
                        if (isset($field['choices']) && !empty($field['choices'])) {
                            ?>
                            <div id="accordion3">
                                <div class="card">
                                    <div class="card-header" id="headingOne3">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link d-flex" data-toggle="collapse" data-target="#collapseOne3" aria-expanded="true" aria-controls="collapseOne3">
                                                <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/arrow.png'; ?>" alt="" width="15px" height="15px">
                                                <span><?php esc_html_e('Idioma', 'libreriasocial'); ?></span>
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseOne3" class="collapse " aria-labelledby="headingOne3" data-parent="#accordion3">
                                        <div class="card-body">
                                            <?php
                                            foreach ($field['choices'] as $value => $label) {
                                                ?>
                                                <div class="d-flex contenedor-checkbox">
                                                    <input type="checkbox" id="idioma_<?php echo sanitize_title($label); ?>" name="idiomas[]" value="<?php echo sanitize_title($label); ?>">
                                                    <label for="idioma_<?php echo sanitize_title($label); ?>" class="ml-2 d-flex flex-row-reverse"><span><?php echo esc_html($label); ?></span></label>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
            }
        }
    }
}
?>


</li>
<li>

<div id="accordion7">
    <div class="card">
        <div class="card-header" id="headingOne7">
            <h5 class="mb-0">
                <button class="btn btn-link d-flex" data-toggle="collapse" data-target="#collapseOne7" aria-expanded="true" aria-controls="collapseOne7">
                    <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/arrow.png'; ?>" alt="" width="15px" height="15px">
                    <span><?php esc_html_e('Fecha', 'libreriasocial'); ?></span>
                </button>
            </h5>
        </div>

        <div id="collapseOne7" class="collapse " aria-labelledby="headingOne7" data-parent="#accordion7">
            <div class="card-body">
                <form id="filter-form">
                    <div class="form-group">
                        <label for="start-date"><?php esc_html_e('Fecha de inicio:', 'libreriasocial'); ?></label>
                        <input type="date" id="start-date" name="start_date" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="end-date"><?php esc_html_e('Fecha de fin:', 'libreriasocial'); ?></label>
                        <input type="date" id="end-date" name="end_date" class="form-control" max="<?php echo date('Y-m-d'); ?>">
                    </div>
            </form>
            </div>
        </div>
    </div>
</div>


</li>
<li>

<?php
if (function_exists('acf_get_field_groups')) {
    $field_groups = acf_get_field_groups();

    if (!empty($field_groups)) {
        foreach ($field_groups as $group) {
            $fields = acf_get_fields($group['key']);
            if (!empty($fields)) {
                foreach ($fields as $field) {
                    if ($field['name'] === 'tema') {
                        if (isset($field['choices']) && !empty($field['choices'])) {
                            ?>
                            <div id="accordion4">
                                <div class="card">
                                    <div class="card-header" id="headingOne4">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link d-flex" data-toggle="collapse" data-target="#collapseOne4" aria-expanded="true" aria-controls="collapseOne4">
                                                <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/arrow.png'; ?>" alt="" width="15px" height="15px">
                                                <span><?php esc_html_e('Tema', 'libreriasocial'); ?> </span>
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseOne4" class="collapse " aria-labelledby="headingOne4" data-parent="#accordion4">
                                        <div class="card-body">
                                            <?php
                                            foreach ($field['choices'] as $value => $label) {
                                                ?>
                                                <div class="d-flex contenedor-checkbox">
                                                    <input type="checkbox" id="tema_<?php echo sanitize_title($label); ?>" name="temas[]" value="<?php echo sanitize_title($label); ?>">
                                                    <label for="tema_<?php echo sanitize_title($label); ?>" class="ml-2 d-flex flex-row-reverse"><span><?php echo esc_html($label); ?></span></label>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
            }
        }
    }
}
?>



</li>

<li>

<div id="accordion6">
    <div class="card">
        <div class="card-header" id="headingOne6">
            <h5 class="mb-0">
                <button class="btn btn-link d-flex" data-toggle="collapse" data-target="#collapseOne6" aria-expanded="true" aria-controls="collapseOne6">
                    <img src="<?php echo get_stylesheet_directory_uri() . '/assents/svg/arrow.png'; ?>" alt="" width="15px" height="15px">
                    <span><?php esc_html_e('Género Literario', 'libreriasocial'); ?></span>
                </button>
            </h5>
        </div>

        <div id="collapseOne6" class="collapse " aria-labelledby="headingOne6" data-parent="#accordion6">
            <div class="card-body">
                <?php
                $categories = get_categories(array(
                    'taxonomy'     => 'product_cat',
                    'exclude'      => get_option('default_product_cat'), 
                    'hide_empty'   => false,
                ));

                foreach ($categories as $category) {
                    ?>
                    <div class="d-flex contenedor-checkbox">
                        <input type="checkbox" id="cat_<?php echo $category->slug; ?>" name="categorias[]" value="<?php echo $category->slug; ?>">
                        <label for="cat_<?php echo $category->slug; ?>" class="ml-2 d-flex flex-row-reverse"><span><?php echo $category->name; ?></span></label>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>


</li>